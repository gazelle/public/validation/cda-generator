#!/bin/bash

CURRENT_PATH=`pwd`
sonar_host_url="http://gazelle.ihe.net/sonar"
sonar_jdbc_url="jdbc:postgresql://ovh2.ihe-europe.net/sonar"
sonar_jdbc_driverClassName="org.postgresql.Driver"
sonar_jdbc_username="gazelle"
sonar_jdbc_password="gazelle"

if [ "x$1" = "xlocal" ]; then 
	echo "executed on local sonar"
	CURRENT_PATH=`pwd`
	sonar_host_url="http://localhost:9000"
	sonar_jdbc_url="jdbc:postgresql://localhost/sonar?useUnicode=true&amp;characterEncoding=utf8"
	sonar_jdbc_driverClassName="org.postgresql.Driver"
	sonar_jdbc_username="gazelle"
	sonar_jdbc_password="gazelle"
fi

process_sonar(){
	echo "${VALIDATOR_NAME} - processing"
	if [ -d ${VALIDATOR_PATH} ]; then 
		echo "${VALIDATOR_NAME} - SVN update"
		cd ${VALIDATOR_PATH}
		svn up
		cd ${CURRENT_PATH}
	else 
		echo "${VALIDATOR_NAME} - SVN checkout"
		svn checkout --username anonsvn --password anonsvn ${VALIDATOR_FORGE_PATH} ${VALIDATOR_PATH}
	fi
	cd ${VALIDATOR_PATH}
	echo "${VALIDATOR_NAME} - sonar processing"
	mvn package
	mvn sonar:sonar -Dsonar.host.url=${sonar_host_url} -Dsonar.jdbc.url=${sonar_jdbc_url} -Dsonar.jdbc.driverClassName=${sonar_jdbc_driverClassName} -Dsonar.jdbc.username=${sonar_jdbc_username} -Dsonar.jdbc.password=${sonar_jdbc_password}
	echo "${VALIDATOR_NAME} - end processing"
	echo "=============================================================================================="
	cd ${CURRENT_PATH}
}

VALIDATOR_PATH="$CURRENT_PATH/ccd-validator-jar"
VALIDATOR_NAME="CCD"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/ccd-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cda-validator-jar"
VALIDATOR_NAME="CDA"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/cda-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cdaEpsosSpec-validator-jar"
VALIDATOR_NAME="CDA-EPSOS-SPEC"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/cdaEpsosSpec-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cdamin-validator-jar"
VALIDATOR_NAME="ASIP-CDA-MINIMALE"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/ihe/validators/src-gen/cda/cdamin-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cdapcc-validator-jar"
VALIDATOR_NAME="CDA-PCC"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/cdapcc-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cdapharm-validator-jar"
VALIDATOR_NAME="CDA-PHARM"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/cdapharm-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/cdarcp-validator-jar"
VALIDATOR_NAME="CDA-RPC"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/cdarcp-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/common-code-jar"
VALIDATOR_NAME="CDA-COMMON"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/common-code-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/common-operations-jar"
VALIDATOR_NAME="CDA-COMMON-OPERATION"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/common-operations-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/datatypes-validator-jar"
VALIDATOR_NAME="DATATYPES"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/datatypes-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/ihe-xmltools-jar"
VALIDATOR_NAME="XMLTOOLS"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/Maven/ihe-xmltools-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xdlab-validator-jar"
VALIDATOR_NAME="CDA-XDLAB"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/xdlab-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xdssd-validator-jar"
VALIDATOR_NAME="CDA - XDSSD"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/xdssd-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/crc-validator-jar"
VALIDATOR_NAME="CDA - CRC"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/crc-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/ccda1-validator-jar"
VALIDATOR_NAME="CDA - C-CDA R1"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/ccda1-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/luxh-validator-jar"
VALIDATOR_NAME="CDA - LUX Header"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/cda/luxh-validator-jar/trunk"
process_sonar;


