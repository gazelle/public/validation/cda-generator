<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n1="urn:hl7-org:v3"
  xmlns:n2="urn:hl7-org:v3/meta/voc" xmlns:voc="urn:hl7-org:v3/voc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:darva="xalan://com.projet.util.Utilitaire">
    <xsl:output method="xml" indent="yes" version="4.01" encoding="ISO-8859-1" doctype-public="-//W3C//DTD HTML 4.01//EN"/>
    <!-- CDA document -->
    <xsl:param name="id" />
    <xsl:param name="extension" />
    <xsl:param name="aa" />
    <xsl:param name="effectivetime"/>
    <xsl:param name="language" />
    
    <xsl:param name="pharma.id.root" />
    <xsl:param name="pharma.id.extension" />
    <xsl:param name="pharma.id.assigningAuthorityName" />
    <xsl:param name="pharma.given" />
    <xsl:param name="pharma.family" />
    <xsl:param name="pharma.org.id.root" />
    <xsl:param name="pharma.org.id.extension" />
    <xsl:param name="pharma.org.name" />
    
    <xsl:param name="custodian.id.root" />
    <xsl:param name="custodian.id.extension" />
    <xsl:param name="custodian.name" />
    
    <xsl:template match="/">
        <xsl:apply-templates select="n1:ClinicalDocument"/>
    </xsl:template>
    <xsl:template match="n1:ClinicalDocument">
        <ClinicalDocument xmlns="urn:hl7-org:v3" xmlns:cda="urn:hl7-org:v3"
            xmlns:epsos="urn:epsos-org:ep:medication"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
            <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.2.2"/>
            <id displayable="true">
                <xsl:attribute name="root">
                    <xsl:value-of select="$id"/>
                </xsl:attribute>
                <xsl:attribute name="extension">
                    <xsl:value-of select="$extension"/>
                </xsl:attribute>
                <xsl:attribute name="assigningAuthorityName">
                    <xsl:value-of select="$aa"/>
                </xsl:attribute>
            </id>
            <code code="60593-1" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                displayName="eDispensation"/>
            <title>Generated eDispensation friendly</title>
            <effectiveTime>
                <xsl:attribute name="value">
                    <xsl:value-of select="$effectivetime"/>
                </xsl:attribute>
            </effectiveTime>
            <confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"
                codeSystemName="Confidentiality" displayName="normal"/>
            <languageCode>
                <xsl:attribute name="code">
                    <xsl:value-of select="$language"/>
                </xsl:attribute>
            </languageCode>

            <xsl:copy-of select="/n1:ClinicalDocument/n1:recordTarget"/>
            
            <author typeCode="AUT" contextControlCode="OP">
                <functionCode code="2262" codeSystem="2.16.840.1.113883.2.9.6.2.7" codeSystemName="ISCO" displayName="farmatseudid"/>
                <!--Koostamise aeg-->
                <time>
                    <xsl:attribute name="value">
                        <xsl:value-of select="$effectivetime"/>
                    </xsl:attribute>
                </time>
                <assignedAuthor classCode="ASSIGNED">
                    <xsl:call-template name="addPersonInfo" />
                </assignedAuthor>
            </author>
            
            <custodian typeCode="CST">
                <assignedCustodian classCode="ASSIGNED">
                    <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
                        <id>
                            <xsl:attribute name="root"><xsl:value-of select="$custodian.id.root"/></xsl:attribute>
                            <xsl:attribute name="extension"><xsl:value-of select="$custodian.id.extension"/></xsl:attribute>
                        </id>
                        <name>
                            <xsl:value-of select="$custodian.name"/>
                        </name>
                    </representedCustodianOrganization>
                </assignedCustodian>
            </custodian>
            
            <legalAuthenticator>
                <time>
                    <xsl:attribute name="value"><xsl:value-of select="$effectivetime"/></xsl:attribute>
                </time>
                <signatureCode code="S"/>
                <assignedEntity>
                    <id>
                        <xsl:attribute name="root"><xsl:value-of select="$pharma.id.root"/></xsl:attribute>
                        <xsl:attribute name="extension"><xsl:value-of select="$pharma.id.extension"/></xsl:attribute>
                        <xsl:attribute name="assigningAuthorityName"><xsl:value-of select="$pharma.id.assigningAuthorityName"/></xsl:attribute>
                        <xsl:attribute name="displayable">false</xsl:attribute>
                    </id> 
                    <!-- R1.10.8 required nullFlavor=NI allowed and the "value" and "use" attributes shall be omitted otherwise  " attributes shall be present-->
                    <telecom nullFlavor="NI"/>
                    <assignedPerson>
                        <name>
                            <family>
                                <xsl:value-of select="$pharma.family"/>
                            </family>
                            <given>
                                <xsl:value-of select="$pharma.given"/>
                            </given>
                        </name>
                    </assignedPerson>
                    <representedOrganization>
                        <id>
                            <xsl:attribute name="root">
                                <xsl:value-of select="$pharma.org.id.root"/>
                            </xsl:attribute>
                            <xsl:attribute name="extension">
                                <xsl:value-of select="$pharma.org.id.extension"/>
                            </xsl:attribute>
                        </id>
                        <name>
                            <xsl:value-of select="$pharma.org.name"/>
                        </name>
                        <telecom nullFlavor="NI"/>
                        <addr nullFlavor="NA"/>
                    </representedOrganization>
                </assignedEntity>
            </legalAuthenticator>
            
            <component typeCode="COMP" contextConductionInd="true">
                <structuredBody classCode="DOCBODY" moodCode="EVN">
                    
                    <xsl:for-each select="/n1:ClinicalDocument/n1:component/n1:structuredBody/n1:component">
                        <component contextConductionInd="true" typeCode="COMP">
                        <xsl:for-each select="child::*" xml:base="ss">
                            <xsl:if test="name()='section'">
                              <section classCode="DOCSECT" moodCode="EVN">
                                  <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.2.2" />
                                  <xsl:for-each select="child::*">
                                      <xsl:if test="name()='id'">
                                          <xsl:copy-of select="self::*"/>
                                      </xsl:if>
                                  </xsl:for-each>
                                  <code code="60590-7" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC" displayName="Medication dispensed"/>
                                  <title>eDispensation</title>
                                  <xsl:for-each select="child::*">
                                      <xsl:if test="name()='text'">
                                          <xsl:copy-of select="self::*"/>
                                      </xsl:if>
                                  </xsl:for-each>
                                  
                                  <xsl:for-each select="child::node()[name()='entry']">
                                        <entry typeCode="COMP" contextConductionInd="true">
                                            <supply classCode="SPLY" moodCode="EVN">
                                                <xsl:for-each select="child::*">
                                                    <xsl:if test="name()='substanceAdministration'">
                                                        <xsl:for-each select="child::*">
                                                            <xsl:if test="name()='templateId' or name()='id'
                                                                or name()='text' or name()='statusCode'
                                                                or name()='routeCode' or name()='doseQuantity'
                                                                or name()='quantity' or name()='effectiveTime'">
                                                                <xsl:copy-of select="self::*"/>
                                                            </xsl:if>
                                                            <xsl:if test="name()='consumable'">
                                                                <product>
                                                                    <xsl:for-each select="child::node()[name()='manufacturedProduct']">
                                                                        <xsl:copy-of select="self::*"/>
                                                                    </xsl:for-each>
                                                                </product>
                                                                <performer typeCode="PRF">
                                                                    <time>
                                                                        <xsl:value-of select="$effectivetime"/>
                                                                    </time>
                                                                    <assignedEntity>
                                                                        <xsl:call-template name="addPersonInfo" />
                                                                    </assignedEntity>
                                                                </performer>
                                                            </xsl:if>
                                                         </xsl:for-each>
                                                    </xsl:if>
                                                </xsl:for-each>
                                                <entryRelationship typeCode="REFR">
                                                    <xsl:copy-of select="child::node()[name()='substanceAdministration']" />
                                                </entryRelationship>
                                            </supply>
                                        </entry>
                                  </xsl:for-each>
                                  
                              </section>
                           </xsl:if>
                        </xsl:for-each>   
                        </component>
                 </xsl:for-each>
                    
                </structuredBody>
            </component>
            

        </ClinicalDocument>
        
    </xsl:template>
    
    <xsl:template name="addPersonInfo">
        <id>
            <xsl:attribute name="root">
                <xsl:value-of select="$pharma.id.root"/> 
            </xsl:attribute>
            <xsl:attribute name="extension">
                <xsl:value-of select="$pharma.id.extension"/>
            </xsl:attribute>
            <xsl:attribute name="assigningAuthorityName">
                <xsl:value-of select="$pharma.id.assigningAuthorityName"/>
            </xsl:attribute>
            <xsl:attribute name="displayable">false</xsl:attribute>
        </id>
        <telecom nullFlavor="NI"/>
        <assignedPerson classCode="PSN" determinerCode="INSTANCE">
            <name>
                <given>
                    <xsl:value-of select="$pharma.given"/>
                </given>
                <family>
                    <xsl:value-of select="$pharma.family"/>
                </family>
            </name>
        </assignedPerson>
        <representedOrganization classCode="ORG" determinerCode="INSTANCE">
            <id>
                <xsl:attribute name="root">
                    <xsl:value-of select="$pharma.org.id.root"/>
                </xsl:attribute>
                <xsl:attribute name="extension">
                    <xsl:value-of select="$pharma.org.id.extension"/>
                </xsl:attribute>
            </id>
            <name>
                <xsl:value-of select="$pharma.org.name"/>
            </name>
            <telecom nullFlavor="NI"/>
            <addr nullFlavor="NA"/>
        </representedOrganization>
    </xsl:template>
 
    
</xsl:stylesheet>

