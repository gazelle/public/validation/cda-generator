INSERT INTO public.validator_describer (id, validator_name) VALUES (5, 'HL7 - CDA Release 2');
INSERT INTO public.validator_describer (id, validator_name) VALUES (6, 'HL7 - CDA Release 2 (strict)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (7, 'IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (8, 'HL7 - CCD');
INSERT INTO public.validator_describer (id, validator_name) VALUES (9, 'ASIP - CDA Structuration minimale');
INSERT INTO public.validator_describer (id, validator_name) VALUES (10, 'IHE - PCC - Common templates');
INSERT INTO public.validator_describer (id, validator_name) VALUES (11, 'HL7 - C-CDA R1.1');
INSERT INTO public.validator_describer (id, validator_name) VALUES (12, 'HL7 - C-CDA R2.1');
INSERT INTO public.validator_describer (id, validator_name) VALUES (13, 'IHE - CARD - Cath Report Content (CRC)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (17, 'IHE - CARD - Registry Content Submission (RCS-C)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (18, 'IHE - PHARM - Pharmacy Prescription (PRE)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (19, 'IHE - PHARM - Pharmacy Dispense (DIS)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (20, 'IHE - PHARM - Pharmacy Pharmaceutical Advice (PADV)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (21, 'IHE - CARD - Registry Content Submission - Electrophysiology (RCS-EP)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (22, 'IHE - EYE - Summary Record (EC-Summary)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (23, 'IHE - EYE - GEE Progress Note');
INSERT INTO public.validator_describer (id, validator_name) VALUES (24, 'EXPAND - CDA documents [informal testing - art-decor based requirements]');
INSERT INTO public.validator_describer (id, validator_name) VALUES (25, 'IHE - PATH - Anatomic Pathology Structured Reports (APSR)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (26, 'IHE - LAB - Sharing Laboratory Reports (XD-LAB)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (27, 'epSOS - Patient Summary Friendly');
INSERT INTO public.validator_describer (id, validator_name) VALUES (28, 'epSOS - Patient Summary Pivot');
INSERT INTO public.validator_describer (id, validator_name) VALUES (29, 'epSOS - HCER HealthCare Encounter Report');
INSERT INTO public.validator_describer (id, validator_name) VALUES (30, 'epSOS - MRO Medication Related Overview');
INSERT INTO public.validator_describer (id, validator_name) VALUES (31, 'epSOS - Scanned Document');
INSERT INTO public.validator_describer (id, validator_name) VALUES (32, 'epSOS - ePrescription Friendly');
INSERT INTO public.validator_describer (id, validator_name) VALUES (33, 'epSOS - ePrescription Pivot');
INSERT INTO public.validator_describer (id, validator_name) VALUES (34, 'epSOS - eDispensation Friendly');
INSERT INTO public.validator_describer (id, validator_name) VALUES (35, 'epSOS - eDispensation Pivot');
INSERT INTO public.validator_describer (id, validator_name) VALUES (36, 'IHE - ITI - Basic Patient Privacy Consent (BPPC)');
INSERT INTO public.validator_describer (id, validator_name) VALUES (37, 'epSOS - eConsent');
INSERT INTO public.validator_describer (id, validator_name) VALUES (38, 'IHE - RAD - CDA document wrapper (XDS-I.b)');
SELECT pg_catalog.setval('validator_describer_id_seq', 38, true);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (7, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (9, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (10, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (10, 8);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (13, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (17, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (21, 6);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (22, 6);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (23, 6);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (20, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (19, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (18, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (25, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (26, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (27, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (28, 27);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (29, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (30, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (31, 7);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (32, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (33, 32);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (34, 10);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (35, 34);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (36, 5);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (37, 36);
INSERT INTO public.validator_association_parents_link (validator_describer_id, validator_parent_id) VALUES (38, 5);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (5, 124);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (5, 229);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (6, 124);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (6, 167);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (6, 168);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (7, 141);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (7, 140);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (8, 132);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (9, 133);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (10, 125);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (10, 127);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (10, 126);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (11, 172);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (11, 167);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (11, 168);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (11, 209);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (12, 172);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (12, 167);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (12, 168);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (13, 136);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (13, 155);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (17, 171);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (21, 179);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (21, 180);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (20, 235);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (19, 234);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (18, 236);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (24, 172);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (24, 167);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (24, 168);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (26, 227);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (26, 228);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (27, 210);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (27, 214);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (27, 237);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (28, 219);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (28, 218);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (29, 210);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (29, 218);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (29, 223);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (29, 237);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (29, 221);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (30, 223);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (30, 237);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (30, 216);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (30, 210);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (30, 218);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (31, 211);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (32, 212);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (32, 214);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (32, 237);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (33, 219);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (33, 220);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (34, 214);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (34, 215);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (34, 237);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (35, 219);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (35, 217);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (36, 139);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (36, 125);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (36, 141);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (38, 142);
INSERT INTO public.validator_describer_packages_link (validator_describer_id, package_id) VALUES (38, 141);


