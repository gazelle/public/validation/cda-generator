UPDATE cmn_application_preference SET preference_name = 'application_release_notes_url' WHERE preference_name = 'application_gazelle_release_notes_url' ;
 -- If application_release_notes_url has been manually added, then delete useless application_gazelle_release_notes_url preference.
DELETE FROM cmn_application_preference WHERE preference_name = 'application_gazelle_release_notes_url' ;

INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.String', 'URL to the application user documentation', 'documentation_url', 'https://gazelle.ihe.net/gazelle-documentation/CDA-Generator/user.html');

-- Update issue tracker url if still set to wrong 'jira' value.
UPDATE cmn_application_preference SET preference_value = 'https://gazelle.ihe.net/jira/projects/CDAGEN' WHERE preference_name = 'application_issue_tracker_url' AND preference_value = 'jira' ;

INSERT INTO cmn_application_preference (id, class_name, description, preference_name, preference_value) VALUES (nextval('cmn_application_preference_id_seq'), 'java.lang.Boolean', 'This preference allow to disable the Tools part in the menu', 'enable_tools_menu', 'true');
UPDATE building_block_repository SET ignore_templateid_requirements = false;

UPDATE building_block_repository SET version=TRIM(version);
UPDATE building_block_repository SET label=TRIM(label);
UPDATE building_block_repository SET version_label=TRIM(version_label);

-- from mbval-documentation --
ALTER TABLE mbv_package ADD COLUMN IF NOT EXISTS last_updated timestamp without time zone;
UPDATE mbv_package SET last_updated=current_timestamp;