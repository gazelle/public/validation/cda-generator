--
-- PostgreSQL database dump
--

-- Dumped from database version 14.6 (Debian 14.6-1.pgdg110+1)
-- Dumped by pg_dump version 14.10 (Ubuntu 14.10-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: building_block_repository; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.building_block_repository (
    id integer NOT NULL,
    hl7temp_cdaconffoldername character varying(255),
    active boolean,
    bbrcontent bytea,
    buildingdate timestamp without time zone,
    creationdate timestamp without time zone,
    currentstatus integer,
    ignore_templateid_requirements boolean,
    java_version integer,
    label character varying(255),
    online boolean,
    root_class_name character varying(255),
    status integer,
    url character varying(255),
    version character varying(255),
    version_label character varying(255),
    xsd_zip_file_id integer
);


ALTER TABLE public.building_block_repository OWNER TO gazelle;

--
-- Name: building_block_repository_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.building_block_repository_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.building_block_repository_id_seq OWNER TO gazelle;

--
-- Name: building_block_repository_other_files; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.building_block_repository_other_files (
    building_block_repository_id integer NOT NULL,
    otherfiles_id integer NOT NULL
);


ALTER TABLE public.building_block_repository_other_files OWNER TO gazelle;

--
-- Name: cda_doc; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cda_doc (
    id integer NOT NULL,
    author character varying(255),
    creationdate timestamp without time zone,
    path character varying(255)
);


ALTER TABLE public.cda_doc OWNER TO gazelle;

--
-- Name: cda_doc_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cda_doc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cda_doc_id_seq OWNER TO gazelle;

--
-- Name: cda_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cda_file (
    id integer NOT NULL,
    cda_file_content text,
    cda_type integer,
    creator character varying(255),
    date_creation timestamp without time zone,
    file_document integer
);


ALTER TABLE public.cda_file OWNER TO gazelle;

--
-- Name: cda_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cda_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cda_file_id_seq OWNER TO gazelle;

--
-- Name: cda_generation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cda_generation (
    id integer NOT NULL,
    affinitydomain integer,
    creator character varying(255),
    date_creation timestamp without time zone,
    author_id integer,
    dispensation_id integer,
    organization_id integer,
    prescription_id integer
);


ALTER TABLE public.cda_generation OWNER TO gazelle;

--
-- Name: cda_generation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cda_generation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cda_generation_id_seq OWNER TO gazelle;

--
-- Name: cdagen_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cdagen_object_type (
    id integer NOT NULL,
    description character varying(250),
    keyword character varying(250) NOT NULL
);


ALTER TABLE public.cdagen_object_type OWNER TO gazelle;

--
-- Name: cdagen_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cdagen_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cdagen_object_type_id_seq OWNER TO gazelle;

--
-- Name: cdagen_object_type_tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cdagen_object_type_tf_actor_integration_profile_option (
    cdagen_object_type_id integer NOT NULL,
    actorintegrationprofileoptions_id integer NOT NULL
);


ALTER TABLE public.cdagen_object_type_tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: cdagen_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cdagen_oid (
    id integer NOT NULL
);


ALTER TABLE public.cdagen_oid OWNER TO gazelle;

--
-- Name: cdagen_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cdagen_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cdagen_oid_id_seq OWNER TO gazelle;

--
-- Name: cdagen_organization; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cdagen_organization (
    id integer NOT NULL,
    aa_name character varying(255),
    extension character varying(255),
    keyword character varying(255),
    root character varying(255)
);


ALTER TABLE public.cdagen_organization OWNER TO gazelle;

--
-- Name: cdagen_organization_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cdagen_organization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cdagen_organization_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_application_preference (
    id integer NOT NULL,
    class_name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    preference_name character varying(64) NOT NULL,
    preference_value character varying(512) NOT NULL
);


ALTER TABLE public.cmn_application_preference OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_path_linking_a_document (
    id integer NOT NULL,
    comment character varying(255),
    path character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: cons_classe; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_classe (
    id integer NOT NULL,
    classtype character varying(255),
    name character varying(255),
    parentid character varying(255),
    templateid character varying(255),
    xmlname character varying(255),
    package_id integer,
    parent_id integer
);


ALTER TABLE public.cons_classe OWNER TO gazelle;

--
-- Name: cons_classe_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_classe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_classe_id_seq OWNER TO gazelle;

--
-- Name: cons_constraint; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_constraint (
    id integer NOT NULL,
    description text,
    name character varying(255),
    ocl text,
    type character varying(255),
    classe_id integer
);


ALTER TABLE public.cons_constraint OWNER TO gazelle;

--
-- Name: cons_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_constraint_id_seq OWNER TO gazelle;

--
-- Name: cons_operation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_operation (
    id integer NOT NULL,
    name character varying(255),
    returntype character varying(255),
    classe_id integer
);


ALTER TABLE public.cons_operation OWNER TO gazelle;

--
-- Name: cons_operation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_operation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_operation_id_seq OWNER TO gazelle;

--
-- Name: cons_package; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_package (
    id integer NOT NULL,
    description text,
    name character varying(255),
    packagename character varying(255),
    xmlnamespace character varying(255)
);


ALTER TABLE public.cons_package OWNER TO gazelle;

--
-- Name: cons_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_package_id_seq OWNER TO gazelle;

--
-- Name: cons_parameter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_parameter (
    id integer NOT NULL,
    name character varying(255),
    type character varying(255),
    operation_id integer
);


ALTER TABLE public.cons_parameter OWNER TO gazelle;

--
-- Name: cons_parameter_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_parameter_id_seq OWNER TO gazelle;

--
-- Name: cons_property; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cons_property (
    id integer NOT NULL,
    lower character varying(255),
    name character varying(255),
    type character varying(255),
    upper character varying(255),
    xmlfeaturekind character varying(255),
    xmlname character varying(255),
    xmlnamespace character varying(255),
    classe_id integer
);


ALTER TABLE public.cons_property OWNER TO gazelle;

--
-- Name: cons_property_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cons_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cons_property_id_seq OWNER TO gazelle;

--
-- Name: constraint_svs; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.constraint_svs (
    id integer NOT NULL,
    constraint_id character varying(255)
);


ALTER TABLE public.constraint_svs OWNER TO gazelle;

--
-- Name: constraint_svs_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.constraint_svs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.constraint_svs_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: hl7temp_validation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7temp_validation (
    id integer NOT NULL,
    ended boolean,
    name character varying(255),
    token character varying(255),
    url character varying(255)
);


ALTER TABLE public.hl7temp_validation OWNER TO gazelle;

--
-- Name: hl7temp_validation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7temp_validation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7temp_validation_id_seq OWNER TO gazelle;

--
-- Name: mbv_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_assertion (
    id integer NOT NULL,
    assertionid character varying(255),
    idscheme character varying(255),
    constraint_id integer
);


ALTER TABLE public.mbv_assertion OWNER TO gazelle;

--
-- Name: mbv_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mbv_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_assertion_id_seq OWNER TO gazelle;

--
-- Name: mbv_class_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_class_type (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    parent_identifier character varying(255),
    xml_name character varying(255),
    path character varying(255),
    templateid character varying(255),
    constraintadvanced text,
    documentation_spec_id integer,
    package_id integer
);


ALTER TABLE public.mbv_class_type OWNER TO gazelle;

--
-- Name: mbv_class_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mbv_class_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_class_type_id_seq OWNER TO gazelle;

--
-- Name: mbv_constraint; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_constraint (
    id integer NOT NULL,
    author character varying(255),
    datecreation character varying(255),
    description text,
    history text,
    kind character varying(255),
    lastchange character varying(255),
    name character varying(255),
    ocl text,
    svsref bytea,
    type character varying(255),
    classtype_id integer
);


ALTER TABLE public.mbv_constraint OWNER TO gazelle;

--
-- Name: mbv_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mbv_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_constraint_id_seq OWNER TO gazelle;

--
-- Name: mbv_documentation_spec; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_documentation_spec (
    id integer NOT NULL,
    description text,
    document character varying(255),
    name character varying(255),
    paragraph character varying(255)
);


ALTER TABLE public.mbv_documentation_spec OWNER TO gazelle;

--
-- Name: mbv_documentation_spec_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mbv_documentation_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_documentation_spec_id_seq OWNER TO gazelle;

--
-- Name: mbv_package; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_package (
    id integer NOT NULL,
    description character varying(255),
    last_updated timestamp without time zone,
    name character varying(255),
    package_name character varying(255),
    namespace character varying(255)
);


ALTER TABLE public.mbv_package OWNER TO gazelle;

--
-- Name: mbv_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.mbv_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mbv_package_id_seq OWNER TO gazelle;

--
-- Name: mbv_standards; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.mbv_standards (
    package_id integer NOT NULL,
    standards character varying(255)
);


ALTER TABLE public.mbv_standards OWNER TO gazelle;

--
-- Name: other_files; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.other_files (
    id integer NOT NULL,
    filename character varying(255),
    mimetype character varying(255)
);


ALTER TABLE public.other_files OWNER TO gazelle;

--
-- Name: other_files_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.other_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.other_files_id_seq OWNER TO gazelle;

--
-- Name: pat_patient; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    alternate_first_name character varying(255),
    alternate_last_name character varying(255),
    alternate_mothers_maiden_name character varying(255),
    alternate_second_name character varying(255),
    alternate_third_name character varying(255),
    character_set character varying(255),
    country_code character varying(255),
    creation_date timestamp without time zone,
    date_of_birth timestamp without time zone,
    first_name character varying(255),
    gender_code character varying(255),
    last_name character varying(255),
    mother_maiden_name character varying(255),
    race_code character varying(255),
    religion_code character varying(255),
    second_name character varying(255),
    size integer,
    third_name character varying(255),
    weight integer,
    creator character varying(255)
);


ALTER TABLE public.pat_patient OWNER TO gazelle;

--
-- Name: pat_patient_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient_address (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    address_line character varying(255),
    address_type integer,
    city character varying(255),
    country_code character varying(255),
    state character varying(255),
    street character varying(255),
    street_number character varying(255),
    zip_code character varying(255),
    is_main_address boolean,
    patient_id integer
);


ALTER TABLE public.pat_patient_address OWNER TO gazelle;

--
-- Name: pat_patient_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_address_id_seq OWNER TO gazelle;

--
-- Name: pat_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_id_seq OWNER TO gazelle;

--
-- Name: revinfo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


ALTER TABLE public.revinfo OWNER TO gazelle;

--
-- Name: tc_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tc_describer (
    id integer NOT NULL,
    tc_content bytea,
    tc_flatten bytea,
    tc_name character varying(255) NOT NULL,
    updatedate timestamp without time zone
);


ALTER TABLE public.tc_describer OWNER TO gazelle;

--
-- Name: tc_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tc_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tc_describer_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    combined boolean,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    combined boolean,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_actor_aud OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    maybe_supportive boolean,
    actor_integration_profile_id integer NOT NULL,
    document_section integer,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    maybe_supportive boolean,
    actor_integration_profile_id integer,
    document_section integer,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain OWNER TO gazelle;

--
-- Name: tf_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_audit_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_audit_message (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    audited_event integer,
    comment character varying(255),
    event_code_type character varying(255),
    oid character varying(255),
    audited_transaction integer,
    document_section integer,
    issuing_actor integer
);


ALTER TABLE public.tf_audit_message OWNER TO gazelle;

--
-- Name: tf_audit_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_audit_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_audit_message_id_seq OWNER TO gazelle;

--
-- Name: tf_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    document_dateofpublication timestamp without time zone,
    md5_hash_code character varying(255) NOT NULL,
    document_lifecyclestatus integer,
    document_linkstatus integer,
    document_linkstatusdescription character varying(255),
    document_name character varying(255) NOT NULL,
    document_revision character varying(255),
    document_title character varying(255),
    document_type integer NOT NULL,
    document_url character varying(255) NOT NULL,
    document_volume character varying(255),
    document_domain_id integer NOT NULL
);


ALTER TABLE public.tf_document OWNER TO gazelle;

--
-- Name: tf_document_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    document_dateofpublication timestamp without time zone,
    md5_hash_code character varying(255),
    document_lifecyclestatus integer,
    document_linkstatus integer,
    document_linkstatusdescription character varying(255),
    document_name character varying(255),
    document_revision character varying(255),
    document_title character varying(255),
    document_type integer,
    document_url character varying(255),
    document_volume character varying(255),
    document_domain_id integer
);


ALTER TABLE public.tf_document_aud OWNER TO gazelle;

--
-- Name: tf_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_id_seq OWNER TO gazelle;

--
-- Name: tf_document_sections; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    section character varying(255),
    type integer,
    document_id integer
);


ALTER TABLE public.tf_document_sections OWNER TO gazelle;

--
-- Name: tf_document_sections_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    section character varying(255),
    type integer,
    document_id integer
);


ALTER TABLE public.tf_document_sections_aud OWNER TO gazelle;

--
-- Name: tf_document_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_sections_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_domain_aud OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_profile OWNER TO gazelle;

--
-- Name: tf_domain_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile_aud (
    rev integer NOT NULL,
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_domain_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    hl7_version character varying(255) NOT NULL,
    message_order_control_code character varying(8),
    message_type character varying(15) NOT NULL,
    profile_content character varying(255),
    profile_oid character varying(64) NOT NULL,
    profile_type character varying(255),
    actor_id integer,
    domain_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain (
    hl7_message_profile_id integer NOT NULL,
    affinity_domain_id integer NOT NULL
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain_aud (
    rev integer NOT NULL,
    hl7_message_profile_id integer NOT NULL,
    affinity_domain_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    hl7_version character varying(255),
    message_order_control_code character varying(8),
    message_type character varying(15),
    profile_content character varying(255),
    profile_oid character varying(64),
    profile_type character varying(255),
    actor_id integer,
    domain_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_hl7_message_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_hl7_message_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(32) NOT NULL,
    name character varying(128),
    documentsection integer,
    integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(32),
    name character varying(128),
    documentsection integer,
    integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128),
    reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link (
    integration_profile_id integer NOT NULL,
    integration_profile_type_id integer NOT NULL
);


ALTER TABLE public.tf_integration_profile_type_link OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link_aud (
    rev integer NOT NULL,
    integration_profile_id integer NOT NULL,
    integration_profile_type_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_integration_profile_type_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    actor_integration_profile_id integer,
    transaction_id integer,
    transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link OWNER TO gazelle;

--
-- Name: tf_profile_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    actor_integration_profile_id integer,
    transaction_id integer,
    transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_profile_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_profile_link_id_seq OWNER TO gazelle;

--
-- Name: tf_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule (
    id integer NOT NULL,
    comment character varying(1024),
    cause_id integer,
    consequence_id integer
);


ALTER TABLE public.tf_rule OWNER TO gazelle;

--
-- Name: tf_rule_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_criterion (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    list_or boolean,
    single_actor_id integer,
    aiporule_id integer,
    single_integration_profile_id integer,
    single_option_id integer
);


ALTER TABLE public.tf_rule_criterion OWNER TO gazelle;

--
-- Name: tf_rule_criterion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_criterion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_criterion_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_list_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_list_criterion (
    tf_rule_criterion_id integer NOT NULL,
    aipocriterions_id integer NOT NULL,
    tf_rule_list_criterion_id integer NOT NULL
);


ALTER TABLE public.tf_rule_list_criterion OWNER TO gazelle;

--
-- Name: tf_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    keyword character varying(255),
    name text,
    network_communication_type integer,
    url text,
    version character varying(255)
);


ALTER TABLE public.tf_standard OWNER TO gazelle;

--
-- Name: tf_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    keyword character varying(255),
    name text,
    network_communication_type integer,
    url text,
    version character varying(255)
);


ALTER TABLE public.tf_standard_aud OWNER TO gazelle;

--
-- Name: tf_standard_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_standard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_standard_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    reference character varying(2048),
    documentsection integer,
    transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128),
    reference character varying(2048),
    documentsection integer,
    transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction_aud OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    from_actor_id integer,
    to_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_transaction_link OWNER TO gazelle;

--
-- Name: tf_transaction_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    from_actor_id integer,
    to_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.tf_transaction_link_aud OWNER TO gazelle;

--
-- Name: tf_transaction_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_link_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_option_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(2048),
    keyword character varying(128),
    name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_option_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_option_type_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard (
    transaction_id integer NOT NULL,
    standard_id integer NOT NULL
);


ALTER TABLE public.tf_transaction_standard OWNER TO gazelle;

--
-- Name: tf_transaction_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard_aud (
    rev integer NOT NULL,
    transaction_id integer NOT NULL,
    standard_id integer NOT NULL,
    revtype smallint
);


ALTER TABLE public.tf_transaction_standard_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    keyword character varying(32) NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE public.tf_transaction_status_type OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    description character varying(255),
    keyword character varying(32),
    name character varying(64)
);


ALTER TABLE public.tf_transaction_status_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage (
    id integer NOT NULL,
    last_changed timestamp with time zone,
    last_modifier_id character varying(255),
    usage character varying(255),
    transaction_id integer NOT NULL
);


ALTER TABLE public.tf_ws_transaction_usage OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    usage character varying(255),
    transaction_id integer
);


ALTER TABLE public.tf_ws_transaction_usage_aud OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_ws_transaction_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_ws_transaction_usage_id_seq OWNER TO gazelle;

--
-- Name: validator_association_parents_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validator_association_parents_link (
    validator_describer_id integer NOT NULL,
    validator_parent_id integer NOT NULL
);


ALTER TABLE public.validator_association_parents_link OWNER TO gazelle;

--
-- Name: validator_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validator_describer (
    id integer NOT NULL,
    validator_name character varying(255),
    tc_describer_id integer
);


ALTER TABLE public.validator_describer OWNER TO gazelle;

--
-- Name: validator_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.validator_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.validator_describer_id_seq OWNER TO gazelle;

--
-- Name: validator_describer_packages_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validator_describer_packages_link (
    validator_describer_id integer NOT NULL,
    package_id integer NOT NULL
);


ALTER TABLE public.validator_describer_packages_link OWNER TO gazelle;

--
-- Name: building_block_repository building_block_repository_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.building_block_repository
    ADD CONSTRAINT building_block_repository_pkey PRIMARY KEY (id);


--
-- Name: cda_doc cda_doc_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_doc
    ADD CONSTRAINT cda_doc_pkey PRIMARY KEY (id);


--
-- Name: cda_file cda_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_file
    ADD CONSTRAINT cda_file_pkey PRIMARY KEY (id);


--
-- Name: cda_generation cda_generation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_generation
    ADD CONSTRAINT cda_generation_pkey PRIMARY KEY (id);


--
-- Name: cdagen_object_type cdagen_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_object_type
    ADD CONSTRAINT cdagen_object_type_pkey PRIMARY KEY (id);


--
-- Name: cdagen_oid cdagen_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_oid
    ADD CONSTRAINT cdagen_oid_pkey PRIMARY KEY (id);


--
-- Name: cdagen_organization cdagen_organization_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_organization
    ADD CONSTRAINT cdagen_organization_pkey PRIMARY KEY (id);


--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_path_linking_a_document cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: cons_classe cons_classe_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_classe
    ADD CONSTRAINT cons_classe_pkey PRIMARY KEY (id);


--
-- Name: cons_constraint cons_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_constraint
    ADD CONSTRAINT cons_constraint_pkey PRIMARY KEY (id);


--
-- Name: cons_operation cons_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_operation
    ADD CONSTRAINT cons_operation_pkey PRIMARY KEY (id);


--
-- Name: cons_package cons_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_package
    ADD CONSTRAINT cons_package_pkey PRIMARY KEY (id);


--
-- Name: cons_parameter cons_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_parameter
    ADD CONSTRAINT cons_parameter_pkey PRIMARY KEY (id);


--
-- Name: cons_property cons_property_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_property
    ADD CONSTRAINT cons_property_pkey PRIMARY KEY (id);


--
-- Name: constraint_svs constraint_svs_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.constraint_svs
    ADD CONSTRAINT constraint_svs_pkey PRIMARY KEY (id);


--
-- Name: hl7temp_validation hl7temp_validation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7temp_validation
    ADD CONSTRAINT hl7temp_validation_pkey PRIMARY KEY (id);


--
-- Name: mbv_assertion mbv_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_assertion
    ADD CONSTRAINT mbv_assertion_pkey PRIMARY KEY (id);


--
-- Name: mbv_class_type mbv_class_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_class_type
    ADD CONSTRAINT mbv_class_type_pkey PRIMARY KEY (id);


--
-- Name: mbv_constraint mbv_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_constraint
    ADD CONSTRAINT mbv_constraint_pkey PRIMARY KEY (id);


--
-- Name: mbv_documentation_spec mbv_documentation_spec_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_documentation_spec
    ADD CONSTRAINT mbv_documentation_spec_pkey PRIMARY KEY (id);


--
-- Name: mbv_package mbv_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_package
    ADD CONSTRAINT mbv_package_pkey PRIMARY KEY (id);


--
-- Name: other_files other_files_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.other_files
    ADD CONSTRAINT other_files_pkey PRIMARY KEY (id);


--
-- Name: pat_patient_address pat_patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT pat_patient_address_pkey PRIMARY KEY (id);


--
-- Name: pat_patient pat_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT pat_patient_pkey PRIMARY KEY (id);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: tc_describer tc_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tc_describer
    ADD CONSTRAINT tc_describer_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_aud tf_actor_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT tf_actor_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_aud tf_actor_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT tf_actor_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option_aud tf_actor_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT tf_actor_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_affinity_domain_aud tf_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT tf_affinity_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_affinity_domain tf_affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT tf_affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_audit_message tf_audit_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT tf_audit_message_pkey PRIMARY KEY (id);


--
-- Name: tf_document_aud tf_document_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT tf_document_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document tf_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT tf_document_pkey PRIMARY KEY (id);


--
-- Name: tf_document_sections_aud tf_document_sections_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT tf_document_sections_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document_sections tf_document_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT tf_document_sections_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_aud tf_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT tf_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_profile_aud tf_domain_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT tf_domain_profile_aud_pkey PRIMARY KEY (rev, domain_id, integration_profile_id);


--
-- Name: tf_domain_profile tf_domain_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT tf_domain_profile_pkey PRIMARY KEY (domain_id, integration_profile_id);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud tf_hl7_message_profile_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT tf_hl7_message_profile_affinity_domain_aud_pkey PRIMARY KEY (rev, hl7_message_profile_id, affinity_domain_id);


--
-- Name: tf_hl7_message_profile_aud tf_hl7_message_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT tf_hl7_message_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_hl7_message_profile tf_hl7_message_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT tf_hl7_message_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_aud tf_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT tf_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option_aud tf_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT tf_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_status_type_aud tf_integration_profile_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT tf_integration_profile_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_status_type tf_integration_profile_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT tf_integration_profile_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_type_aud tf_integration_profile_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT tf_integration_profile_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_type_link_aud tf_integration_profile_type_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT tf_integration_profile_type_link_aud_pkey PRIMARY KEY (rev, integration_profile_id, integration_profile_type_id);


--
-- Name: tf_integration_profile_type tf_integration_profile_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT tf_integration_profile_type_pkey PRIMARY KEY (id);


--
-- Name: tf_profile_link_aud tf_profile_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT tf_profile_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_profile_link tf_profile_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT tf_profile_link_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_criterion tf_rule_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT tf_rule_criterion_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_list_criterion tf_rule_list_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT tf_rule_list_criterion_pkey PRIMARY KEY (tf_rule_list_criterion_id);


--
-- Name: tf_rule tf_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT tf_rule_pkey PRIMARY KEY (id);


--
-- Name: tf_standard_aud tf_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT tf_standard_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_standard tf_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT tf_standard_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_aud tf_transaction_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT tf_transaction_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link_aud tf_transaction_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT tf_transaction_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link tf_transaction_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT tf_transaction_link_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_option_type_aud tf_transaction_option_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT tf_transaction_option_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_option_type tf_transaction_option_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT tf_transaction_option_type_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_standard_aud tf_transaction_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT tf_transaction_standard_aud_pkey PRIMARY KEY (rev, transaction_id, standard_id);


--
-- Name: tf_transaction_status_type_aud tf_transaction_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT tf_transaction_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_status_type tf_transaction_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT tf_transaction_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_ws_transaction_usage_aud tf_ws_transaction_usage_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT tf_ws_transaction_usage_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_ws_transaction_usage tf_ws_transaction_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT tf_ws_transaction_usage_pkey PRIMARY KEY (id);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tf_integration_profile uk_4ojy2hqgxhoytlcrjkp9h9lr3; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_4ojy2hqgxhoytlcrjkp9h9lr3 UNIQUE (name);


--
-- Name: cmn_application_preference uk_535ry4wkukk8xivd9vqvu06hp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT uk_535ry4wkukk8xivd9vqvu06hp UNIQUE (preference_name);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: tf_transaction_status_type uk_712597f0v980ai4l7v0cg1rrh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_712597f0v980ai4l7v0cg1rrh UNIQUE (keyword);


--
-- Name: tf_rule_list_criterion uk_81shykw7emr3q41anx9ksqdb6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT uk_81shykw7emr3q41anx9ksqdb6 UNIQUE (aipocriterions_id);


--
-- Name: tf_standard uk_862df7ewvip5ajs8fpnnx9hm6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT uk_862df7ewvip5ajs8fpnnx9hm6 UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_transaction_status_type uk_8ojkw0me2bv4tsm5vyrmwb6gu; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_8ojkw0me2bv4tsm5vyrmwb6gu UNIQUE (name);


--
-- Name: tf_transaction_link uk_97iyim1a054rgjkpa32j9ksxb; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT uk_97iyim1a054rgjkpa32j9ksxb UNIQUE (from_actor_id, to_actor_id, transaction_id);


--
-- Name: tf_affinity_domain uk_9lriehprhbeo91cxqn7rqwjsy; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT uk_9lriehprhbeo91cxqn7rqwjsy UNIQUE (keyword);


--
-- Name: tf_transaction_standard uk_9xw54omnofi1bn7px13kr6jyk; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT uk_9xw54omnofi1bn7px13kr6jyk UNIQUE (standard_id, transaction_id);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_integration_profile_type_link uk_d8f4vv4juh946m3kbs3o0w7p2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT uk_d8f4vv4juh946m3kbs3o0w7p2 UNIQUE (integration_profile_id, integration_profile_type_id);


--
-- Name: cdagen_object_type uk_ds4l7bm3wb366b3rqeet98mx6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_object_type
    ADD CONSTRAINT uk_ds4l7bm3wb366b3rqeet98mx6 UNIQUE (keyword);


--
-- Name: tc_describer uk_f8auku746uye34fs00nebaa6a; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tc_describer
    ADD CONSTRAINT uk_f8auku746uye34fs00nebaa6a UNIQUE (tc_name);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: tf_transaction_option_type uk_fv74hod7rl73bhyerub82q67c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_fv74hod7rl73bhyerub82q67c UNIQUE (name);


--
-- Name: tf_document_sections uk_gqqt5kk1xa5npiaptbwwdu9bt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT uk_gqqt5kk1xa5npiaptbwwdu9bt UNIQUE (section, document_id);


--
-- Name: tf_integration_profile_status_type uk_gs2vk7vmy1ggt3p34n1f5da6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_gs2vk7vmy1ggt3p34n1f5da6 UNIQUE (keyword);


--
-- Name: tf_integration_profile_type uk_jx75w0mlp0ptnjsbiv8576nl0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT uk_jx75w0mlp0ptnjsbiv8576nl0 UNIQUE (keyword);


--
-- Name: tf_hl7_message_profile uk_k6ujf0i2mjoaf4w15wk7fq89y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT uk_k6ujf0i2mjoaf4w15wk7fq89y UNIQUE (profile_oid);


--
-- Name: tf_domain_profile uk_luennoioveseb7edrlppqaum2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT uk_luennoioveseb7edrlppqaum2 UNIQUE (domain_id, integration_profile_id);


--
-- Name: tf_profile_link uk_mei0k3s7jd4oyc3000w96e3oi; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT uk_mei0k3s7jd4oyc3000w96e3oi UNIQUE (actor_integration_profile_id, transaction_id, transaction_option_type_id);


--
-- Name: building_block_repository_other_files uk_mgvp4culbeq33mwmtmboftj39; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.building_block_repository_other_files
    ADD CONSTRAINT uk_mgvp4culbeq33mwmtmboftj39 UNIQUE (otherfiles_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: cdagen_organization uk_n2q0890gtntxq2mkj7r5y5hqj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_organization
    ADD CONSTRAINT uk_n2q0890gtntxq2mkj7r5y5hqj UNIQUE (keyword);


--
-- Name: tf_hl7_message_profile_affinity_domain uk_n5trtb5a0bt27vlsad418w4rj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT uk_n5trtb5a0bt27vlsad418w4rj UNIQUE (hl7_message_profile_id, affinity_domain_id);


--
-- Name: tf_transaction_option_type uk_nix1duwegwr24dpbqa0u7mgm9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_nix1duwegwr24dpbqa0u7mgm9 UNIQUE (keyword);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: tf_integration_profile_status_type uk_pejuyujydm4t4ylimltqlogfc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_pejuyujydm4t4ylimltqlogfc UNIQUE (name);


--
-- Name: tf_document uk_q7ne4eu5ts5lgfl3aces2ikin; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT uk_q7ne4eu5ts5lgfl3aces2ikin UNIQUE (md5_hash_code);


--
-- Name: tf_ws_transaction_usage uk_s65caasaiq9j4f8f99hikh9qm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT uk_s65caasaiq9j4f8f99hikh9qm UNIQUE (usage);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: validator_describer validator_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_describer
    ADD CONSTRAINT validator_describer_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile fk_1cdstaxi6v493qidfghxv24ry; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_1cdstaxi6v493qidfghxv24ry FOREIGN KEY (integration_profile_status_type_id) REFERENCES public.tf_integration_profile_status_type(id);


--
-- Name: tf_rule fk_1nipbe194qol7ts0abf705jv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_1nipbe194qol7ts0abf705jv4 FOREIGN KEY (consequence_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: cda_generation fk_2mwr6g1cgo0508p7h1a4fbig; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_generation
    ADD CONSTRAINT fk_2mwr6g1cgo0508p7h1a4fbig FOREIGN KEY (author_id) REFERENCES public.pat_patient(id);


--
-- Name: cda_generation fk_35n20jpl4p294d00c925cjnkw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_generation
    ADD CONSTRAINT fk_35n20jpl4p294d00c925cjnkw FOREIGN KEY (prescription_id) REFERENCES public.cda_file(id);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_37u9uwxammh3m9bg9jfueia3i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_37u9uwxammh3m9bg9jfueia3i FOREIGN KEY (affinity_domain_id) REFERENCES public.tf_affinity_domain(id);


--
-- Name: tf_transaction_standard_aud fk_384id91wu9ujfmxhkbj25hfl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT fk_384id91wu9ujfmxhkbj25hfl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud fk_3kwlpb038de40g5jrhoa83fab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT fk_3kwlpb038de40g5jrhoa83fab FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: validator_describer_packages_link fk_460xvdungubxl5e1cdj434rmj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_describer_packages_link
    ADD CONSTRAINT fk_460xvdungubxl5e1cdj434rmj FOREIGN KEY (validator_describer_id) REFERENCES public.validator_describer(id);


--
-- Name: tf_affinity_domain_aud fk_4ji8wqgg51x3x2k4jgl52y07b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT fk_4ji8wqgg51x3x2k4jgl52y07b FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_link fk_5lra5ub3x8tyhweuxa4wfv1bi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_5lra5ub3x8tyhweuxa4wfv1bi FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: validator_describer fk_5vg4tj7xt1tnlgpu7b9w8lrt7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_describer
    ADD CONSTRAINT fk_5vg4tj7xt1tnlgpu7b9w8lrt7 FOREIGN KEY (tc_describer_id) REFERENCES public.tc_describer(id);


--
-- Name: tf_domain_profile fk_5xrcq9irc0lfqkd1hfwfggh7q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_5xrcq9irc0lfqkd1hfwfggh7q FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_rule_list_criterion fk_65ptid9agnv6s0kkvttoxmtqx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_65ptid9agnv6s0kkvttoxmtqx FOREIGN KEY (tf_rule_criterion_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tf_hl7_message_profile fk_6ok2phqeqflrttiroorpowqhb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_6ok2phqeqflrttiroorpowqhb FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_integration_profile_type_link_aud fk_6qrcdc6ud9tdwmr0h3vc85un2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT fk_6qrcdc6ud9tdwmr0h3vc85un2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_criterion fk_6ybxwerfkie3n814ffvgjghvk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_6ybxwerfkie3n814ffvgjghvk FOREIGN KEY (single_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_profile_link fk_7gn0cpke6kjpbcps2iy70rvxx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_7gn0cpke6kjpbcps2iy70rvxx FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: tf_integration_profile_aud fk_7r7ffh8bhd4yj0gsn6o900j72; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT fk_7r7ffh8bhd4yj0gsn6o900j72 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_list_criterion fk_81shykw7emr3q41anx9ksqdb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_81shykw7emr3q41anx9ksqdb6 FOREIGN KEY (aipocriterions_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tf_audit_message fk_8i38nhkvxfbynrf6n14x74sni; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_8i38nhkvxfbynrf6n14x74sni FOREIGN KEY (audited_transaction) REFERENCES public.tf_transaction(id);


--
-- Name: tf_transaction fk_8u4sa8axnsn6v8jy23skc3qty; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_8u4sa8axnsn6v8jy23skc3qty FOREIGN KEY (transaction_status_type_id) REFERENCES public.tf_transaction_status_type(id);


--
-- Name: tf_profile_link fk_92x366ed20bldob29khteawg4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_92x366ed20bldob29khteawg4 FOREIGN KEY (transaction_option_type_id) REFERENCES public.tf_transaction_option_type(id);


--
-- Name: validator_describer_packages_link fk_98w27aw1xv7twr3xts4ay17mx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_describer_packages_link
    ADD CONSTRAINT fk_98w27aw1xv7twr3xts4ay17mx FOREIGN KEY (package_id) REFERENCES public.mbv_package(id);


--
-- Name: tf_hl7_message_profile fk_9by8jwr94eeb0bme1bsql5hdb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_9by8jwr94eeb0bme1bsql5hdb FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_rule fk_9cynyuydbfmqhresmj8n1uuqo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_9cynyuydbfmqhresmj8n1uuqo FOREIGN KEY (cause_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tf_transaction_link_aud fk_9gvvnh0aqrll2mp1u89a5msy0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT fk_9gvvnh0aqrll2mp1u89a5msy0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: pat_patient_address fk_9unnhbo9qfbfu2c7hfexts89g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT fk_9unnhbo9qfbfu2c7hfexts89g FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: mbv_constraint fk_a9f1f4s3giknx17lapvv09vb3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_constraint
    ADD CONSTRAINT fk_a9f1f4s3giknx17lapvv09vb3 FOREIGN KEY (classtype_id) REFERENCES public.mbv_class_type(id);


--
-- Name: tf_rule_criterion fk_ac8qpll9fw2bdtemmbkeos5pd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_ac8qpll9fw2bdtemmbkeos5pd FOREIGN KEY (single_integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: cda_generation fk_adwuop65idx314ve0g2kqi20u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_generation
    ADD CONSTRAINT fk_adwuop65idx314ve0g2kqi20u FOREIGN KEY (dispensation_id) REFERENCES public.cda_file(id);


--
-- Name: tf_transaction_aud fk_ae8odst29dbdua80lcyqyckjp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT fk_ae8odst29dbdua80lcyqyckjp FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: cons_classe fk_apaoonfoxdpmngxf2y0m2uc0t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_classe
    ADD CONSTRAINT fk_apaoonfoxdpmngxf2y0m2uc0t FOREIGN KEY (parent_id) REFERENCES public.cons_classe(id);


--
-- Name: tf_transaction_link fk_apnhy27aykxqfd0exsrxlnns7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_apnhy27aykxqfd0exsrxlnns7 FOREIGN KEY (from_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_transaction_standard fk_atvilp88b07p05km224t7n5l4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_atvilp88b07p05km224t7n5l4 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cdagen_object_type_tf_actor_integration_profile_option fk_auwbgcrusj09cnsapi7pgjkww; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_object_type_tf_actor_integration_profile_option
    ADD CONSTRAINT fk_auwbgcrusj09cnsapi7pgjkww FOREIGN KEY (actorintegrationprofileoptions_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: mbv_class_type fk_awc0r3uu9khjgxisjms3x4mih; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_class_type
    ADD CONSTRAINT fk_awc0r3uu9khjgxisjms3x4mih FOREIGN KEY (documentation_spec_id) REFERENCES public.mbv_documentation_spec(id);


--
-- Name: tf_integration_profile_type_link fk_b1w97il0injkp7gasy79nqj2q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_b1w97il0injkp7gasy79nqj2q FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: cda_generation fk_bushxbxa3w0eu4n7qcf74hrq5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cda_generation
    ADD CONSTRAINT fk_bushxbxa3w0eu4n7qcf74hrq5 FOREIGN KEY (organization_id) REFERENCES public.cdagen_organization(id);


--
-- Name: tf_document_aud fk_c8up7729eltevk3c03jfnxd8f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT fk_c8up7729eltevk3c03jfnxd8f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_criterion fk_cguvfq0yml0ppij05b9549s2e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_cguvfq0yml0ppij05b9549s2e FOREIGN KEY (single_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_integration_profile_option_aud fk_cju6k73ktqmuwinim0g8h12e0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT fk_cju6k73ktqmuwinim0g8h12e0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_domain_aud fk_d3mp0skbxicng7w2tdumqnuel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT fk_d3mp0skbxicng7w2tdumqnuel FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_actor_aud fk_d66h3bo446ibhr2oql90bmq9h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT fk_d66h3bo446ibhr2oql90bmq9h FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: mbv_standards fk_dch4reyp4qvadb9xwq495xf4q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_standards
    ADD CONSTRAINT fk_dch4reyp4qvadb9xwq495xf4q FOREIGN KEY (package_id) REFERENCES public.mbv_package(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: tf_domain_profile fk_du7myrlv6npj0053efkqu9rpg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_du7myrlv6npj0053efkqu9rpg FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_link fk_dy6dg8etr367irrt8xqoh3q9g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_dy6dg8etr367irrt8xqoh3q9g FOREIGN KEY (to_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_integration_profile_type_link fk_e1ifbmgpje5wcfsg34tuvvq83; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_e1ifbmgpje5wcfsg34tuvvq83 FOREIGN KEY (integration_profile_type_id) REFERENCES public.tf_integration_profile_type(id);


--
-- Name: tf_actor_integration_profile_aud fk_fffhdfnac6f1ry2ijq1jsehya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT fk_fffhdfnac6f1ry2ijq1jsehya FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_standard_aud fk_g07iv8pl5kcogd8i5e0vp5dpc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT fk_g07iv8pl5kcogd8i5e0vp5dpc FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_profile_link fk_gfv3euo4c19tpch111m3859t2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_gfv3euo4c19tpch111m3859t2 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_hl7_message_profile fk_gmenyb86ceqff9ujrud4p4elf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_gmenyb86ceqff9ujrud4p4elf FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cons_property fk_gpeuj8980oaqjdjkp0t5gaxkx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_property
    ADD CONSTRAINT fk_gpeuj8980oaqjdjkp0t5gaxkx FOREIGN KEY (classe_id) REFERENCES public.cons_classe(id);


--
-- Name: cons_parameter fk_hgc6h5j1vxu3n32iprb66fbg8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_parameter
    ADD CONSTRAINT fk_hgc6h5j1vxu3n32iprb66fbg8 FOREIGN KEY (operation_id) REFERENCES public.cons_operation(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_document_sections fk_iqelub40m4bwli6jc4p1gahw0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT fk_iqelub40m4bwli6jc4p1gahw0 FOREIGN KEY (document_id) REFERENCES public.tf_document(id);


--
-- Name: tf_rule_criterion fk_j241r3eqgmslvierka38b8g0n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_j241r3eqgmslvierka38b8g0n FOREIGN KEY (aiporule_id) REFERENCES public.tf_rule(id);


--
-- Name: tf_domain_profile_aud fk_j86yg7l48lurifx013ijn01hj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT fk_j86yg7l48lurifx013ijn01hj FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_option_type_aud fk_j92xo8oyqun96s7ct4dxoqj21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT fk_j92xo8oyqun96s7ct4dxoqj21 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: cons_constraint fk_jjdxaf5s11oonqt8w1pg7nyuf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_constraint
    ADD CONSTRAINT fk_jjdxaf5s11oonqt8w1pg7nyuf FOREIGN KEY (classe_id) REFERENCES public.cons_classe(id);


--
-- Name: cons_operation fk_jropkoovhnr4o1rasf62pffrv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_operation
    ADD CONSTRAINT fk_jropkoovhnr4o1rasf62pffrv FOREIGN KEY (classe_id) REFERENCES public.cons_classe(id);


--
-- Name: validator_association_parents_link fk_jrwumyqmlpmvvlgjbxf0ajdmw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_association_parents_link
    ADD CONSTRAINT fk_jrwumyqmlpmvvlgjbxf0ajdmw FOREIGN KEY (validator_describer_id) REFERENCES public.validator_describer(id);


--
-- Name: cons_classe fk_k4auxpj7r0b860cbrc0eip37x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cons_classe
    ADD CONSTRAINT fk_k4auxpj7r0b860cbrc0eip37x FOREIGN KEY (package_id) REFERENCES public.cons_package(id);


--
-- Name: mbv_class_type fk_kcyt8tgre58f5uasosbq0q12i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_class_type
    ADD CONSTRAINT fk_kcyt8tgre58f5uasosbq0q12i FOREIGN KEY (package_id) REFERENCES public.mbv_package(id);


--
-- Name: tf_document fk_kghw5fo7w5wbq5ayl4bo1hwok; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT fk_kghw5fo7w5wbq5ayl4bo1hwok FOREIGN KEY (document_domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_audit_message fk_kqshjsm4nxkadcwjw3d3csiny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_kqshjsm4nxkadcwjw3d3csiny FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: tf_integration_profile_status_type_aud fk_kuduyri7glq93006crkrnufkw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT fk_kuduyri7glq93006crkrnufkw FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_actor_integration_profile_option fk_l1fdaa86cbvmpqmp1q3f0hwcm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_l1fdaa86cbvmpqmp1q3f0hwcm FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- Name: tf_actor_integration_profile_option_aud fk_l3r8wknk5xpfwpagfodbrevi2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT fk_l3r8wknk5xpfwpagfodbrevi2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: building_block_repository_other_files fk_mgvp4culbeq33mwmtmboftj39; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.building_block_repository_other_files
    ADD CONSTRAINT fk_mgvp4culbeq33mwmtmboftj39 FOREIGN KEY (otherfiles_id) REFERENCES public.other_files(id);


--
-- Name: tf_transaction fk_nmsbssdcd0ll5g9dvehic1cdg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_nmsbssdcd0ll5g9dvehic1cdg FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: tf_integration_profile_type_aud fk_np3xu3ya9brlrhx2jxadk1gl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT fk_np3xu3ya9brlrhx2jxadk1gl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_pfco9j3tuok8p2pxb7jrqh47y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_pfco9j3tuok8p2pxb7jrqh47y FOREIGN KEY (hl7_message_profile_id) REFERENCES public.tf_hl7_message_profile(id);


--
-- Name: building_block_repository_other_files fk_qeq527bwj8sq8rdkvy9j6syh6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.building_block_repository_other_files
    ADD CONSTRAINT fk_qeq527bwj8sq8rdkvy9j6syh6 FOREIGN KEY (building_block_repository_id) REFERENCES public.building_block_repository(id);


--
-- Name: mbv_assertion fk_qn571k10ispaog0vfcunl16xr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.mbv_assertion
    ADD CONSTRAINT fk_qn571k10ispaog0vfcunl16xr FOREIGN KEY (constraint_id) REFERENCES public.mbv_constraint(id);


--
-- Name: tf_hl7_message_profile_aud fk_qx4njevahh67dplugi2iqgn9f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT fk_qx4njevahh67dplugi2iqgn9f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage_aud fk_qxy2usxp9p277j5m1gyjgrlxv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT fk_qxy2usxp9p277j5m1gyjgrlxv FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_document_sections_aud fk_rfgwvh14e22kxi49wt8rtus68; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT fk_rfgwvh14e22kxi49wt8rtus68 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_standard fk_ru5fqa00qtwrerdt1oqqsjlv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_ru5fqa00qtwrerdt1oqqsjlv4 FOREIGN KEY (standard_id) REFERENCES public.tf_standard(id);


--
-- Name: building_block_repository fk_s4guukkryj5o3wafriihwmw3d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.building_block_repository
    ADD CONSTRAINT fk_s4guukkryj5o3wafriihwmw3d FOREIGN KEY (xsd_zip_file_id) REFERENCES public.other_files(id);


--
-- Name: tf_integration_profile fk_s62lad7mgha6mk7y8d1j4ww6a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_s62lad7mgha6mk7y8d1j4ww6a FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: validator_association_parents_link fk_sh2pnghw8n33apwv6knn6uupq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validator_association_parents_link
    ADD CONSTRAINT fk_sh2pnghw8n33apwv6knn6uupq FOREIGN KEY (validator_parent_id) REFERENCES public.validator_describer(id);


--
-- Name: cdagen_object_type_tf_actor_integration_profile_option fk_t8yk11n076y9d2elamvf1b66d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cdagen_object_type_tf_actor_integration_profile_option
    ADD CONSTRAINT fk_t8yk11n076y9d2elamvf1b66d FOREIGN KEY (cdagen_object_type_id) REFERENCES public.cdagen_object_type(id);


--
-- Name: tf_profile_link_aud fk_tbg8e3v0117w55eah8dspnd4f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT fk_tbg8e3v0117w55eah8dspnd4f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_status_type_aud fk_to2vmdf96j8l3ek5jv1sf1qul; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT fk_to2vmdf96j8l3ek5jv1sf1qul FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage fk_tqey90ukaddb48sit5ikipdps; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT fk_tqey90ukaddb48sit5ikipdps FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_audit_message fk_ululbp46ig0t093pgkr38y6h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_ululbp46ig0t093pgkr38y6h FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- PostgreSQL database dump complete
--

