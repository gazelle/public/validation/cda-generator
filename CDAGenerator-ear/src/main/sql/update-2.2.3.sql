-- This command add a column for the java_version for validators
ALTER TABLE building_block_repository ADD COLUMN java_version integer NOT NULL DEFAULT 0;

-- This command set all MBVal validator to Java 1.7
update building_block_repository set java_version=0;