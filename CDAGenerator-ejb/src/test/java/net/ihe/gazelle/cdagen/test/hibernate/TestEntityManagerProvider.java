package net.ihe.gazelle.cdagen.test.hibernate;
import org.kohsuke.MetaInfServices;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;

@MetaInfServices(net.ihe.gazelle.hql.providers.EntityManagerProvider.class)
public class TestEntityManagerProvider extends AbstractEntityManagerProvider {

    @Override
    public Integer getWeight() {
        return 0;
    }

    @Override
    public String getHibernateConfigPath() {
        return AbstractTestQuery.HIBERNATE_XML;
    }

}