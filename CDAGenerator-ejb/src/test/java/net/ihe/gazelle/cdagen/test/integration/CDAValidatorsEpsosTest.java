package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.cdagen.bbr.action.BBRUtil;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.ihe.gazelle.cdagen.bbr.action.BuildingBlockRepositoryManager;
import net.ihe.gazelle.cdagen.constraint.action.FileReadWrite;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.cdagen.validator.ws.MicroDocumentValidation;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.validation.DetailedResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ApplicationPreferenceManager.class, FacesMessages.class,
	Component.class, FacesContext.class, EntityManager.class,
	EntityManagerService.class, ApplicationConfiguration.class, BBRUtil.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*", "org.apache.http.*" })
public class CDAValidatorsEpsosTest {

	EntityManager em;

	@Before
	public void setUp() throws Exception {
		em = mockEntityManager();
		mockFacesMessages();
		PowerMockito.mockStatic(FacesContext.class);
		FacesContext fc = PowerMockito.mock(FacesContext.class);
		PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(fc);
		Map<String, String> requestParameterMap = new HashMap<>();
		ExternalContext ec = PowerMockito.mock(ExternalContext.class);
		PowerMockito.when(fc.getExternalContext()).thenReturn(ec);
		PowerMockito.when(ec.getRequestParameterMap()).thenReturn(requestParameterMap);
		HttpServletResponse resp = PowerMockito.mock(HttpServletResponse.class);
		PowerMockito.when(ec.getResponse()).thenReturn(resp);
		ServletOutputStream sos = PowerMockito.mock(ServletOutputStream.class);
		PowerMockito.when(resp.getOutputStream()).thenReturn(sos);
		PowerMockito.mockStatic(ApplicationConfiguration.class);
		PowerMockito.when(ApplicationConfiguration.getValueOfVariable("svs_endpoint")).thenReturn(
				"	https://gazelle.ehdsi.eu/SVSSimulator/rest/RetrieveValueSetForSimulator");
		System.out.println("Initiating the CDA validator of the application");
		CommonOperations.setValueSetProvider(new SVSConsumer() {
			@Override
			protected String getSVSRepositoryUrl() {
				return "https://gazelle.ehdsi.eu/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		});
		initBBRLabel();
	}

	private void initBBRLabel() {
		PowerMockito.mockStatic(BBRUtil.class);
		PowerMockito.when(BBRUtil.getBBRByLabel("epSOS - HCER HealthCare Encounter Report")).thenReturn(
				"2.0.0");
	}
	private EntityManager mockEntityManager() {
		EntityManager em = PowerMockito.mock(EntityManager.class);
		PowerMockito.mockStatic(EntityManagerService.class);
		PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(em);
		return em;
	}

	private void mockFacesMessages() {
		FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
		PowerMockito.mockStatic(FacesContext.class);
		PowerMockito.mockStatic(Component.class);
		PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
	}
	
	@Test
	public void testValidateEPSOS_HCER() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_HCER/2.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSHCER(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		boolean b = false;
		for (Object object : lr) {
			if(object instanceof net.ihe.gazelle.validation.Error){
				b = true;
			}
		}
		assertTrue(b);
	}

	@Test
	public void testValidateEPSOS_MRO() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_MRO/2.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSMRO(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		boolean b = false;
		for (Object object : lr) {
			if(object instanceof net.ihe.gazelle.validation.Error){
				b = true;
			}
		}
		assertTrue(b);
	}

	@Test
	public void testValidateEPSOS_SCAN() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_SCAN/1.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSSCAN(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		for (Object object : lr) {
			assertFalse(object instanceof net.ihe.gazelle.validation.Error);
		}
	}

	@Test
	public void testValidateEPSOS_EP_PIVOT() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_EP_PIVOT/1.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSEPPIVOT(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		for (Object object : lr) {
			if (object instanceof net.ihe.gazelle.validation.Error) {
				net.ihe.gazelle.validation.Error err = (net.ihe.gazelle.validation.Error)object;
				System.err.println("err.desc = " + err.getDescription());
				System.err.println("err.loca = " + err.getLocation());
				System.err.println("err.test = " + err.getTest());
			}
			assertFalse(object instanceof net.ihe.gazelle.validation.Error);
		}
	}

	@Test
	public void testValidateEPSOS_EP_FRIENDLY() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_EP_FRIENDLY/1.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSEPFRIENDLY(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		for (Object object : lr) {
			assertFalse(object instanceof net.ihe.gazelle.validation.Error);
		}
	}

	@Test
	public void testValidateEPSOS_ED_PIVOT() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_ED_PIVOT/1.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSEDPIVOT(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		for (Object object : lr) {
			if (object instanceof net.ihe.gazelle.validation.Error) {
				net.ihe.gazelle.validation.Error err = (net.ihe.gazelle.validation.Error)object;
				System.err.println("err.desc = " + err.getDescription());
				System.err.println("err.loca = " + err.getLocation());
				System.err.println("err.test = " + err.getTest());
			}
			assertFalse(object instanceof net.ihe.gazelle.validation.Error);
		}
	}

	@Test
	public void testValidateEPSOS_ED_FRIENDLY() throws IOException {
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_ED_FRIENDLY/1.xml");
		DetailedResult dr = MicroDocumentValidation.validateEPSOSEDFRIENDLY(cdacontent);
		List<Object> lr = dr.getMDAValidation().getWarningOrErrorOrNote();
		for (Object object : lr) {
			assertFalse(object instanceof net.ihe.gazelle.validation.Error);
		}
	}
	
}
