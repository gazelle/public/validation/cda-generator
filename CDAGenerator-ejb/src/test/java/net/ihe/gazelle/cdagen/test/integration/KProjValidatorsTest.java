package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.soap.SOAPException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.validator.ws.CDAValidatorWS;
import net.ihe.gazelle.cdagen.validator.ws.Validators;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;

public class KProjValidatorsTest {

	private static Logger log = LoggerFactory.getLogger(KProjValidatorsTest.class);

	private static final String pathDoc = "src/test/resources/samples/";
	
	private static final String URL_SVS = "http://k-project.ihe-europe.net:8180/SVSSimulator/rest/RetrieveValueSetForSimulator";
	
	@Before
	public void before() {
		System.setProperty("javax.xml.xpath.XPathFactory:",
                "org.apache.xpath.jaxp.XPathFactoryImpl");
	}

	static {
		System.setProperty("javax.xml.xpath.XPathFactory:",
                "org.apache.xpath.jaxp.XPathFactoryImpl");
		System.setProperty("javax.xml.xpath.XPathFactory", "org.apache.xpath.jaxp.XPathFactoryImpl");
		System.out.println("Initiating the CDA validator of the application");
		CommonOperations.setValueSetProvider(new SVSConsumer() {
			@Override
			protected String getSVSRepositoryUrl() {
				return URL_SVS;
			}
		});
	}

	@Test
	public void validateKSA_BPPC() throws SOAPException, MalformedURLException {
		System.setProperty("javax.xml.xpath.XPathFactory:",
                "org.apache.xpath.jaxp.XPathFactoryImpl");
		Validators val = Validators.KSA_BPPC;
		testValidatorType(val);
	}

	@Test
	public void validateKSA_BSRR() throws SOAPException, MalformedURLException {
		Validators val = Validators.KSA_BSRR;
		testValidatorType(val);
	}

	@Test
	public void validateKSA_DRR() throws SOAPException, MalformedURLException {
		Validators val = Validators.KSA_DRR;
		testValidatorType(val);
	}

	@Test
	public void validateKSA_HEADER() throws SOAPException, MalformedURLException {
		Validators val = Validators.KSA_HEADER;
		testValidatorType(val);
	}

	@Test
	public void validateKSA_LABR() throws SOAPException, MalformedURLException {
		Validators val = Validators.KSA_LABR;
		testValidatorType(val);
	}

	// not testable if there is no SVS online
//	@Test
//	public void validateKSA_LABO() throws SOAPException {
//		Validators val = Validators.KSA_LABO;
//		testValidatorType(val);
//	}
	
	private void testValidatorType(Validators val) throws SOAPException, MalformedURLException {
		File file = new File(pathDoc);
		if (!verifyUrlReachable(new URL(URL_SVS))) {
			return;
		}
		if (file.isDirectory()) {
			File dir = null;
			for (File fis : file.listFiles()) {
				if (fis.getName().equals(val.toString())) {
					dir = fis;
				}
			}
			if (dir == null || !dir.isDirectory()) {
				fail("The standard was not foind : " + val.toString());
				return;
			}
			for (File fis : dir.listFiles()) {
				if (fis.getName().equals("1.xml")) {
					String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
					if (valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>")){
						log.warn(valRes);
					}
					Assert.assertFalse(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
				} else if (fis.getName().equals("2.xml")) {
					String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
					Assert.assertTrue(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
				}
			}
		} else {
			fail("The standards specified has no tests !");
		}
	}

	private String validateStandards(Validators val, String doc) throws SOAPException {
		return (new CDAValidatorWS()).validateDocument(doc, val.getValue());
	}

	private static String readDoc(String name) {
		BufferedReader scanner = null;
		StringBuilder res = new StringBuilder();
		try {
			scanner = new BufferedReader(new FileReader(name));
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				scanner.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res.toString();
	}
	
	private static boolean verifyUrlReachable(URL url){
		try{
		    final URLConnection connection = url.openConnection();
		    connection.setConnectTimeout(5000);
		    connection.connect();
		    return true;
		} catch(final MalformedURLException e){
		    return false;
		} catch(final IOException e){
		    return false;
		}
	}

}
