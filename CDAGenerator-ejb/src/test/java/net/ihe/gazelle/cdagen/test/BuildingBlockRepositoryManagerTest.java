package net.ihe.gazelle.cdagen.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import net.ihe.gazelle.cdagen.bbr.action.BuildingBlockRepositoryManager;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.OtherFile;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.utils.FileReadWrite;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApplicationPreferenceManager.class, FacesMessages.class,
        Component.class, BuildingBlockRepositoryManager.class, FacesContext.class, EntityManager.class,
        EntityManagerService.class, FileReadWrite.class, ApplicationConfiguration.class})
@PowerMockIgnore({"org.xml.*", "org.w3c.*", "javax.xml.*"})
public class BuildingBlockRepositoryManagerTest {

    EntityManager em;

    @Before
    public void setUp() throws Exception {
        em = mockEntityManager();
        mockFacesMessages();
        PowerMockito.mockStatic(FacesContext.class);
        FacesContext fc = PowerMockito.mock(FacesContext.class);
        PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(fc);
        Map<String, String> requestParameterMap = new HashMap<>();
        ExternalContext ec = PowerMockito.mock(ExternalContext.class);
        PowerMockito.when(fc.getExternalContext()).thenReturn(ec);
        PowerMockito.when(ec.getRequestParameterMap()).thenReturn(requestParameterMap);
        HttpServletResponse resp = PowerMockito.mock(HttpServletResponse.class);
        PowerMockito.when(ec.getResponse()).thenReturn(resp);
        ServletOutputStream sos = PowerMockito.mock(ServletOutputStream.class);
        PowerMockito.when(resp.getOutputStream()).thenReturn(sos);
        PowerMockito.mockStatic(ApplicationConfiguration.class);
    }

    @Test
    public void testUploadXSDFileListener() {
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setSelectedBuildingBlockRepository(new BuildingBlockRepository());
        FileUploadEvent mfue = Mockito.mock(FileUploadEvent.class);
        UploadedFile uf = Mockito.mock(UploadedFile.class);
        Mockito.when(mfue.getUploadedFile()).thenReturn(uf);
        Mockito.when(uf.getData()).thenReturn("Hello !".getBytes());
        Mockito.when(uf.getName()).thenReturn("testing");
        bbrm.uploadXSDFileListener(mfue);
        assertTrue(bbrm.getSelectedBuildingBlockRepository().getXsdZipFile().getFileName().equals("testing"));
        assertTrue(new String(bbrm.getSelectedBuildingBlockRepository().getXsdZipFile().getContent()).equals("Hello !"));
    }

    @Test
    public void testSave() {
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setSelectedBuildingBlockRepository(new BuildingBlockRepository());
        bbrm.setEditMode(true);
        Mockito.when(em.merge(bbrm.getSelectedBuildingBlockRepository())).thenReturn(bbrm.getSelectedBuildingBlockRepository());
        String res = bbrm.save();
        assertTrue(res.equals("/admin/buildingBlockRepository/list.seam"));
        assertTrue(bbrm.getSelectedBuildingBlockRepository().getStatus() == null);
    }

    @Test
    public void testInitSelectedBuildingBlockRepository1() throws Exception {
        FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().clear();
        FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().put("bbrId", "5");
        BuildingBlockRepository bbr = new BuildingBlockRepository();
        bbr.setLabel("test");
        Mockito.when(em.find(BuildingBlockRepository.class, 5)).thenReturn(bbr);
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setEditMode(true);
        bbrm.initSelectedBuildingBlockRepository();
        assertTrue(bbrm.getSelectedBuildingBlockRepository().getLabel().equals("test"));
        assertTrue(bbrm.isEditMode());
    }

    @Test
    public void testInitSelectedBuildingBlockRepository2() throws Exception {
        FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().clear();
        BuildingBlockRepository bbr = new BuildingBlockRepository();
        bbr.setLabel("test");
        Mockito.when(em.find(BuildingBlockRepository.class, 5)).thenReturn(bbr);
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setEditMode(true);
        bbrm.initSelectedBuildingBlockRepository();
        assertTrue(bbrm.getSelectedBuildingBlockRepository().getLabel() == null);
        assertFalse(bbrm.isEditMode());
    }

    @Test
    public void testUploadBbrListener() throws Exception {
        FileUploadEvent fue = PowerMockito.mock(FileUploadEvent.class);
        UploadedFile uf = PowerMockito.mock(UploadedFile.class);
        Mockito.when(fue.getUploadedFile()).thenReturn(uf);
        Mockito.when(uf.getData()).thenReturn("test".getBytes());
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setSelectedBuildingBlockRepository(new BuildingBlockRepository());
        bbrm.uploadBbrListener(fue);
        assertTrue(new String(bbrm.getSelectedBuildingBlockRepository().getBbrContent()).equals("test"));
    }

    @Test
    public void testDisplayLogs() throws Exception {
        PowerMockito.mockStatic(ApplicationPreferenceManager.class);
        PowerMockito.when(ApplicationPreferenceManager.getStringValue("bbr_folder")).thenReturn("/hello");
        PowerMockito.mockStatic(BuildingBlockRepositoryManager.class);
        PowerMockito.when(BuildingBlockRepositoryManager.readBashLogs("/hello/generatedValidators/execs/3/logs.txt")).thenReturn("hello all");
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setSelectedBuildingBlockRepository(new BuildingBlockRepository());
        bbrm.getSelectedBuildingBlockRepository().setId(3);
        String res = bbrm.displayLogs();
        assertTrue(res.equals("hello all"));
    }

    @Test
    public void testDownloadXSDFile() throws Exception {
        BuildingBlockRepository bbr = new BuildingBlockRepository();
        bbr.setXsdZipFile(new OtherFile());
        bbr.getXsdZipFile().setFileName("RFC3881.xsd.zip");
        byte[] zip = FileUtils.readFileToByteArray(new File("src/test/resources/RFC3881.xsd.zip"));
        bbr.getXsdZipFile().setContent(zip);
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.downloadXSDFile(bbr);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        InOrder inorder = Mockito.inOrder(response, servletOutputStream);
        inorder.verify(response).getOutputStream();
        inorder.verify(servletOutputStream).write(zip);
        inorder.verify(servletOutputStream).flush();
    }

    @Test
    public void testDeleteXSDFile() throws Exception {
        BuildingBlockRepository bbr = new BuildingBlockRepository();
        bbr.setXsdZipFile(new OtherFile());
        bbr.getXsdZipFile().setFileName("RFC3881.xsd.zip");
        PowerMockito.when(ApplicationConfiguration.getValueOfVariable("other_files_path")).thenReturn("/hello");
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.deleteXSDFile(bbr);
        assertTrue(bbr.getXsdZipFile() == null);
    }

    @Test
    public void testDeleteSelectedBuildingBlockRepository() throws Exception {
        BuildingBlockRepositoryManager bbrm = new BuildingBlockRepositoryManager();
        bbrm.setSelectedBuildingBlockRepository(new BuildingBlockRepository());
        bbrm.getSelectedBuildingBlockRepository().setId(33);
        Mockito.when(em.find(BuildingBlockRepository.class, 33)).thenReturn(bbrm.getSelectedBuildingBlockRepository());
        bbrm.deleteSelectedBuildingBlockRepository();
        InOrder inorder = Mockito.inOrder(em);
        inorder.verify(em).remove(bbrm.getSelectedBuildingBlockRepository());
    }

    private EntityManager mockEntityManager() {
        EntityManager em = PowerMockito.mock(EntityManager.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(em);
        return em;
    }

    private void mockFacesMessages() {
        FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(Component.class);
        PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
    }

}
