package net.ihe.gazelle.cdagen.script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestExec {
	
	public static void main(String[] args) throws IOException {
		InputStream is = Runtime.getRuntime().exec("/./bin/sh /home/aboufahj/workspaceTopcased/hl7templates-validator-app/bin/CDATemplatesValidator.sh -out \"/opt/CDAGenerator/hl7tempValidation/7\" -bbr \"http://kujira.irisa.fr/decor/services/RetrieveProject?prefix=RCS-C-&mode=compiled&language=en-US&download=true\" -prb").getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader buff = new BufferedReader (isr);

		String line;
		while((line = buff.readLine()) != null) {
		    System.out.println(line);
		}
	}

}
