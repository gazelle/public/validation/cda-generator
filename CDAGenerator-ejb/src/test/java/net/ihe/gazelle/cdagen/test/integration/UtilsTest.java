package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.assertTrue;

import java.nio.charset.Charset;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testCharset() {
		Charset chars  = Charset.forName("UTF8");
		assertTrue(chars != null);
		System.out.println("charset :: " + chars.name());
	}

}
