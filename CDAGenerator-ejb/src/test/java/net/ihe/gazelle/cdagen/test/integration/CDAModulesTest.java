package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.cdagen.bbr.action.BBRUtil;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.ihe.gazelle.ccd.ccd.CCDTemplatesPackValidator;
import net.ihe.gazelle.cdagen.bbr.action.BuildingBlockRepositoryManager;
import net.ihe.gazelle.cdagen.constraint.action.FileReadWrite;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.cdagen.validator.ws.MicroDocumentValidation;
import net.ihe.gazelle.cdagen.validator.ws.Validators;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.crc.crc.CRCPackValidator;
import net.ihe.gazelle.epsos.common.COMMONPackValidator;
import net.ihe.gazelle.epsos.edispensation.EDISPENSATIONPackValidator;
import net.ihe.gazelle.epsos.edispensationpivot.EDISPENSATIONPIVOTPackValidator;
import net.ihe.gazelle.epsos.eedcommon.EEDCOMMONPackValidator;
import net.ihe.gazelle.epsos.eprescription.EPRESCRIPTIONPackValidator;
import net.ihe.gazelle.epsos.eprescriptionpivot.EPRESCRIPTIONPIVOTPackValidator;
import net.ihe.gazelle.epsos.epsostemplates.EPSOSTEMPLATESPackValidator;
import net.ihe.gazelle.epsos.hcer.HCERPackValidator;
import net.ihe.gazelle.epsos.mro.MROPackValidator;
import net.ihe.gazelle.epsos.patientsummary.PATIENTSUMMARYPackValidator;
import net.ihe.gazelle.epsos.patientsummarypivot.PATIENTSUMMARYPIVOTPackValidator;
import net.ihe.gazelle.epsos.scanneddoc.SCANNEDDOCPackValidator;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.lux.luxh.LUXHPackValidator;
import net.ihe.gazelle.pcc.pccdocument.PCCDOCUMENTTemplatesPackValidator;
import net.ihe.gazelle.pcc.pccentry.PCCENTRYTemplatesPackValidator;
import net.ihe.gazelle.pcc.pccsection.PCCSECTIONTemplatesPackValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdlab.xdlabspec.XDLABSPECPackValidator;
import net.ihe.gazelle.xdssd.xdssd.XDSSDPackValidator;
import net.ihe.gazelle.xdssd.xdssd.XDSSDTemplatesPackValidator;
import net.ihe.gazelle.xdssd.xdssdbppc.XDSSDBPPCPackValidator;
import net.ihe.gazelle.xdssd.xdssdrad.XDSSDRADPackValidator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ApplicationPreferenceManager.class, FacesMessages.class,
	Component.class, FacesContext.class, EntityManager.class,
	EntityManagerService.class, ApplicationConfiguration.class, BBRUtil.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*", "org.apache.http.*" })
public class CDAModulesTest {

	EntityManager em;

	@Before
	public void setUp() throws Exception {
		em = mockEntityManager();
		mockFacesMessages();
		PowerMockito.mockStatic(FacesContext.class);
		FacesContext fc = PowerMockito.mock(FacesContext.class);
		PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(fc);
		Map<String, String> requestParameterMap = new HashMap<>();
		ExternalContext ec = PowerMockito.mock(ExternalContext.class);
		PowerMockito.when(fc.getExternalContext()).thenReturn(ec);
		PowerMockito.when(ec.getRequestParameterMap()).thenReturn(requestParameterMap);
		HttpServletResponse resp = PowerMockito.mock(HttpServletResponse.class);
		PowerMockito.when(ec.getResponse()).thenReturn(resp);
		ServletOutputStream sos = PowerMockito.mock(ServletOutputStream.class);
		PowerMockito.when(resp.getOutputStream()).thenReturn(sos);
		PowerMockito.mockStatic(ApplicationConfiguration.class);
		PowerMockito.when(ApplicationConfiguration.getValueOfVariable("svs_endpoint")).thenReturn(
				"	https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
		PowerMockito.when(ApplicationConfiguration.getValueOfVariable("cda_xsd")).thenReturn(
				"src/test/resources/LabCDA.xsd");
		PowerMockito.when(ApplicationConfiguration.getValueOfVariable("cdaepsos_xsd")).thenReturn(
				"src/test/resources/CDA_extended.xsd");
		PowerMockito.when(ApplicationConfiguration.getValueOfVariable("cdapharm_xsd")).thenReturn(
				"src/test/resources/CDA_extended_pharmacy.xsd");
		initBBRLabel();
	}

	private void initBBRLabel() {
		PowerMockito.mockStatic(BBRUtil.class);
		PowerMockito.when(BBRUtil.getBBRByLabel("epSOS - HCER HealthCare Encounter Report")).thenReturn(
				"2.0.0");
	}

	private EntityManager mockEntityManager() {
		EntityManager em = PowerMockito.mock(EntityManager.class);
		PowerMockito.mockStatic(EntityManagerService.class);
		PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(em);
		return em;
	}

	private void mockFacesMessages() {
		FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
		PowerMockito.mockStatic(FacesContext.class);
		PowerMockito.mockStatic(Component.class);
		PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
	}
	
	@Test
	public void testXDLABSPECPackValidator() throws IOException {
		XDLABSPECPackValidator xd = new XDLABSPECPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/IHE_XDLAB/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		List<Notification> ln = new ArrayList<Notification>();
		DetailedResult dr = MicroDocumentValidation.validateWithCDALABValidator(cdacontent, Validators.IHE_XDLAB, lis,
				null, ln);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"XDLABSpec"));
	}


	@Test
	public void testPATIENTSUMMARYPackValidator() throws IOException {
		PATIENTSUMMARYPackValidator xd = new PATIENTSUMMARYPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_PS_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_PS_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"PatientSummary-"));
	}

	@Test
	public void testCOMMONPackValidator() throws IOException {
		COMMONPackValidator xd = new COMMONPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_PS_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent,
				Validators.EPSOS_PS_PIVOT, lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"Common-"));
	}

	@Test
	public void testPATIENTSUMMARYPIVOTPackValidator() throws IOException {
		PATIENTSUMMARYPIVOTPackValidator xd = new PATIENTSUMMARYPIVOTPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_PS_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent,
				Validators.EPSOS_PS_PIVOT, lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"PatientSummaryPivot-"));
	}

	@Test
	public void testEPSOSTEMPLATESPackValidator() throws IOException {
		EPSOSTEMPLATESPackValidator xd = new EPSOSTEMPLATESPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_PS_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_PS_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"epSOSTemplates-"));
	}

	@Test
	public void testCCDTemplatesPackValidator() throws IOException {
		CCDTemplatesPackValidator xd = new CCDTemplatesPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/CCD_BASIC/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent, Validators.CCD_BASIC,
				lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"ccd-"));
	}

	@Test
	public void testEEDCOMMONPackValidator() throws IOException {
		EEDCOMMONPackValidator xd = new EEDCOMMONPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_HCER/2.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent, Validators.EPSOS_HCER,
				lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"EEDCommon-"));
	}

	@Test
	public void testHCERPackValidator() throws IOException {
		HCERPackValidator xd = new HCERPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_HCER/2.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent, Validators.EPSOS_HCER,
				lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"hcer-"));
	}

	@Test
	public void testMROPackValidator() throws IOException {
		MROPackValidator xd = new MROPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_MRO/2.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent, Validators.EPSOS_MRO,
				lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"mro-"));
	}

	@Test
	public void testSCANNEDDOCPackValidator() throws IOException {
		SCANNEDDOCPackValidator xd = new SCANNEDDOCPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_SCAN/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent, Validators.EPSOS_SCAN,
				lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"ScannedDoc-"));
	}

	@Test
	public void testXDSSDBPPCPackValidator() throws IOException {
		XDSSDBPPCPackValidator xd = new XDSSDBPPCPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/IHE_BPPC/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent, Validators.IHE_BPPC, lis,
				null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"xdssdbppc-"));
	}

	@Test
	public void testXDSSDRADPackValidator() throws IOException {
		XDSSDRADPackValidator xd = new XDSSDRADPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/IHE_XDS_SD_XDSIB/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent,
				Validators.IHE_XDS_SD_XDSIB, lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"xdssdrad-"));
	}

	@Test
	public void testEPRESCRIPTIONPackValidator() throws IOException {
		EPRESCRIPTIONPackValidator xd = new EPRESCRIPTIONPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_EP_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_EP_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"ePrescription-"));
	}

	@Test
	public void testEPRESCRIPTIONPIVOTPackValidator() throws IOException {
		EPRESCRIPTIONPIVOTPackValidator xd = new EPRESCRIPTIONPIVOTPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_EP_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_EP_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"ePrescriptionPivot-"));
	}

	@Test
	public void testEDISPENSATIONPackValidator() throws IOException {
		EDISPENSATIONPackValidator xd = new EDISPENSATIONPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_ED_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_ED_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"eDispensation-"));
	}

	@Test
	public void testEDISPENSATIONPIVOTPackValidator() throws IOException {
		EDISPENSATIONPIVOTPackValidator xd = new EDISPENSATIONPIVOTPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/EPSOS_ED_PIVOT/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithCDAEPSOSValidator(cdacontent,
				Validators.EPSOS_ED_PIVOT, lis, null, new ArrayList<Notification>());
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"eDispensationPivot-"));
	}

	@Test
	public void testIHECRCTemplatesPackValidator() throws IOException {
		CRCPackValidator xd = new CRCPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/IHE_CRC/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent, Validators.IHE_CRC, lis,
				null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"crc-"));
	}

	@Test
	public void testLUXHTemplatesPackValidator() throws IOException {
		LUXHPackValidator xd = new LUXHPackValidator();
		String cdacontent = FileReadWrite.readDoc("src/test/resources/samples/LUX_HEADER/1.xml");
		List<ConstraintValidatorModule> lis = new ArrayList<ConstraintValidatorModule>();
		lis.add(xd);
		DetailedResult dr = MicroDocumentValidation.validateWithBasicCDAValidator(cdacontent, Validators.LUX_HEADER,
				lis, null);
		assertTrue(((Notification) dr.getMDAValidation().getWarningOrErrorOrNote().get(0)).getIdentifiant().contains(
				"luxh-"));
	}

}
