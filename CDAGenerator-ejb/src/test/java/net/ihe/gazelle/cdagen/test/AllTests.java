package net.ihe.gazelle.cdagen.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import net.ihe.gazelle.cdagen.test.integration.CDAModulesTest;
import net.ihe.gazelle.cdagen.test.integration.CDAValidatorsTest;
import net.ihe.gazelle.cdagen.test.integration.StandardsValidatorsTest;
import net.ihe.gazelle.cdagen.test.integration.UtilsTest;

@RunWith(Suite.class)
@SuiteClasses({CDAModulesTest.class, CDAValidatorsTest.class, StandardsValidatorsTest.class, UtilsTest.class})
public class AllTests {
	
}
