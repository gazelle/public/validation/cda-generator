package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.cdagen.bbr.action.BBRUtil;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.validator.ws.CDAValidatorWS;
import net.ihe.gazelle.cdagen.validator.ws.MicroDocumentValidation;
import net.ihe.gazelle.cdagen.validator.ws.Validators;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({BBRUtil.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*", "org.apache.http.*" })

public class StandardsValidatorsTest {

	private static Logger log = LoggerFactory.getLogger(StandardsValidatorsTest.class);

	private static final String pathDoc = "src/test/resources/samples/";
	
	@Before
	public void before() {
		System.setProperty("javax.xml.xpath.XPathFactory:",
                "org.apache.xpath.jaxp.XPathFactoryImpl");
		initBBRLabel();
	}

	private void initBBRLabel() {
		PowerMockito.mockStatic(BBRUtil.class);
		PowerMockito.when(BBRUtil.getBBRByLabel("epSOS - HCER HealthCare Encounter Report")).thenReturn(
				"2.0.0");
	}

	static {
		System.setProperty("javax.xml.xpath.XPathFactory:",
                "org.apache.xpath.jaxp.XPathFactoryImpl");
		try {
			MicroDocumentValidation.evaluate(null, null, null);
		} catch (Exception e) {

		}
		System.out.println("Initiating the CDA validator of the application");
		CommonOperations.setValueSetProvider(new SVSConsumer() {
			@Override
			protected String getSVSRepositoryUrl() {
				return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		});
	}

//	@Test
//	public void validateIHE_BASIC_CDA() throws SOAPException {
//		Validators val = Validators.IHE_BASIC_CDA;
//		testValidatorType(val);
//	}

	@Test
	public void validateIHE_XDS_SD_XDSIB() throws SOAPException {
		Validators val = Validators.IHE_XDS_SD_XDSIB;
		testValidatorType(val);
	}


	@Test
	public void validateASIP_FRCP() throws SOAPException {
		Validators val = Validators.ASIP_FRCP;
		testValidatorType(val);
	}


	private void testValidatorType(Validators val) throws SOAPException {
		File file = new File(pathDoc);
		if (file.isDirectory()) {
			File dir = null;
			for (File fis : file.listFiles()) {
				if (fis.getName().equals(val.toString())) {
					dir = fis;
				}
			}
			if (dir == null || !dir.isDirectory()) {
				fail("The standard was not found : " + val.toString());
				return;
			}
			for (File fis : dir.listFiles()) {
				if (fis.getName().equals("1.xml")) {
					String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
					if (valRes.contains("<Error>")){
						log.warn(valRes);
					}
					Assert.assertFalse(valRes.contains("<Error>"));
				} else if (fis.getName().equals("2.xml")) {
					String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
					Assert.assertTrue(valRes.contains("<Error>"));
				}
			}
		} else {
			fail("The standards specified has no tests !");
		}
	}

	private String validateStandards(Validators val, String doc) throws SOAPException {
		return (new CDAValidatorWS()).validateDocument(doc, val.getValue());
	}

	private static String readDoc(String name) {
		BufferedReader scanner = null;
		StringBuilder res = new StringBuilder();
		try {
			scanner = new BufferedReader(new FileReader(name));
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				scanner.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res.toString();
	}

}
