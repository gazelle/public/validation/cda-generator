<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2" xmlns:cda="urn:hl7-org:v3">
    <ns prefix="cda" uri="urn:hl7-org:v3"/>
    <pattern id="common-cda-components">
        <rule context="/cda:ClinicalDocument">
            <assert test="every $i in distinct-values(for $value in //cda:reference/@value return if(starts-with( $value, '#') and count(//@ID[(self::node() = substring($value, 2))]) = 0) then false() else true()) satisfies $i = true() ">Error: Cannot find all the IDs referenced. Missing IDs are : <value-of
                select="distinct-values(for $value in //cda:reference/@value return if (starts-with( $value, '#') and count(//@ID[(self::node() = substring($value, 2))] ) = 0) then concat(substring($value, 2), ',') else comment())"/> </assert>
            <report test="every $i in distinct-values(for $value in //cda:reference/@value return if(starts-with( $value, '#') and count(//@ID[(self::node() = substring($value, 2))]) = 0) then false() else true()) satisfies $i = true() ">Success: Found all IDs referenced</report>
        </rule>
    </pattern>
</schema>