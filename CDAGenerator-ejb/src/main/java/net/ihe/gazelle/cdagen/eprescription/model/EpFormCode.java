package net.ihe.gazelle.cdagen.eprescription.model;

public class EpFormCode {
    
    public String code;
    public String codeSystem;
    public String codeSystemName;
    public String displayName;
    
    public EpFormCode(String code, String codeSystem, String codeSystemName,
            String displayName) {
        super();
        this.code = code;
        this.codeSystem = codeSystem;
        this.codeSystemName = codeSystemName;
        this.displayName = displayName;
    }
    
    

}
