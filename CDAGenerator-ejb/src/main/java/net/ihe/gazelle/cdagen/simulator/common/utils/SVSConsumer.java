package net.ihe.gazelle.cdagen.simulator.common.utils;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.cdagen.simulator.common.model.Concept;
import net.ihe.gazelle.common.model.ApplicationPreference;

public class SVSConsumer {


    private static final String CODE_SYSTEM_NAME = "codeSystemName";
    private static final String CODE_SYSTEM = "codeSystem";
    private static final String DISPLAY_NAME = "displayName";
    private static Logger log = LoggerFactory.getLogger(SVSConsumer.class);

    /**
     * @param valueSetId id of VS
     * @param lang       lang
     */
    public static Concept getRandomConceptFromValueSet(String valueSetId, String lang) {
        String svsRepository = getSVSRepositoryUrl();
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSet"));
        request.queryParameter("id", valueSetId);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        request.queryParameter("random", "true");

        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    Node conceptNode = concepts.item(0);
                    NamedNodeMap attributes = conceptNode.getAttributes();
                    return new Concept(attributes.getNamedItem("code").getTextContent(),
                            attributes.getNamedItem(DISPLAY_NAME).getTextContent(),
                            attributes.getNamedItem(CODE_SYSTEM).getTextContent(),
                            attributes.getNamedItem(CODE_SYSTEM_NAME).getTextContent());
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Problem to process the generation", e);
        }
        return null;
    }

    public static List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
        String svsRepository = getSVSRepositoryUrl();
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSet"));
        request.queryParameter("id", valueSetId);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }
        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    List<Concept> conceptsList = new ArrayList<Concept>();
                    for (int index = 0; index < concepts.getLength(); index++) {
                        Node conceptNode = concepts.item(index);
                        NamedNodeMap attributes = conceptNode.getAttributes();
                        Concept aa = new Concept();
                        if (attributes.getNamedItem("code") != null) {
                            aa.setCode(attributes.getNamedItem("code").getTextContent());
                        }
                        if (attributes.getNamedItem(DISPLAY_NAME) != null) {
                            aa.setDisplayName(attributes.getNamedItem(DISPLAY_NAME).getTextContent());
                        }
                        if (attributes.getNamedItem(CODE_SYSTEM) != null) {
                            aa.setCodeSystem(attributes.getNamedItem(CODE_SYSTEM).getTextContent());
                        }
                        if (attributes.getNamedItem(CODE_SYSTEM_NAME) != null) {
                            aa.setCodeSystemName(attributes.getNamedItem(CODE_SYSTEM_NAME).getTextContent());
                        }

                        conceptsList.add(aa);
                    }
                    return conceptsList;
                }
            }
            return null;
        } catch (Exception e) {
            log.error("Problem to process the extraction", e);
        }
        return null;
    }

    /**
     * Returns the displayName attribute associate to a given code from a given value set
     *
     * @param valueSetId id in string
     * @param lang land
     * @param code code
     * @return the displayName attribute associate to a given code from a given value set
     */
    public static String getDisplayNameForGivenCode(String valueSetId, String lang, String code) {
        String svsRepository = getSVSRepositoryUrl();
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSet"));
        request.queryParameter("id", valueSetId);
        request.queryParameter("code", code);
        if (lang != null && !lang.isEmpty()) {
            request.queryParameter("lang", lang);
        }

        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0) {
                    Node conceptNode = concepts.item(0);
                    NamedNodeMap attributes = conceptNode.getAttributes();
                    return attributes.getNamedItem(DISPLAY_NAME).getTextContent();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Problem to extract displayName", e);
        }
        return null;
    }

    public static String getCodeSetAsXmlString(String valueSetId) {
        if (valueSetId == null || valueSetId.isEmpty()) {
            return null;
        }
        String svsRepository = getSVSRepositoryUrl();
        ClientRequest request = new ClientRequest(svsRepository.concat("/RetrieveValueSet"));
        request.queryParameter("id", valueSetId);
        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200) {
                return response.getEntity();
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Problem to extract code set", e);
            return null;
        }
    }

    private static String getSVSRepositoryUrl() {
        String svsRepository = ApplicationConfiguration.getValueOfVariable("svs_repository_url");
        if (svsRepository == null) {
            ApplicationPreference pref = new ApplicationPreference();
            pref.setClassName("java.lang.String");
            pref.setDescription("");
            pref.setPreferenceName("svs_repository_url");
            pref.setPreferenceValue("http://gazelle.ihe.net");
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.merge(pref);
            em.flush();
            svsRepository = pref.getPreferenceValue();
        }
        return svsRepository;
    }
}
