package net.ihe.gazelle.cdagen.constraint.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class FileReadWrite {
	
	private static Logger log = LoggerFactory.getLogger(FileReadWrite.class);

	private FileReadWrite() {}
	
	public static void printDoc(String doc, String name)  {
		try (FileWriter fw = new FileWriter(new File(name))){
			fw.append(doc);
		} catch (IOException e) {
			log.info("Problem to write doc in the file:" + name, e);
		}
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
