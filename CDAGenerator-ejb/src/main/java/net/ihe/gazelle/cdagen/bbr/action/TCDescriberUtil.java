package net.ihe.gazelle.cdagen.bbr.action;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.jfree.util.Log;

import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.scoring.model.templates.TemplatesContainment;
import net.ihe.gazelle.scoring.richness.util.MMExtractor;
import net.ihe.gazelle.scoring.richness.util.TemplatesContainmentMarshaller;

public final class TCDescriberUtil {

	private static final String ATTACHMENT_FILENAME = "attachment;filename=\"";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	private TCDescriberUtil() {
		// private constructor
	}

	public static TemplatesContainment extractTemplateContainment(TCDescriber tcd) {
		if (tcd != null && tcd.getTcContent() != null) {
			try {
				return TemplatesContainmentMarshaller
						.loadTemplatesContainment(new ByteArrayInputStream(tcd.getTcContent()));
			} catch (JAXBException e) {
				Log.error("Problem to extract TC", e);
			}
		}
		return null;
	}

	public static TemplatesContainment extractTemplateContainmentFlattened(TCDescriber tcd) {
		if (tcd != null && tcd.getTcFlattened() != null) {
			try {
				return TemplatesContainmentMarshaller
						.loadTemplatesContainment(new ByteArrayInputStream(tcd.getTcFlattened()));
			} catch (JAXBException e) {
				Log.error("Problem to extract Flatened TC", e);
			}
		}
		return null;
	}

	public static void downloadTemplatesContainmentAsMM(TemplatesContainment tc, String selectedTemplateId,
			String tcName) throws IOException, JAXBException {
		if (tc != null && selectedTemplateId != null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/xml");
			String mm = MMExtractor.extractMM(tc, selectedTemplateId);
			mm = mm.substring(mm.indexOf("?>") + 2).trim();
			response.setContentLength(mm.getBytes().length);
			response.setHeader(CONTENT_DISPOSITION,
					ATTACHMENT_FILENAME + tcName + "_" + selectedTemplateId + "_flattened_asMM.mm" + "\"");
			servletOutputStream.write(mm.getBytes());
			servletOutputStream.flush();
			servletOutputStream.close();
			FacesContext.getCurrentInstance().responseComplete();
		}
	}

}
