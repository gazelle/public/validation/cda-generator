package net.ihe.gazelle.cdagen.datamodel;

import net.ihe.gazelle.cdagen.hl7templates.validation.HL7TemplValidation;
import net.ihe.gazelle.cdagen.hl7templates.validation.HL7TemplValidationQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

public class HL7TemplValidationDataModel extends FilterDataModel<HL7TemplValidation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HL7TemplValidationDataModel() {
		super(new Filter<HL7TemplValidation>(getCriterionList()));
	}

	private static HQLCriterionsForFilter<HL7TemplValidation> getCriterionList() {
		HL7TemplValidationQuery query = new HL7TemplValidationQuery();
		return query.getHQLCriterionsForFilter();
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<HL7TemplValidation> queryBuilder) {
		// nothin to append
	}

	@Override
	protected Object getId(HL7TemplValidation t) {
		return t.getId();
	}
}
