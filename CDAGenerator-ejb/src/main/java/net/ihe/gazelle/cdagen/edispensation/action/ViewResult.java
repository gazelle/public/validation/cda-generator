package net.ihe.gazelle.cdagen.edispensation.action;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Name("viewResultBean")
@Scope(ScopeType.SESSION)
public class ViewResult implements ViewResultLocal{
    
    @Logger
    private static Log log;
    
    private String htmlResultValidation;
    
    private String ePrescriptionHtml1;
    
    private String ePrescriptionHtml2;
    
    private String eDispensationHtml1;
    
    private String eDispensationHtml2;

    public String getHtmlResultValidation() {
        return htmlResultValidation;
    }

    public void setHtmlResultValidation(String htmlResultValidation) {
        this.htmlResultValidation = htmlResultValidation;
    }
    
    
    
    public String getePrescriptionHtml1() {
        return ePrescriptionHtml1;
    }

    public void setePrescriptionHtml1(String ePrescriptionHtml1) {
        this.ePrescriptionHtml1 = ePrescriptionHtml1;
    }

    public String getePrescriptionHtml2() {
        return ePrescriptionHtml2;
    }

    public void setePrescriptionHtml2(String ePrescriptionHtml2) {
        this.ePrescriptionHtml2 = ePrescriptionHtml2;
    }

    public String geteDispensationHtml1() {
        return eDispensationHtml1;
    }

    public void seteDispensationHtml1(String eDispensationHtml1) {
        this.eDispensationHtml1 = eDispensationHtml1;
    }

    public String geteDispensationHtml2() {
        return eDispensationHtml2;
    }

    public void seteDispensationHtml2(String eDispensationHtml2) {
        this.eDispensationHtml2 = eDispensationHtml2;
    }

    @Destroy
    @Remove
    public void destroy(){
        log.info("destroy");
    }
    
}
