package net.ihe.gazelle.cdagen.validator.ws;

public class InternalValidationExeption extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InternalValidationExeption() {
		// public constr
	}
	
	public InternalValidationExeption(String str) {
		super(str);
	}

}
