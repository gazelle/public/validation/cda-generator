package net.ihe.gazelle.cdagen.bbr.action;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.cdagen.bbr.model.JavaVersionEnum;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.async.Asynchronous;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.utils.FileReadWrite;

@Stateless
@Name("processBBR")
public class ProcessBBR implements ProcessBBRInterface {

    private static final String GENERATED_VALIDATORS_EXECS_FOLDER = "/generatedValidators/execs/";
    private static final Logger LOG = LoggerFactory.getLogger(ProcessBBR.class);

    public static String processBbrValidation(BuildingBlockRepository bbr, String cdaDocument, JavaVersionEnum javaVersion)
            throws IOException, InterruptedException {

        File cdaFile = File.createTempFile("cdaBbrToValidate", "xml");
        FileReadWrite.printDoc(cdaDocument, cdaFile.getAbsolutePath());

        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        File validatorSh = new File(
                bbrPathString + GENERATED_VALIDATORS_EXECS_FOLDER + bbr.getId() + "/bin/validator.sh");
        File tmpResFile = File.createTempFile("xml", null);
        String resourcesPath = bbrPathString + GENERATED_VALIDATORS_EXECS_FOLDER + bbr.getId() + "/bin/resources";
        String executedVal = "/bin/sh " + validatorSh.getAbsolutePath() + " -out " + tmpResFile.getAbsolutePath() + " -path "
                + cdaFile.getAbsolutePath() + " -resources " + resourcesPath;
        if(javaVersion.equals(JavaVersionEnum.JAVA_11)){
            String javaPath = getJavaPathFromVersion("11");
            if(javaPath == null){
                LOG.error("Failed to find a JDK 11 installed on the actual machine, validation failed");
                return null;
            }
            executedVal = "env JAVACMD="+javaPath+" "+executedVal;
        }
        LOG.warn("executed validation query >> {}", executedVal);
        final Process processRm = Runtime.getRuntime().exec(executedVal);
        processRm.waitFor();

        System.out.println("Process exitValue: " + processRm.exitValue());
        if(processRm.exitValue() != 0) {
            LOG.error("Process didn't exit correctly, with value: {}", processRm.exitValue());
        }

        System.out.println("-- Process errors");
        LOG.error("-- Process errors");
        BufferedReader error = new BufferedReader(new InputStreamReader(processRm.getErrorStream()));
        String line;
        while ((line = error.readLine()) != null) {
            System.out.println(line);
            LOG.error(line);
        }
        error.close();

        System.out.println("-- Process inputs");
        LOG.warn("-- Process inputs");
        BufferedReader input = new BufferedReader(new InputStreamReader(processRm.getInputStream()));
        while ((line = input.readLine()) != null) {
            System.out.println(line);
            LOG.warn(line);
        }

        input.close();

        System.out.println("-- Process output");

        OutputStream outputStream = processRm.getOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        printStream.println();
        printStream.flush();
        printStream.close();

        processRm.destroy();
        System.out.println("-----");

        return FileReadWrite.readDoc(tmpResFile.getAbsolutePath());
    }

    private static String extractVersion(String version, String decorpath) throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor(new FileInputStream(decorpath));
        List<TemplateDefinition> listRetained = new ArrayList<>();
        for (TemplateDefinition templateDefinition : RulesUtil.getTemplates(dec.getRules())) {
            if (templateDefinition.getVersionLabel() != null && templateDefinition.getVersionLabel().equals(version)) {
                listRetained.add(templateDefinition);
            }
        }
        dec.getRules().getTemplateAssociationOrTemplate().removeAll(RulesUtil.getTemplates(dec.getRules()));
        dec.getRules().getTemplateAssociationOrTemplate().addAll(listRetained);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos);
        return baos.toString();
    }

    public static void printDoc(String doc, String name) throws IOException {
        try (FileWriter fw = new FileWriter(new File(name))) {
            fw.append(doc);
        }
    }

    @Asynchronous
    @Override
    public void processJarValidation(String rootPathString, BuildingBlockRepository bbr)
            throws IOException, InterruptedException {
        File rootPath = new File(rootPathString);
        File execPath = new File(
                rootPath.getAbsoluteFile() + "/generator/hl7templates-packager-app/generateValidator.sh");
        File generatorVersionPath = new File(
                rootPath.getAbsoluteFile() + "/generator/hl7templates-packager-app/version.txt");
        File bbrArchivesPath = new File(rootPath.getAbsoluteFile() + "/generatedValidators/archives/");
        File bbrExecsPath = new File(rootPath.getAbsoluteFile() + GENERATED_VALIDATORS_EXECS_FOLDER);
        File currentArchivePath = new File(
                bbrArchivesPath.getAbsoluteFile() + File.separator + bbr.getId() + File.separator);
        File currentExecPath = new File(bbrExecsPath.getAbsoluteFile() + File.separator + bbr.getId() + File.separator);
        File currentOutputPath = new File(
                rootPath.getAbsoluteFile() + "/generator/hl7templates-packager-app/output-" + bbr.getId() + File.separator);
        File outputLogFile = new File(currentExecPath + "/logs.txt");
        currentArchivePath.mkdirs();
        currentExecPath.mkdirs();
        currentOutputPath.mkdirs();
        String generatorVersion = extractGeneratorVersion(generatorVersionPath);
        File bbrTempFile = File.createTempFile("bbrTemp", ".xml");
        String bbrPath;
        if (bbr.isOnline()) {
            FileUtils.copyURLToFile(new URL(bbr.getUrl()), bbrTempFile);
        } else {
            FileUtils.writeByteArrayToFile(bbrTempFile, bbr.getBbrContent());
        }
        bbrPath = bbrTempFile.getAbsolutePath();

        if (bbr.getVersion() != null && !bbr.getVersion().isEmpty()) {
            try {
                String bbrXmlWithVersion = extractVersion(bbr.getVersion(), bbrPath);
                File bbrVersionFile = File.createTempFile("bbrTemp", ".xml");
                FileUtils.writeStringToFile(bbrVersionFile, bbrXmlWithVersion);
                bbrPath = bbrTempFile.getAbsolutePath();
            } catch (JAXBException e) {
                LOG.error("Problem to parse the BBR", e);
            }
        }

        String request = extractRequest(bbr, execPath, currentArchivePath, currentExecPath, currentOutputPath,
                generatorVersion, bbrPath);

        LOG.warn("begin");
        LOG.warn(String.format("request::>> %s", request));

        String requestPath = bbrArchivesPath.getAbsolutePath() + "/request-compiling-" + bbr.getId() + ".sh";
        FileReadWrite.printDoc(request, requestPath);
        Runtime.getRuntime().exec("chmod 775 " + requestPath);

        String asyncRequest = "sh " + requestPath + " > " + outputLogFile.getAbsolutePath() + " 2>&1 &";
        String asyncRequestPath = bbrArchivesPath.getAbsolutePath() + "/async-request-compiling-" + bbr.getId() + ".sh";
        FileReadWrite.printDoc(asyncRequest, asyncRequestPath);
        Runtime.getRuntime().exec("chmod 775 " + asyncRequestPath);

        final Process processRm = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", asyncRequestPath});
        processRm.waitFor();
        processRm.destroy();
        LOG.warn("process BBR ended (executed on background)");
        EntityManager em = EntityManagerService.provideEntityManager();
        bbr.setBuildingDate(new Date());
        em.merge(bbr);

    }

    private String extractGeneratorVersion(File generatorVersionPath) {
        Properties prop = new Properties();
        try (InputStream input = new FileInputStream(generatorVersionPath)) {
            prop.load(input);
            return prop.getProperty("version");
        } catch (IOException ex) {
            LOG.info("Error to process properties file", ex);
        }
        return null;
    }

    private String extractRequest(BuildingBlockRepository bbr, File execPath, File currentArchivePath,
                                  File currentExecPath, File currentOutputPath, String generatorVersion, String bbrPath) {
        String request = execPath.getAbsolutePath() +
                " --BBR_LINK=\"" + bbrPath + "\""
                + " --OUTPUT_FOLDER=\"output-" + bbr.getId() + "\""
                + " --VALIDATOR_NAME=\"" + bbr.getLabel() + "\""
                + " --IGNORE_TEMPLATEID_REQUIREMENTS=\"" + Boolean.toString(bbr.isIgnoreTemplateidRequirements()) + "\"";
        if (bbr.getVersionLabel() != null && !bbr.getVersionLabel().isEmpty()) {
            request += " --VERSION_LABEL=\"" + bbr.getVersionLabel() + "\"";
        }
        if (bbr.getRootClassName() != null && !bbr.getRootClassName().isEmpty()) {
            request += " --ROOT_CLASS_NAME=\"" + bbr.getRootClassName() + "\"";
        }
        if (bbr.getHL7TEMP_CDACONFFOLDERNAME() != null && !bbr.getHL7TEMP_CDACONFFOLDERNAME().isEmpty()) {
            request += " --HL7TEMP_CDACONFFOLDERNAME=\"" + bbr.getHL7TEMP_CDACONFFOLDERNAME() + "\"";
        }
        request += "\n";
        request += "cd " + currentOutputPath.getAbsolutePath() + "\n";
        request += "mkdir bbr\n";
        request += "cp " + bbrPath + " bbr/bbr.xml\n";
        request += "zipToCopy=`ls *.zip`\n";
        request += "zipBasename=`basename $zipToCopy .zip`\n";
        request += "outputZip=$zipBasename-" + generatorVersion + "-" + bbr.getVersion() + ".zip \n";
        request += "cp $zipToCopy " + currentArchivePath + "/$outputZip\n";
        request += "zip -r " + currentArchivePath.getAbsolutePath() + "/$outputZip documentation \n";
        request += "zip -r " + currentArchivePath.getAbsolutePath() + "/$outputZip bbr \n";
        if (bbr.getXsdZipFile() != null && bbr.getXsdZipFile().getContent() != null) {
            request += "## override default CDA schema ## \n";
            request += "mkdir bin \n";
            request += "cd bin \n";
            request += "mkdir resources \n";
            request += "cd resources \n";
            request += "mkdir xsd \n";
            request += "cd xsd \n";
            request += "unzip -o \"" + bbr.getXsdZipFile().getFilePath() + "\" -d " +
                    currentOutputPath.getAbsolutePath() + "/bin/resources/xsd/ \n";
            request += "cd " + currentOutputPath.getAbsolutePath() + "\n";
            request += "zip -r " + currentArchivePath.getAbsolutePath() + "/$outputZip bin \n";
            request += "################################# \n";
        }
        request += "cp " + currentArchivePath + "/$outputZip " + currentArchivePath + "/latest.zip\n";
        request += "unzip -o " + currentArchivePath.getAbsolutePath() + "/$outputZip -d "
                + currentExecPath.getAbsolutePath() + " \n";
        return request;
    }

    private static String getJavaPathFromVersion(String version){
        Pattern JAVA_VERSION_PATT = Pattern.compile("version \"?"+version+"\\.[0-9]+\\.?[0-9]*\"?");
        try {
            Process process = Runtime.getRuntime().exec("update-alternatives --list java");
            process.waitFor();
            BufferedReader listBuffer = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String javaPath = null;
            while((javaPath = listBuffer.readLine()) != null){
                Process subProcess = Runtime.getRuntime().exec(javaPath+" -version");
                subProcess.waitFor();
                //The output of "java" command is given through STDERR and not STDOUT
                BufferedReader versionBuffer = new BufferedReader(new InputStreamReader(subProcess.getErrorStream()));
                String javaVersion = versionBuffer.readLine();
                if(javaVersion == null){
                    return null;
                }
                Matcher matcher = JAVA_VERSION_PATT.matcher(javaVersion);
                if(matcher.find()){
                    return javaPath;
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


}
