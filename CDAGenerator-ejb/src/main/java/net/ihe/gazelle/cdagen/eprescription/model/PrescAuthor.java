package net.ihe.gazelle.cdagen.eprescription.model;

public class PrescAuthor {
    
    public String aassigningAuthorityName;
    public String adisplayable;
    public String aextension; 
    public String aroot;
    public String afamily;
    public String agiven;
    public PrescAuthor(String aassigningAuthorityName, String adisplayable,
            String aextension, String aroot, String afamily, String agiven) {
        super();
        this.aassigningAuthorityName = aassigningAuthorityName;
        this.adisplayable = adisplayable;
        this.aextension = aextension;
        this.aroot = aroot;
        this.afamily = afamily;
        this.agiven = agiven;
    }
    
    

}
