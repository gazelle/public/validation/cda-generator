package net.ihe.gazelle.cdagen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;

@Entity
@Name("organization")
@Table(name="cdagen_organization", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cdagen_organization_sequence", sequenceName="cdagen_organization_id_seq", allocationSize=1)
public class Organization implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator="cdagen_organization_sequence", strategy=GenerationType.SEQUENCE)
    @NotNull
    @Column(name="id")
    private Integer id;
    
    @Column(name="keyword", unique=true)
    private String keyword;
    
    @Column(name="extension")
    private String extension;
    
    @Column(name="root")
    private String root;
    
    @Column(name="aa_name")
    private String assigningAuthorityName;

    @Override
    public String toString() {
        return "Organization [id=" + id + ", keyword=" + keyword
                + ", extension=" + extension + ", root=" + root
                + ", assigningAuthorityName=" + assigningAuthorityName + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((assigningAuthorityName == null) ? 0
                        : assigningAuthorityName.hashCode());
        result = prime * result
                + ((extension == null) ? 0 : extension.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((root == null) ? 0 : root.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        Organization other = (Organization) obj;
        if (assigningAuthorityName == null) {
            if (other.assigningAuthorityName != null)
                return false;
        } else if (!assigningAuthorityName.equals(other.assigningAuthorityName))
            return false;
        if (extension == null) {
            if (other.extension != null){
                return false;
            }
        } else if (!extension.equals(other.extension))
            return false;
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        if (root == null) {
            if (other.root != null){
                return false;
            }
        } else if (!root.equals(other.root)){
            return false;
        }
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getAssigningAuthorityName() {
        return assigningAuthorityName;
    }

    public void setAssigningAuthorityName(String assigningAuthorityName) {
        this.assigningAuthorityName = assigningAuthorityName;
    }

}
