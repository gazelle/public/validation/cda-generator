package net.ihe.gazelle.cdagen.validator.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.hql.HQLQueryBuilder;

@Entity
@Name("constraintSVS")
@Table(name="constraint_svs", schema="public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "constraint_svs_sequence", sequenceName = "constraint_svs_id_seq", allocationSize=1)
public class ConstraintSVS implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Column(name="id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="constraint_svs_sequence")
	private Integer id;
	
	@Column(name="constraint_id")
	private String constraintId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConstraintId() {
		return constraintId;
	}

	public void setConstraintId(String constraintId) {
		this.constraintId = constraintId;
	}
	
	public static List<ConstraintSVS> getListConstraintSVS(){
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		HQLQueryBuilder<ConstraintSVS> hh = new HQLQueryBuilder<>(em, ConstraintSVS.class);
		return hh.getList();
	}
	
	public static List<String> getListConstraintId(){
		List<ConstraintSVS> ll = getListConstraintSVS();
		List<String> ls = new ArrayList<>();
		for (ConstraintSVS cs : ll) {
			ls.add(cs.constraintId);
		}
		return ls;
	}
	
	public boolean constaintId(String id){
		return (this.constraintId != null) && this.constraintId.equals(id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((constraintId == null) ? 0 : constraintId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConstraintSVS other = (ConstraintSVS) obj;
		if (constraintId == null) {
			if (other.constraintId != null)
				return false;
		} else if (!constraintId.equals(other.constraintId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConstraintSVS [id=" + id + ", constraintId="
				+ constraintId + "]";
	}
	
}
