package net.ihe.gazelle.cdagen.edispensation.action;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import net.ihe.gazelle.cdagen.dao.OrganizationDAO;
import net.ihe.gazelle.cdagen.dao.PatientDAO;
import net.ihe.gazelle.cdagen.edispensation.dao.CDAGenerationDAO;
import net.ihe.gazelle.cdagen.edispensation.model.AffinityDomain;
import net.ihe.gazelle.cdagen.edispensation.model.CDAFile;
import net.ihe.gazelle.cdagen.edispensation.model.CDAGeneration;
import net.ihe.gazelle.cdagen.model.CDADocument;
import net.ihe.gazelle.cdagen.model.CDAType;
import net.ihe.gazelle.cdagen.model.Organization;
import net.ihe.gazelle.cdagen.model.Patient;
import net.ihe.gazelle.cdagen.tools.CDAValidator;
import net.ihe.gazelle.cdagen.tools.XSLTransformer;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.util.File;
import net.ihe.gazelle.patient.PatientGenerator;

@Name("eDispGeneratorManager")
@Scope(ScopeType.PAGE)
public class EDispGeneratorManager extends PatientGenerator implements Serializable {
    
    enum Validator{
        EDISP_FRIENDLY("epSOS - eDispensation - Friendly"), EDISP_PIVOT("epSOS - eDispensation - Pivot");
        
        private String value;
        
        public String getValue(){
            return this.value;
        }
        
        private Validator(String value) {
            this.value = value;
        }
    }
    
    public void initializeListCountry() {
    	if (this.getAvailableCountries() == null || this.getAvailableCountries().size()==0) {
    		this.initializeGenerator();
    	}
    }
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;
    
    private File fichPresc ;
    
    private boolean displayDDSPanel = true;
    
    private boolean displayEditPatientPanel = false;
    
    private Patient selectedPatient;
    
    private String ePrescription;
    
    private String eDispensation;
    
    private String ePViewMode = "xml";
    
    private String eDViewMode = "xml";
    
    private String xmlMetaDataReqForEDisp;
    
    private String xmlMetaDataReqForEPresc;
    
    private String resultValidation;
    
    private CDAGeneration selectedCDAGeneration;
    
    private Organization organization = this.getIHEOrg();
    
    private CDAType cdaType;
    
    private AffinityDomain affinityDomain;
    
    private String ddsDisplayMode = "List Authors";

    public AffinityDomain getAffinityDomain() {
        return affinityDomain;
    }

    public void setAffinityDomain(AffinityDomain affinityDomain) {
        this.affinityDomain = affinityDomain;
    }

    public String getDdsDisplayMode() {
        return ddsDisplayMode;
    }

    public void setDdsDisplayMode(String ddsDisplayMode) {
        this.ddsDisplayMode = ddsDisplayMode;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public CDAType getCdaType() {
        return cdaType;
    }

    public void setCdaType(CDAType cdaType) {
        this.cdaType = cdaType;
    }

    public File getFichPresc() {
        if (fichPresc == null){
            fichPresc = new File();
        }
        return fichPresc;
    }

    public void setFichPresc(File fichPresc) {
        this.fichPresc = fichPresc;
    }

    public CDAGeneration getSelectedCDAGeneration() {
        return selectedCDAGeneration;
    }

    public void setSelectedCDAGeneration(CDAGeneration selectedCDAGeneration) {
        this.selectedCDAGeneration = selectedCDAGeneration;
    }

    public String geteDViewMode() {
        return eDViewMode;
    }

    public void seteDViewMode(String eDViewMode) {
        this.eDViewMode = eDViewMode;
    }

    public String getXmlMetaDataReqForEDisp() {
        return xmlMetaDataReqForEDisp;
    }

    public void setXmlMetaDataReqForEDisp(String xmlMetaDataReqForEDisp) {
        this.xmlMetaDataReqForEDisp = xmlMetaDataReqForEDisp;
    }

    public String getResultValidation() {
        return resultValidation;
    }

    public void setResultValidation(String resultValidation) {
        this.resultValidation = resultValidation;
    }

    public String getePViewMode() {
        return ePViewMode;
    }

    public void setePViewMode(String ePViewMode) {
        this.ePViewMode = ePViewMode;
    }

    public String getePrescription() {
        return ePrescription;
    }

    public void setePrescription(String ePrescription) {
        this.ePrescription = ePrescription;
    }

    public String geteDispensation() {
        return eDispensation;
    }

    public void seteDispensation(String eDispensation) {
        this.eDispensation = eDispensation;
    }

    public boolean isDisplayEditPatientPanel() {
        return displayEditPatientPanel;
    }

    public void setDisplayEditPatientPanel(boolean displayEditPatientPanel) {
        this.displayEditPatientPanel = displayEditPatientPanel;
    }

    public void setDisplayDDSPanel(boolean displayDDSPanel) {
        this.displayDDSPanel = displayDDSPanel;
    }

    public boolean isDisplayDDSPanel() {
        return displayDDSPanel;
    }

    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    public String getXmlMetaDataReqForEPresc() {
        return xmlMetaDataReqForEPresc;
    }

    public void setXmlMetaDataReqForEPresc(String xmlMetaDataReqForEPresc) {
        this.xmlMetaDataReqForEPresc = xmlMetaDataReqForEPresc;
    }

    public void generateDispensation(){
        if (this.ePrescription != null){
            String message_uuid = UUID.randomUUID().toString();
            Date currentDate = new Date();
            String effectiveTime = new SimpleDateFormat("yyyyMMdd").format(currentDate);
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            String authorLN = "";
            String authorFN = "";
            if (this.selectedPatient != null){
                authorLN = this.selectedPatient.getLastName();
                authorFN = this.selectedPatient.getFirstName();
            }
            
            this.eDispensation = DispGenerator.generateDispensation(this.ePrescription, message_uuid, effectiveTime, "CDAGEN", "IHE", "fr-FR", 
                    this.organization.getRoot(), this.organization.getExtension(), this.organization.getAssigningAuthorityName(), 
                    authorLN, authorFN, entityManager);
            
            CDAFile cdapresc = new CDAFile(ePrescription, CDADocument.PRESCRIPTION, this.cdaType, currentDate);
            CDAFile cdadisp = new CDAFile(eDispensation,CDADocument.DISPENSATION, this.cdaType, currentDate);
            CDAGeneration cdagen = new CDAGeneration(cdapresc, cdadisp, currentDate, this.selectedPatient, null, this.affinityDomain);
            CDAGenerationDAO.mergeCDAGeneration(cdagen, entityManager);
        }
    }
    
    public void generateAndSavePatient()
    {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedPatient = new Patient(generatePatient());
        this.selectedPatient = PatientDAO.mergePatient(selectedPatient, entityManager);
        if (selectedPatient != null){
            displayDDSPanel = false;
        }
    }
    
    public void displayDDSPanel()
    {
        displayDDSPanel = true;
        selectedPatient = null;
    }
    
    public List<String> getSelectionModes(){
        List<String> ls = new ArrayList<String>();
        ls.add("xml");
        ls.add("html");
        return ls;
    }
    
    public String getEPrescriptionModified(){
        if (this.ePrescription != null){
            return this.getCDAModified(this.ePrescription);
        }
        return null;
    }
    
    public String getCDAModified(String cda){
        if (cda != null){
            String res = "";
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            res = XSLTransformer.resultTransformation(cda, "cda.xsl", null, entityManager);
            return res;
        }
        return null;
    }
    
    public String getEPrescriptionModified2(){
        return CDAGenUtil.getCDAModified2(this.ePrescription);
    }
    
    public String getEDispensationModified(){
        return this.getCDAModified(this.eDispensation);
    }
    
    public void validateEDispensation(){
        if (this.xmlMetaDataReqForEDisp != null){
            EntityManager em = (EntityManager)Component.getInstance("entityManager");
            this.resultValidation = CDAValidator.getValidationResultV3(this.xmlMetaDataReqForEDisp, this.eDispensation, em);
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            vrl.setHtmlResultValidation(this.geteResultValidationAndFormatted());
        }
        else{
            FacesMessages.instance().add("Chose a type of validation.");
        }
    }
    
    public String geteResultValidationAndFormatted(){
        if (this.resultValidation != null){
            EntityManager em = (EntityManager)Component.getInstance("entityManager");
            return CDAValidator.transformXMLToHTML(this.resultValidation, em);
        }
        return null;
    }
    
    public void updateEPView(){
        if (this.ePrescription != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            if ((this.ePViewMode != null) && (ePViewMode.equals("html1"))){
                vrl.setePrescriptionHtml1(this.getEPrescriptionModified());
            }
            if ((this.ePViewMode != null) && (ePViewMode.equals("html2"))){
                vrl.setePrescriptionHtml2(this.getEPrescriptionModified2());
            }
        }
    }
    
    public void updateEDView(){
        if (this.eDispensation != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            if ((this.eDViewMode != null) && (eDViewMode.equals("html"))){
                vrl.seteDispensationHtml1(this.getEDispensationModified());
            }
        }
    }
    
    public void updateEPViewForMessages(){
        if (this.selectedCDAGeneration != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            if ((this.ePViewMode != null) && (ePViewMode.equals("html1"))){
                vrl.setePrescriptionHtml1(this.getCDAModified(this.selectedCDAGeneration.getPrescription().getCdaFileContent()));
            }
            if ((this.ePViewMode != null) && (ePViewMode.equals("html2"))){
                vrl.setePrescriptionHtml2(CDAGenUtil.getCDAModified2(this.selectedCDAGeneration.getPrescription().getCdaFileContent()));
            }
        }
    }
    
    public void updateEDViewForMessages(){
        if (this.selectedCDAGeneration != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            if ((this.eDViewMode != null) && (eDViewMode.equals("html"))){
                vrl.seteDispensationHtml1(this.getCDAModified(this.selectedCDAGeneration.getDispensation().getCdaFileContent()));
            }
        }
    }
    
    public void validateEPrescription(){
        if (this.xmlMetaDataReqForEPresc != null){
            EntityManager em = (EntityManager)Component.getInstance("entityManager");
            this.resultValidation = CDAValidator.getValidationResultV3(this.xmlMetaDataReqForEPresc, this.ePrescription, em);
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            vrl.setHtmlResultValidation(this.geteResultValidationAndFormatted());
        }
        else{
            FacesMessages.instance().add("Chose a type of validation.");
        }
    }
    
    public List<CDAGeneration> getListCDAGeneration(){
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        return CDAGenerationDAO.getListCDAGeneration(entityManager);
    }
    
    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        this.getFichPresc().setData(item.getData());
        this.ePrescription = new String(this.getFichPresc().getData());
        boolean validpresc = this.assertStringIsCDAEPrescription(this.ePrescription);
        if (!validpresc){
            this.ePrescription = null;
            this.setFichPresc(new File());
            FacesMessages.instance().add("The document uploaded is not a Prescription !");
        }
    }
    
    private boolean assertStringIsCDAEPrescription(String pres){
        EntityManager em = (EntityManager)Component.getInstance("entityManager");
        return CDAValidator.assertStringIsCDAEPrescription(pres, em);
    }
    
    public List<CDAType> listCDATypes(){
        return Arrays.asList(CDAType.values());
    }
    
    public void init(){
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        this.organization = OrganizationDAO.getOrganizationByKeyword("IHE", em);
    }
    
    private Organization getIHEOrg(){
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        Organization org = OrganizationDAO.getOrganizationByKeyword("IHE", em);
        if (org == null) {
        	org = new Organization();
        }
        return org;
    }
    
    public void setSelectedPatientAndUpdateGUI(Patient pp){
        this.selectedPatient = pp;
        this.displayDDSPanel = false;
        this.displayEditPatientPanel = false;
    }
    
    public void updateAuthor(){
        this.setDisplayEditPatientPanel(false);
        if (this.selectedPatient.getId() != null){
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            List<CDAGeneration> lp = CDAGenerationDAO.getListCDAGenerationFiltered(em, selectedPatient);
            if ((lp != null)&& (lp.size()>0)){
                this.selectedPatient.setId(null);
                this.selectedPatient.setCreationDate(new Date());
                this.selectedPatient = PatientDAO.mergePatient(selectedPatient, em);
            }
            else{
                this.selectedPatient = PatientDAO.mergePatient(selectedPatient, em);
            }
        }
    }
    
    public String generateLinkForCDAGeneration(CDAGeneration cdagen){
        String res = "";
        res = res + ApplicationPreferenceManager.getStringValue("application_url") + "/cdagen.seam?id=" + cdagen.getId().intValue();
        return res;
    }
    
    public List<AffinityDomain> listAffinityDomains(){
        return Arrays.asList(AffinityDomain.values());
    }

}
