package net.ihe.gazelle.cdagen.bbr.action;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.scoring.model.templates.TemplatesContainment;
import net.ihe.gazelle.scoring.richness.util.TemplatesContainmentMarshaller;

/**
 * Created by xfs on 25/11/16.
 */

@Name("tcDescriberManager")
@Scope(ScopeType.PAGE)
public class TCDescriberManager {

	private static final String ATTACHMENT_FILENAME = "attachment;filename=\"";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	private TCDescriber selectedTCDescriber;

	private String selectedTemplateId;

	private FilterDataModel<TCDescriber> tcDescribers;

	private TCDescriberFilter filter;

	public String getSelectedTemplateId() {
		return selectedTemplateId;
	}

	public void setSelectedTemplateId(String selectedTemplateId) {
		this.selectedTemplateId = selectedTemplateId;
	}

	public TCDescriberFilter getFilter() {
		if (filter == null) {
			final FacesContext fc = FacesContext.getCurrentInstance();
			final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
			filter = new TCDescriberFilter(requestParameterMap);
		}
		return filter;
	}

	public FilterDataModel<TCDescriber> getTcDescribers() {
		if (tcDescribers == null) {
			tcDescribers = new FilterDataModel<TCDescriber>(getFilter()) {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected Object getId(TCDescriber t) {
					return t.getId();
				}
			};
		}
		return tcDescribers;
	}

	public void setTcDescribers(FilterDataModel<TCDescriber> tcDescribers) {
		this.tcDescribers = tcDescribers;
	}

	public TCDescriber getSelectedTCDescriber() {
		return selectedTCDescriber;
	}

	public void setSelectedTCDescriber(TCDescriber selectedTCDescriber) {
		this.selectedTCDescriber = selectedTCDescriber;
	}

	public void deleteSelectedTCDescriber() {
		EntityManager em = EntityManagerService.provideEntityManager();
		TCDescriber tcdToDelete = em.find(TCDescriber.class, selectedTCDescriber.getId());
		em.remove(tcdToDelete);
	}

	public String save() {
		EntityManager em = EntityManagerService.provideEntityManager();
		em.merge(this.selectedTCDescriber);
		em.flush();
		return "/admin/templatesContainment/list.seam";
	}

	public void uploadTCDListener(FileUploadEvent event) {
		UploadedFile item = event.getUploadedFile();
		this.selectedTCDescriber.setTcContent(item.getData());
	}

	public void initSelectedTCDescriber() {
		EntityManager em = EntityManagerService.provideEntityManager();
		final FacesContext fc = FacesContext.getCurrentInstance();
		final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
		if (requestParameterMap.get("tcdId") != null) {
			Integer id = Integer.parseInt(requestParameterMap.get("tcdId"));
			this.selectedTCDescriber = em.find(TCDescriber.class, id);
		} else {
			this.selectedTCDescriber = new TCDescriber();
		}
	}

	public void deleteSelectedBuildingBlockRepository() {
		EntityManager em = EntityManagerService.provideEntityManager();
		TCDescriber tcdToDelete = em.find(TCDescriber.class, this.selectedTCDescriber.getId());
		em.remove(tcdToDelete);
		em.flush();
	}

	public void downloadTC(TCDescriber tcd) throws IOException {
		if (tcd != null && tcd.getTcContent() != null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/xml");
			response.setContentLength(tcd.getTcContent().length);
			response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + tcd.getTcName() + ".xml" + "\"");
			servletOutputStream.write(tcd.getTcContent());
			servletOutputStream.flush();
			servletOutputStream.close();
			FacesContext.getCurrentInstance().responseComplete();
		}
	}

	public void downloadTCF(TCDescriber tcd) throws IOException {
		if (tcd != null && tcd.getTcFlattened() != null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType("application/xml");
			response.setContentLength(tcd.getTcFlattened().length);
			response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + tcd.getTcName() + "_flattened.xml" + "\"");
			servletOutputStream.write(tcd.getTcFlattened());
			servletOutputStream.flush();
			servletOutputStream.close();
			FacesContext.getCurrentInstance().responseComplete();
		}
	}

	public void downloadTCFMM() throws IOException, JAXBException {
		if (this.selectedTCDescriber != null && this.selectedTCDescriber.getTcFlattened() != null) {
			TemplatesContainment tcf = TemplatesContainmentMarshaller
					.loadTemplatesContainment(new ByteArrayInputStream(this.selectedTCDescriber.getTcFlattened()));
			TCDescriberUtil.downloadTemplatesContainmentAsMM(tcf, this.selectedTemplateId,
					this.selectedTCDescriber.getTcName());
		}
	}

	public void downloadTCMM() throws IOException, JAXBException {
		if (this.selectedTCDescriber != null && this.selectedTCDescriber.getTcContent() != null) {
			TemplatesContainment tcf = TemplatesContainmentMarshaller
					.loadTemplatesContainment(new ByteArrayInputStream(this.selectedTCDescriber.getTcContent()));
			TCDescriberUtil.downloadTemplatesContainmentAsMM(tcf, this.selectedTemplateId,
					this.selectedTCDescriber.getTcName());
		}
	}

}
