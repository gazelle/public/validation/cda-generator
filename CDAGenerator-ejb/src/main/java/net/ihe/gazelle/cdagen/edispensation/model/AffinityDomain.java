/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.cdagen.edispensation.model;

import javax.persistence.Column;

/**
 * 
 * @author Abderrazek Boufahja
 * @version 7 oct. 2011
 *
 */
public enum AffinityDomain {

	EPSOS("epsos", "epsos");

	@Column(name = "label_to_display")
	private String labelToDisplay;

	@Column(name = "keyword", unique = true)
	private String keyword;

	AffinityDomain(String keyword, String label) {
		this.keyword = keyword;
		this.labelToDisplay = label;
	}

	public String getLabelToDisplay() {
		return labelToDisplay;
	}

	public String getKeyword() {
		return keyword;
	}

}
