package net.ihe.gazelle.cdagen.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

    // menu elements

    HOME("/home.xhtml", null, "Defined at runtime", Authorizations.ALL),
    ERROR("/error.xhtml", null, "Error page", Authorizations.ALL),
    VERSION("/version.xhtml", null, "Gazelle version", Authorizations.ALL),


    DISPENSATION("/dispgenerator/dispensation.xhtml", null, "dispensation", Authorizations.ALL),
    CDAGENERATED("/dispgenerator/cdaGenerated.xhtml", null, "cdaGenerated", Authorizations.ALL),


    LIST_CDA_OBJECT_TYPE("/cdaObjectType/listCDAObjectType.xhtml", null, "list CDA Object Type", Authorizations.ALL),
    LIST_TEMPLATES("/docum/templates/listTemplates.xhtml", null, "list Templates", Authorizations.ALL),
    TEMPLATE_DESCRIPTOR("/docum/templates/templateDescriptor.xhtml", null, "template Descriptor", Authorizations.ALL),
    LIST_CLASSES("/docum/classes/listClasses.xhtml", null, "listClasses", Authorizations.ALL),
    CLASS_DESCRIPTOR("/docum/classes/classDescriptor.xhtml", null, "class Descriptor", Authorizations.ALL),
    CONSTRAINTS("/docum/constraints/constraints.xhtml", null, "constraints", Authorizations.ALL),
    CONSTRAINT_DESCRIPTOR("/docum/constraints/constraintDescriptor.xhtml", null, "constraintDescriptor", Authorizations.ALL),


    CDA_EDITOR("/editor/editor.xhtml", null, "CDA editor", Authorizations.ALL),
    DOCUMENT_ROOT_EDITOR("/editor/cda/DocumentRoot.xhtml", null, "DocumentRoot editor", Authorizations.ALL),

    POCDMT000040INTENDEDRECIPIENT("/editor/cda/POCDMT000040IntendedRecipient.xhtml", null, "Intended Recipient", Authorizations.ALL),
    POCDMT000040DOCUMENTATIONOF("/editor/cda/POCDMT000040DocumentationOf.xhtml", null, "Documentation Of", Authorizations.ALL),
    POCDMT000040CUSTODIAN("/editor/cda/POCDMT000040Custodian.xhtml", null, "Custodian", Authorizations.ALL),
    POCDMT000040COMPONENT2("/editor/cda/POCDMT000040Component2.xhtml", null, "Component2", Authorizations.ALL),
    POCDMT000040AUTHOR("/editor/cda/POCDMT000040Author.xhtml", null, "Author", Authorizations.ALL),
    POCDMT000040SUPPLY("/editor/cda/POCDMT000040Supply.xhtml", null, "Supply", Authorizations.ALL),
    POCDMT000040OBSERVATION("/editor/cda/POCDMT000040Observation.xhtml", null, "Observation", Authorizations.ALL),
    POCDMT000040CRITERION("/editor/cda/POCDMT000040Criterion.xhtml", null, "Criterion", Authorizations.ALL),
    POCDMT000040OBSERVATIONMEDIA("/editor/cda/POCDMT000040ObservationMedia.xhtml", null, "Observation Media", Authorizations.ALL),
    POCDMT000040EXTERNALOBSERVATION("/editor/cda/POCDMT000040ExternalObservation.xhtml", null, "External Observation", Authorizations.ALL),
    POCDMT000040STRUCTUREDBODY("/editor/cda/POCDMT000040StructuredBody.xhtml", null, "Structured Body", Authorizations.ALL),
    POCDMT000040PROCEDURE("/editor/cda/POCDMT000040Procedure.xhtml", null, "Procedure", Authorizations.ALL),
    POCDMT000040SUBSTANCEADMINISTRATION("/editor/cda/POCDMT000040SubstanceAdministration.xhtml", null, "Substance Administration", Authorizations
            .ALL),
    POCDMT000040SPECIMENROLE("/editor/cda/POCDMT000040SpecimenRole.xhtml", null, "Specimen Role", Authorizations.ALL),
    POCDMT000040DEVICE("/editor/cda/POCDMT000040Device.xhtml", null, "Device", Authorizations.ALL),
    POCDMT000040SERVICEEVENT("/editor/cda/POCDMT000040ServiceEvent.xhtml", null, "Service Event", Authorizations.ALL),
    POCDMT000040PRECONDITION("/editor/cda/POCDMT000040Precondition.xhtml", null, "Precondition", Authorizations.ALL),
    POCDMT000040CUSTODIANORGANIZATION("/editor/cda/POCDMT000040CustodianOrganization.xhtml", null, "Custodian Organization", Authorizations.ALL),
    POCDMT000040ORGANIZATION("/editor/cda/POCDMT000040Organization.xhtml", null, "Organization", Authorizations.ALL),
    POCDMT000040ENCOUNTER("/editor/cda/POCDMT000040Encounter.xhtml", null, "Encounter", Authorizations.ALL),
    POCDMT000040PARTICIPANTROLE("/editor/cda/POCDMT000040ParticipantRole.xhtml", null, "Participant Role", Authorizations.ALL),
    POCDMT000040LOCATION("/editor/cda/POCDMT000040Location.xhtml", null, "Location", Authorizations.ALL),
    POCDMT000040CLINICALDOCUMENT("/editor/cda/POCDMT000040ClinicalDocument.xhtml", null, "Clinical Document", Authorizations.ALL),
    POCDMT000040BIRTHPLACE("/editor/cda/POCDMT000040Birthplace.xhtml", null, "Birthplace", Authorizations.ALL),
    POCDMT000040AUTHORINGDEVICE("/editor/cda/POCDMT000040AuthoringDevice.xhtml", null, "Authoring Device", Authorizations.ALL),
    POCDMT000040PARTICIPANT2("/editor/cda/POCDMT000040Participant2.xhtml", null, "Participant2", Authorizations.ALL),
    POCDMT000040CONSENT("/editor/cda/POCDMT000040Consent.xhtml", null, "Consent", Authorizations.ALL),
    POCDMT000040ASSIGNEDAUTHOR("/editor/cda/POCDMT000040AssignedAuthor.xhtml", null, "Assigned Author", Authorizations.ALL),
    POCDMT000040RELATEDENTITY("/editor/cda/POCDMT000040RelatedEntity.xhtml", null, "Related Entity", Authorizations.ALL),
    POCDMT000040ENCOMPASSINGENCOUNTER("/editor/cda/POCDMT000040EncompassingEncounter.xhtml", null, "Encompassing Encounter", Authorizations.ALL),
    POCDMT000040PERFORMER1("/editor/cda/POCDMT000040Performer1.xhtml", null, "Performer1", Authorizations.ALL),
    POCDMT000040RELATEDSUBJECT("/editor/cda/POCDMT000040RelatedSubject.xhtml", null, "Related Subject", Authorizations.ALL),
    POCDMT000040SECTION("/editor/cda/POCDMT000040Section.xhtml", null, "Section", Authorizations.ALL),
    POCDMT000040REFERENCERANGE("/editor/cda/POCDMT000040ReferenceRange.xhtml", null, "Reference Range", Authorizations.ALL),
    POCDMT000040PLAYINGENTITY("/editor/cda/POCDMT000040PlayingEntity.xhtml", null, "Playing Entity", Authorizations.ALL),
    POCDMT000040GUARDIAN("/editor/cda/POCDMT000040Guardian.xhtml", null, "Guardian", Authorizations.ALL),
    POCDMT000040ORGANIZATIONPARTOF("/editor/cda/POCDMT000040OrganizationPartOf.xhtml", null, "Organization Part Of", Authorizations.ALL),
    POCDMT000040COMPONENT1("/editor/cda/POCDMT000040Component1.xhtml", null, "Component1", Authorizations.ALL),
    POCDMT000040HEALTHCAREFACILITY("/editor/cda/POCDMT000040HealthCareFacility.xhtml", null, "Health Care Facility", Authorizations.ALL),
    POCDMT000040REGIONOFINTEREST("/editor/cda/POCDMT000040RegionOfInterest.xhtml", null, "Region Of Interest", Authorizations.ALL),
    POCDMT000040INFRASTRUCTUREROOTTYPEID("/editor/cda/POCDMT000040InfrastructureRootTypeId.xhtml", null, "Infrastructure Root Type Id",
            Authorizations.ALL),
    POCDMT000040PARTICIPANT1("/editor/cda/POCDMT000040Participant1.xhtml", null, "Participant1", Authorizations.ALL),
    POCDMT000040PERSON("/editor/cda/POCDMT000040Person.xhtml", null, "Person", Authorizations.ALL),
    POCDMT000040LABELEDDRUG("/editor/cda/POCDMT000040LabeledDrug.xhtml", null, "Labeled Drug", Authorizations.ALL),
    POCDMT000040INFORMANT12("/editor/cda/POCDMT000040Informant12.xhtml", null, "Informant12", Authorizations.ALL),
    POCDMT000040SUBJECTPERSON("/editor/cda/POCDMT000040SubjectPerson.xhtml", null, "Subject Person", Authorizations.ALL),
    POCDMT000040LEGALAUTHENTICATOR("/editor/cda/POCDMT000040LegalAuthenticator.xhtml", null, "Legal Authenticator", Authorizations.ALL),
    POCDMT000040ENCOUNTERPARTICIPANT("/editor/cda/POCDMT000040EncounterParticipant.xhtml", null, "Encounter Participant", Authorizations.ALL),
    POCDMT000040REGIONOFINTERESTVALUE("/editor/cda/POCDMT000040RegionOfInterestValue.xhtml", null, "Region Of Interest Value", Authorizations.ALL),
    POCDMT000040ACT("/editor/cda/POCDMT000040Act.xhtml", null, "Act", Authorizations.ALL),
    POCDMT000040MAINTAINEDENTITY("/editor/cda/POCDMT000040MaintainedEntity.xhtml", null, "Maintained Entity", Authorizations.ALL),
    POCDMT000040ASSIGNEDCUSTODIAN("/editor/cda/POCDMT000040AssignedCustodian.xhtml", null, "Assigned Custodian", Authorizations.ALL),
    POCDMT000040EXTERNALPROCEDURE("/editor/cda/POCDMT000040ExternalProcedure.xhtml", null, "External Procedure", Authorizations.ALL),
    POCDMT000040PATIENT("/editor/cda/POCDMT000040Patient.xhtml", null, "Patient", Authorizations.ALL),
    POCDMT000040INFULFILLMENTOF("/editor/cda/POCDMT000040InFulfillmentOf.xhtml", null, "In Fulfillment Of", Authorizations.ALL),
    POCDMT000040ASSIGNEDENTITY("/editor/cda/POCDMT000040AssignedEntity.xhtml", null, "Assigned Entity", Authorizations.ALL),
    POCDMT000040CONSUMABLE("/editor/cda/POCDMT000040Consumable.xhtml", null, "Consumable", Authorizations.ALL),
    POCDMT000040COMPONENT3("/editor/cda/POCDMT000040Component3.xhtml", null, "Component3", Authorizations.ALL),
    POCDMT000040ORDER("/editor/cda/POCDMT000040Order.xhtml", null, "Order", Authorizations.ALL),
    POCDMT000040ASSOCIATEDENTITY("/editor/cda/POCDMT000040AssociatedEntity.xhtml", null, "Associated Entity", Authorizations.ALL),
    POCDMT000040PLACE("/editor/cda/POCDMT000040Place.xhtml", null, "Place", Authorizations.ALL),
    POCDMT000040DATAENTERER("/editor/cda/POCDMT000040DataEnterer.xhtml", null, "Data Enterer", Authorizations.ALL),
    POCDMT000040LANGUAGECOMMUNICATION("/editor/cda/POCDMT000040LanguageCommunication.xhtml", null, "Language Communication", Authorizations.ALL),
    POCDMT000040INFORMATIONRECIPIENT("/editor/cda/POCDMT000040InformationRecipient.xhtml", null, "Information Recipient", Authorizations.ALL),
    POCDMT000040MATERIAL("/editor/cda/POCDMT000040Material.xhtml", null, "Material", Authorizations.ALL),
    POCDMT000040PATIENTROLE("/editor/cda/POCDMT000040PatientRole.xhtml", null, "Patient Role", Authorizations.ALL),
    POCDMT000040ORGANIZER("/editor/cda/POCDMT000040Organizer.xhtml", null, "Organizer", Authorizations.ALL),
    POCDMT000040RESPONSIBLEPARTY("/editor/cda/POCDMT000040ResponsibleParty.xhtml", null, "Responsible Party", Authorizations.ALL),
    POCDMT000040AUTHENTICATOR("/editor/cda/POCDMT000040Authenticator.xhtml", null, "Authenticator", Authorizations.ALL),
    POCDMT000040SUBJECT("/editor/cda/POCDMT000040Subject.xhtml", null, "Subject", Authorizations.ALL),
    POCDMT000040ENTRY("/editor/cda/POCDMT000040Entry.xhtml", null, "Entry", Authorizations.ALL),
    POCDMT000040ENTRYRELATIONSHIP("/editor/cda/POCDMT000040EntryRelationship.xhtml", null, "Entry Relationship", Authorizations.ALL),
    POCDMT000040RELATEDDOCUMENT("/editor/cda/POCDMT000040RelatedDocument.xhtml", null, "Related Document", Authorizations.ALL),
    POCDMT000040AUTHORIZATION("/editor/cda/POCDMT000040Authorization.xhtml", null, "Authorization", Authorizations.ALL),
    POCDMT000040PARENTDOCUMENT("/editor/cda/POCDMT000040ParentDocument.xhtml", null, "Parent Document", Authorizations.ALL),
    POCDMT000040REFERENCE("/editor/cda/POCDMT000040Reference.xhtml", null, "Reference", Authorizations.ALL),
    POCDMT000040COMPONENT4("/editor/cda/POCDMT000040Component4.xhtml", null, "Component4", Authorizations.ALL),
    POCDMT000040EXTERNALDOCUMENT("/editor/cda/POCDMT000040ExternalDocument.xhtml", null, "External Document", Authorizations.ALL),
    POCDMT000040MANUFACTUREDPRODUCT("/editor/cda/POCDMT000040ManufacturedProduct.xhtml", null, "Manufactured Product", Authorizations.ALL),
    POCDMT000040RECORDTARGET("/editor/cda/POCDMT000040RecordTarget.xhtml", null, "Record Target", Authorizations.ALL),
    POCDMT000040OBSERVATIONRANGE("/editor/cda/POCDMT000040ObservationRange.xhtml", null, "Observation Range", Authorizations.ALL),
    POCDMT000040ENTITY("/editor/cda/POCDMT000040Entity.xhtml", null, "Entity", Authorizations.ALL),
    POCDMT000040PERFORMER2("/editor/cda/POCDMT000040Performer2.xhtml", null, "Performer2", Authorizations.ALL),
    POCDMT000040NONXMLBODY("/editor/cda/POCDMT000040NonXMLBody.xhtml", null, "Non XMLBody", Authorizations.ALL),
    POCDMT000040EXTERNALACT("/editor/cda/POCDMT000040ExternalAct.xhtml", null, "External Act", Authorizations.ALL),
    POCDMT000040COMPONENT5("/editor/cda/POCDMT000040Component5.xhtml", null, "Component5", Authorizations.ALL),
    POCDMT000040SPECIMEN("/editor/cda/POCDMT000040Specimen.xhtml", null, "Specimen", Authorizations.ALL),
    POCDMT000040PRODUCT("/editor/cda/POCDMT000040Product.xhtml", null, "Product", Authorizations.ALL),

    DATATYPES_ADXPSTREETADDRESSLINE("/editor/cda/AdxpStreetAddressLine.xhtml", null, "Adxp Street Address Line", Authorizations.ALL),
    DATATYPES_ADXPDIRECTION("/editor/cda/AdxpDirection.xhtml", null, "Adxp Direction", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYINSTALLATIONQUALIFIER("/editor/cda/AdxpDeliveryInstallationQualifier.xhtml", null, "Adxp Delivery Installation " +
            "Qualifier", Authorizations.ALL),
    DATATYPES_IVXBTS("/editor/cda/IVXBTS.xhtml", null, "IVXBTS", Authorizations.ALL),
    DATATYPES_IVXBPQ("/editor/cda/IVXBPQ.xhtml", null, "IVXBPQ", Authorizations.ALL),
    DATATYPES_CE("/editor/cda/CE.xhtml", null, "CE", Authorizations.ALL),
    DATATYPES_EIVLTS("/editor/cda/EIVLTS.xhtml", null, "EIVLTS", Authorizations.ALL),
    DATATYPES_RTOMOPQ("/editor/cda/RTOMOPQ.xhtml", null, "RTOMOPQ", Authorizations.ALL),
    DATATYPES_SLISTTS("/editor/cda/SLISTTS.xhtml", null, "SLISTTS", Authorizations.ALL),
    DATATYPES_ADXP("/editor/cda/ADXP.xhtml", null, "ADXP", Authorizations.ALL),
    DATATYPES_MO("/editor/cda/MO.xhtml", null, "MO", Authorizations.ALL),
    DATATYPES_IVLPQ("/editor/cda/IVLPQ.xhtml", null, "IVLPQ", Authorizations.ALL),
    DATATYPES_RTO("/editor/cda/RTO.xhtml", null, "RTO", Authorizations.ALL),
    DATATYPES_SXCMMO("/editor/cda/SXCMMO.xhtml", null, "SXCMMO", Authorizations.ALL),
    DATATYPES_EIVLEVENT("/editor/cda/EIVLEvent.xhtml", null, "EIVLEvent", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYINSTALLATIONAREA("/editor/cda/AdxpDeliveryInstallationArea.xhtml", null, "Adxp Delivery Installation Area",
            Authorizations.ALL),
    DATATYPES_SXCMCD("/editor/cda/SXCMCD.xhtml", null, "SXCMCD", Authorizations.ALL),
    DATATYPES_SXCMINT("/editor/cda/SXCMINT.xhtml", null, "SXCMINT", Authorizations.ALL),
    DATATYPES_GLISTTS("/editor/cda/GLISTTS.xhtml", null, "GLISTTS", Authorizations.ALL),
    DATATYPES_ENGIVEN("/editor/cda/EnGiven.xhtml", null, "En Given", Authorizations.ALL),
    DATATYPES_PIVLTS("/editor/cda/PIVLTS.xhtml", null, "PIVLTS", Authorizations.ALL),
    DATATYPES_URL("/editor/cda/URL.xhtml", null, "URL", Authorizations.ALL),
    DATATYPES_CV("/editor/cda/CV.xhtml", null, "CV", Authorizations.ALL),
    DATATYPES_ADXPPOSTALCODE("/editor/cda/AdxpPostalCode.xhtml", null, "Adxp Postal Code", Authorizations.ALL),
    DATATYPES_ADXPSTREETNAMETYPE("/editor/cda/AdxpStreetNameType.xhtml", null, "Adxp Street Name Type", Authorizations.ALL),
    DATATYPES_IVLINT("/editor/cda/IVLINT.xhtml", null, "IVLINT", Authorizations.ALL),
    DATATYPES_ADXPSTREETNAMEBASE("/editor/cda/AdxpStreetNameBase.xhtml", null, "Adxp Street Name Base", Authorizations.ALL),
    DATATYPES_ADXPPRECINCT("/editor/cda/AdxpPrecinct.xhtml", null, "Adxp Precinct", Authorizations.ALL),
    DATATYPES_SXCMPPDPQ("/editor/cda/SXCMPPDPQ.xhtml", null, "SXCMPPDPQ", Authorizations.ALL),
    DATATYPES_CR("/editor/cda/CR.xhtml", null, "CR", Authorizations.ALL),
    DATATYPES_ADXPSTREETNAME("/editor/cda/AdxpStreetName.xhtml", null, "Adxp Street Name", Authorizations.ALL),
    DATATYPES_CO("/editor/cda/CO.xhtml", null, "CO", Authorizations.ALL),
    DATATYPES_PN("/editor/cda/PN.xhtml", null, "PN", Authorizations.ALL),
    DATATYPES_ADXPPOSTBOX("/editor/cda/AdxpPostBox.xhtml", null, "Adxp Post Box", Authorizations.ALL),
    DATATYPES_BN("/editor/cda/BN.xhtml", null, "BN", Authorizations.ALL),
    DATATYPES_CS("/editor/cda/CS.xhtml", null, "CS", Authorizations.ALL),
    DATATYPES_IVLREAL("/editor/cda/IVLREAL.xhtml", null, "IVLREAL", Authorizations.ALL),
    DATATYPES_EN("/editor/cda/EN.xhtml", null, "EN", Authorizations.ALL),
    DATATYPES_IVXBREAL("/editor/cda/IVXBREAL.xhtml", null, "IVXBREAL", Authorizations.ALL),
    DATATYPES_INT("/editor/cda/INT.xhtml", null, "INT", Authorizations.ALL),
    DATATYPES_IVLPPDPQ("/editor/cda/IVLPPDPQ.xhtml", null, "IVLPPDPQ", Authorizations.ALL),
    DATATYPES_EIVLPPDTS("/editor/cda/EIVLPPDTS.xhtml", null, "EIVLPPDTS", Authorizations.ALL),
    DATATYPES_ENPREFIX("/editor/cda/EnPrefix.xhtml", null, "En Prefix", Authorizations.ALL),
    DATATYPES_IVXBMO("/editor/cda/IVXBMO.xhtml", null, "IVXBMO", Authorizations.ALL),
    DATATYPES_PIVLPPDTS("/editor/cda/PIVLPPDTS.xhtml", null, "PIVLPPDTS", Authorizations.ALL),
    DATATYPES_PPDTS("/editor/cda/PPDTS.xhtml", null, "PPDTS", Authorizations.ALL),
    DATATYPES_ADXPUNITTYPE("/editor/cda/AdxpUnitType.xhtml", null, "Adxp Unit Type", Authorizations.ALL),
    DATATYPES_QTY("/editor/cda/QTY.xhtml", null, "QTY", Authorizations.ALL),
    DATATYPES_SXPRTS("/editor/cda/SXPRTS.xhtml", null, "SXPRTS", Authorizations.ALL),
    DATATYPES_TEL("/editor/cda/TEL.xhtml", null, "TEL", Authorizations.ALL),
    DATATYPES_PPDPQ("/editor/cda/PPDPQ.xhtml", null, "PPDPQ", Authorizations.ALL),
    DATATYPES_HXITCE("/editor/cda/HXITCE.xhtml", null, "HXITCE", Authorizations.ALL),
    DATATYPES_SXCMTS("/editor/cda/SXCMTS.xhtml", null, "SXCMTS", Authorizations.ALL),
    DATATYPES_ADXPHOUSENUMBERNUMERIC("/editor/cda/AdxpHouseNumberNumeric.xhtml", null, "Adxp House Number Numeric", Authorizations.ALL),
    DATATYPES_GLISTPQ("/editor/cda/GLISTPQ.xhtml", null, "GLISTPQ", Authorizations.ALL),
    DATATYPES_ST("/editor/cda/ST.xhtml", null, "ST", Authorizations.ALL),
    DATATYPES_RTOQTYQTY("/editor/cda/RTOQTYQTY.xhtml", null, "RTOQTYQTY", Authorizations.ALL),
    DATATYPES_BXITIVLPQ("/editor/cda/BXITIVLPQ.xhtml", null, "BXITIVLPQ", Authorizations.ALL),
    DATATYPES_BL("/editor/cda/BL.xhtml", null, "BL", Authorizations.ALL),
    DATATYPES_ADXPHOUSENUMBER("/editor/cda/AdxpHouseNumber.xhtml", null, "Adxp House Number", Authorizations.ALL),
    DATATYPES_ADXPSTATE("/editor/cda/AdxpState.xhtml", null, "Adxp State", Authorizations.ALL),
    DATATYPES_THUMBNAIL("/editor/cda/Thumbnail.xhtml", null, "Thumbnail", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYMODEIDENTIFIER("/editor/cda/AdxpDeliveryModeIdentifier.xhtml", null, "Adxp Delivery Mode Identifier", Authorizations.ALL),
    DATATYPES_IVXBPPDPQ("/editor/cda/IVXBPPDPQ.xhtml", null, "IVXBPPDPQ", Authorizations.ALL),
    DATATYPES_SLISTPQ("/editor/cda/SLISTPQ.xhtml", null, "SLISTPQ", Authorizations.ALL),
    DATATYPES_ANYNONNULL("/editor/cda/ANYNonNull.xhtml", null, "ANYNon Null", Authorizations.ALL),
    DATATYPES_PQ("/editor/cda/PQ.xhtml", null, "PQ", Authorizations.ALL),
    DATATYPES_ANY("/editor/cda/ANY.xhtml", null, "ANY", Authorizations.ALL),
    DATATYPES_AD("/editor/cda/AD.xhtml", null, "AD", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYINSTALLATIONTYPE("/editor/cda/AdxpDeliveryInstallationType.xhtml", null, "Adxp Delivery Installation Type",
            Authorizations.ALL),
    DATATYPES_SXCMPPDTS("/editor/cda/SXCMPPDTS.xhtml", null, "SXCMPPDTS", Authorizations.ALL),
    DATATYPES_CD("/editor/cda/CD.xhtml", null, "CD", Authorizations.ALL),
    DATATYPES_IVLPPDTS("/editor/cda/IVLPPDTS.xhtml", null, "IVLPPDTS", Authorizations.ALL),
    DATATYPES_IVLMO("/editor/cda/IVLMO.xhtml", null, "IVLMO", Authorizations.ALL),
    DATATYPES_SXCMREAL("/editor/cda/SXCMREAL.xhtml", null, "SXCMREAL", Authorizations.ALL),
    DATATYPES_ENFAMILY("/editor/cda/EnFamily.xhtml", null, "En Family", Authorizations.ALL),
    DATATYPES_ADXPCAREOF("/editor/cda/AdxpCareOf.xhtml", null, "Adxp Care Of", Authorizations.ALL),
    DATATYPES_RTOPQPQ("/editor/cda/RTOPQPQ.xhtml", null, "RTOPQPQ", Authorizations.ALL),
    DATATYPES_BXITCD("/editor/cda/BXITCD.xhtml", null, "BXITCD", Authorizations.ALL),
    DATATYPES_PQR("/editor/cda/PQR.xhtml", null, "PQR", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYMODE("/editor/cda/AdxpDeliveryMode.xhtml", null, "Adxp Delivery Mode", Authorizations.ALL),
    DATATYPES_ADXPBUILDINGNUMBERSUFFIX("/editor/cda/AdxpBuildingNumberSuffix.xhtml", null, "Adxp Building Number Suffix", Authorizations.ALL),
    DATATYPES_IVXBINT("/editor/cda/IVXBINT.xhtml", null, "IVXBINT", Authorizations.ALL),
    DATATYPES_ADXPUNITID("/editor/cda/AdxpUnitID.xhtml", null, "Adxp Unit ID", Authorizations.ALL),
    DATATYPES_ADXPCITY("/editor/cda/AdxpCity.xhtml", null, "Adxp City", Authorizations.ALL),
    DATATYPES_REAL("/editor/cda/REAL.xhtml", null, "REAL", Authorizations.ALL),
    DATATYPES_TN("/editor/cda/TN.xhtml", null, "TN", Authorizations.ALL),
    DATATYPES_BIN("/editor/cda/BIN.xhtml", null, "BIN", Authorizations.ALL),
    DATATYPES_ADXPDELIVERYADDRESSLINE("/editor/cda/AdxpDeliveryAddressLine.xhtml", null, "Adxp Delivery Address Line", Authorizations.ALL),
    DATATYPES_IVXBPPDTS("/editor/cda/IVXBPPDTS.xhtml", null, "IVXBPPDTS", Authorizations.ALL),
    DATATYPES_ON("/editor/cda/ON.xhtml", null, "ON", Authorizations.ALL),
    DATATYPES_SC("/editor/cda/SC.xhtml", null, "SC", Authorizations.ALL),
    DATATYPES_ADXPCOUNTY("/editor/cda/AdxpCounty.xhtml", null, "Adxp County", Authorizations.ALL),
    DATATYPES_II("/editor/cda/II.xhtml", null, "II", Authorizations.ALL),
    DATATYPES_ADXPCENSUSTRACT("/editor/cda/AdxpCensusTract.xhtml", null, "Adxp Census Tract", Authorizations.ALL),
    DATATYPES_ENDELIMITER("/editor/cda/EnDelimiter.xhtml", null, "En Delimiter", Authorizations.ALL),
    DATATYPES_ADXPCOUNTRY("/editor/cda/AdxpCountry.xhtml", null, "Adxp Country", Authorizations.ALL),
    DATATYPES_ADXPDELIMITER("/editor/cda/AdxpDelimiter.xhtml", null, "Adxp Delimiter", Authorizations.ALL),
    DATATYPES_UVPTS("/editor/cda/UVPTS.xhtml", null, "UVPTS", Authorizations.ALL),
    DATATYPES_HXITPQ("/editor/cda/HXITPQ.xhtml", null, "HXITPQ", Authorizations.ALL),
    DATATYPES_IVLTS("/editor/cda/IVLTS.xhtml", null, "IVLTS", Authorizations.ALL),
    DATATYPES_ED("/editor/cda/ED.xhtml", null, "ED", Authorizations.ALL),
    DATATYPES_ENSUFFIX("/editor/cda/EnSuffix.xhtml", null, "En Suffix", Authorizations.ALL),
    DATATYPES_ENXP("/editor/cda/ENXP.xhtml", null, "ENXP", Authorizations.ALL),
    DATATYPES_ADXPADDITIONALLOCATOR("/editor/cda/AdxpAdditionalLocator.xhtml", null, "Adxp Additional Locator", Authorizations.ALL),
    DATATYPES_TS("/editor/cda/TS.xhtml", null, "TS", Authorizations.ALL),
    DATATYPES_SXCMPQ("/editor/cda/SXCMPQ.xhtml", null, "SXCMPQ", Authorizations.ALL),

    CDA_SAMPLES("/editor/samples.xhtml", null, " CDA samples", Authorizations.ALL),
    CDA_DETAIL("/cda.xhtml", null, " CDA document", Authorizations.ALL),


    HL7_TEMPLATES_VALIDATION("/hl7templates/hl7validation.xhtml", null, "HL7 templates validation", Authorizations.LOGGED),
    HL7_TEMPLATES_VALIDATIONS("/hl7templates/hl7validations.xhtml", null, "HL7 templates validations", Authorizations.LOGGED),
    HL7_TEMPLATES_VALIDATION_RESULT("/hl7templates/hl7validationResult.xhtml", null, "HL7 templates validations result", Authorizations.LOGGED),


    UPLOAD_CONSTRAINTS("/docum/upload/uploadConstraints.xhtml", null, "upload Constraints", Authorizations.ADMIN),
    MISSING_ASSERTIONS("/docum/packages/missingAssertions.xhtml", null, "missing Assertions", Authorizations.ADMIN),
    APPLICATION_CONFIGURATION("/admin/applicationConfiguration.xhtml", null, "application Configuration", Authorizations.ADMIN),
    DISPLAY_APPLICATION_PREFERENCE("/admin/displayApplicationPreference.xhtml", null, "display application preference", Authorizations.ADMIN),
    BUILDING_BLOCK_REPOSITORY_LIST("/admin/buildingBlockRepository/list.xhtml", null, "building Block Repository list", Authorizations.ADMIN),
    BUILDING_BLOCK_REPOSITORY_VIEW("/admin/buildingBlockRepository/view.xhtml", null, "building Block Repository view", Authorizations.ADMIN),
    BUILDING_BLOCK_REPOSITORY_EDIT("/admin/buildingBlockRepository/edit.xhtml", null, "building Block Repository edit", Authorizations.ADMIN),
    BUILDING_BLOCK_REPOSITORY_NEW("/admin/buildingBlockRepository/new.xhtml", null, "building Block Repository new", Authorizations.ADMIN),
    BUILDING_BLOCK_REPOSITORY_LOG("/admin/buildingBlockRepository/log.xhtml", null, "building Block Repository log", Authorizations.ADMIN),
    VALIDATOR_ASSOCIATION_LIST("/admin/validatorAssociation/list.xhtml", null, "validator Association list", Authorizations.ADMIN),
    VALIDATOR_ASSOCIATION_VIEW("/admin/validatorAssociation/view.xhtml", null, "validator Association view", Authorizations.ADMIN),
    VALIDATOR_ASSOCIATION_EDIT("/admin/validatorAssociation/edit.xhtml", null, "validator Association edit", Authorizations.ADMIN),
    VALIDATOR_ASSOCIATION_NEW("/admin/validatorAssociation/new.xhtml", null, "validator Association new", Authorizations.ADMIN),
    TEMPLATES_CONTAINMENT_LIST("/admin/templatesContainment/list.xhtml", null, "templates Containment list", Authorizations.ADMIN),
    TEMPLATES_CONTAINMENT_VIEW("/admin/templatesContainment/view.xhtml", null, "templates Containment view", Authorizations.ADMIN),
    TEMPLATES_CONTAINMENT_EDIT("/admin/templatesContainment/edit.xhtml", null, "templates Containment edit", Authorizations.ADMIN),
    TEMPLATES_CONTAINMENT_NEW("/admin/templatesContainment/new.xhtml", null, "templates Containment new", Authorizations.ADMIN);


    private String link;

    private final Authorization[] authorizations;

    private String label;

    private final String icon;

    Pages(final String link, final String icon, final String label, final Authorization... authorizations) {
        this.link = link;
        this.authorizations = authorizations.clone();
        this.label = label;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return this.name();
    }

    @Override
    public String getLink() {
        return this.link;
    }


    @Override
    public String getIcon() {
        return this.icon;
    }

    @Override
    public Authorization[] getAuthorizations() {

        if (this.authorizations != null) {
            return this.authorizations.clone();
        } else {
            return null;
        }
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getMenuLink() {
        return this.link.replace(".xhtml", ".seam");
    }

    private void setLink(final String link) {
        this.link = link;
    }

    private void setLabel(final String label) {
        this.label = label;
    }
}
