/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cdagen.edispensation.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.annotations.Type;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import net.ihe.gazelle.cdagen.edispensation.model.DatabaseUtil.RestrictionType;
import net.ihe.gazelle.cdagen.edispensation.model.DatabaseUtil.When;

/**
 * <b>Class Description : </b>ValidatedObject<br>
 * <br>
 * This abstract class is intended to be extended by other classes, the ones
 * will describe the various objects the EVS Client can be used for validating
 * them (eg HL7v2, HL7v3, CDA and so on)
 * 
 * ValidatedObject possesses the following attributes :
 * <ul>
 * <li><b>oid</b> : the object OID given by the validator or by the client if
 * none is sent back</li>
 * <li><b>user</b> : the user who called the service (if it was logged)</li>
 * <li><b>validationDate</b> : validation date</li>
 * <li><b>service</b> : validation service called</li>
 * <li><b>validationStatus</b> : main result of the validation (failed, passed)</li>
 * <li><b>validationContext</b> : validation context (actor, profile,
 * messageType)</li>
 * <li><b>metadata</b> : some more information about the validated object
 * <li><b>detailedResults</b> : String representing the results of the
 * validation
 * 
 * @class ValidatedObject.java
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 */

@MappedSuperclass
public abstract class ValidatedObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7482942984080329263L;

	@Logger
	private static Log log;

	@Column(name = "oid")
	private String oid;

	@Column(name = "private_user")
	private String privateUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "validation_date")
	protected Date validationDate;

	@Column(name = "user_ip")
	private String userIp;

	/**
	 * validatationStatus value must be one of the ValidationStatus enum class
	 * (label attribute)
	 */
	@Column(name = "validation_status")
	private String validationStatus;

	@Lob
	@Type(type = "text")
	@Column(name = "validation_context")
	private String validationContext;

	@Column(name = "description")
	protected String description;

	@Lob
	@Type(type = "text")
	@Column(name = "detailed_result")
	private String detailedResult;

	@Lob
	@Type(type = "text")
	@Column(name = "metadata")
	private String metadata;

	/**
	 * Constructor
	 */
	public ValidatedObject() {

	}

	/**
	 * Getters and Setters
	 */

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public Date getValidationDate() {
		return validationDate;
	}

	public void setValidationDate(Date validationDate) {
		this.validationDate = validationDate;
	}

	public String getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}

	public String getValidationContext() {
		return validationContext;
	}

	public void setValidationContext(String validationContext) {
		this.validationContext = validationContext;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDetailedResult(String detailedResult) {
		this.detailedResult = detailedResult;
	}

	public String getDetailedResult() {
		return detailedResult;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getMetadata() {
		return metadata;
	}

	public String getPrivateUser() {
		return privateUser;
	}

	public void setPrivateUser(String privateUser) {
		this.privateUser = privateUser;
	}

	@PrePersist
	@PreUpdate
	public void init() {
		if (Identity.instance().isLoggedIn()) {
			this.setPrivateUser(Identity.instance().getCredentials()
					.getUsername());
		} else {
			this.setPrivateUser(null);
		}
	}

	public static <T extends ValidatedObject> T getObjectByID(Class<T> clazz, Integer id) {
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		Session s = (Session) em.getDelegate();
		Criteria c = s.createCriteria(clazz);
		c.add(Restrictions.eq("id", id));
		addSecurityRestriction(c);
		try {
			return (T) c.uniqueResult();
		} catch (Exception e) {
			return null;
		}
	}

	public static void addSecurityRestriction(Criteria c) {
		boolean monitor = false;
		String login = null;
		
		Identity instance = Identity.instance();
		if (instance != null && Identity.instance().isLoggedIn()) {
			login = Identity.instance().getCredentials().getUsername();
			if (instance.hasRole("monitor_role")) {
				monitor = true;
			}
		}
		
		// show all messages if monitor
		if (!monitor) {
			if (login == null) {
				c.add(Restrictions.isNull("privateUser"));
			} else {
				c.add(Restrictions.or(Restrictions.isNull("privateUser"), Restrictions.eq("privateUser", login)));
			}
		}
	}

	/**
	 * 
	 * @param <T>
	 *            Object to return (must be a class extending ValidatedObject)
	 * @param clazz
	 *            Class of the object to return
	 * @param inOID
	 * @return
	 */
	public static <T extends ValidatedObject> T getObjectByOID(Class<T> clazz, String inOID) {
		if ((inOID == null) || (inOID.length() == 0))
			return null;

		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		Session s = (Session) em.getDelegate();
		Criteria c = s.createCriteria(clazz);
		c.add(Restrictions.ilike("oid", inOID));
		addSecurityRestriction(c);
		List<T> objects = (List<T>) c.list();
		if (!objects.isEmpty())
			return objects.get(0);
		else
			return null;
	}

	/**
	 * Get all object from a given class which extends ValidatedObject
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	public static <T extends ValidatedObject> List<T> getAllObjects(Class<T> clazz) {
		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(clazz);
		criteria.addOrder(Order.desc("id"));
		addSecurityRestriction(criteria);
		List<T> results = (List<T>) criteria.list();
		if (results != null && !results.isEmpty())
			return results;
		else
			return new ArrayList<>();
	}

	/**
	 * returns the last "numberOfObjectsToReturn" entries
	 * 
	 * @param <T>
	 * @param clazz
	 * @param numberOfObjectsToReturn
	 * @return
	 */
	public static <T extends ValidatedObject> List<T> getLastObjects(Class<T> clazz,
			Integer numberOfObjectsToReturn) {
		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(clazz);
		criteria.addOrder(Order.desc("id"));
		addSecurityRestriction(criteria);
		criteria.setMaxResults(numberOfObjectsToReturn);
		List<T> results = (List<T>) criteria.list();
		if (results != null && !results.isEmpty())
			return results;
		else
			return new ArrayList<>();
	}

	/**
	 * Remove the given object from the database
	 * 
	 * @param <T>
	 * @param objectToRemove
	 */
	public static <T extends ValidatedObject> void deleteObject(T objectToRemove) {
		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		if (objectToRemove != null) {
			em.remove(objectToRemove);
			em.flush();
		}
	}

	/**
	 * Gathers all the null entries of the table and delete them
	 * 
	 * @param <T>
	 * @param clazz
	 */
	public static <T extends ValidatedObject> void deleteNullEntries(Class<T> clazz) {
		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		Query query = em.createQuery("SELECT DISTINCT obj FROM "
				+ clazz.getCanonicalName()
				+ " obj WHERE obj.validationStatus is null");
		List<T> nullEntries = (List<T>) query.getResultList();
		if (!nullEntries.isEmpty()) {
			for (T obj : nullEntries)
				deleteObject(obj);
		}
	}

	/**
	 * Return a list of objects filtered depending chosen criteria
	 * 
	 * @param <T>
	 * @param clazz
	 * @param inWhen
	 * @param inDate
	 * @param inOid
	 * @param inSchematron
	 * @param inResult
	 * @param inStandard
	 * @param inService
	 * @return
	 */
	public static <T extends ValidatedObject> List<T> getObjectsFiltered(Class<T> clazz, When inWhen,
			Date inDate, String inOid, String inSchematron, String inResult) {
		EntityManager em = (EntityManager) Component
				.getInstance("entityManager");
		Session s = (Session) em.getDelegate();
		Criteria c = s.createCriteria(clazz);
		addSecurityRestriction(c);

		Criterion criterion = null;

		if (inOid != null && inOid.length() > 0) {
			criterion = DatabaseUtil.addCriterion(criterion,
					RestrictionType.EQ, "oid", inOid, null);
		}
		if (inWhen != null && inDate != null) {
			Map<String, Date> dates = DatabaseUtil.getBeginAndEnd(inDate);
			switch (inWhen) {
			case ON:
				criterion = DatabaseUtil.addCriterion(criterion,
						RestrictionType.BETWEEN, "validationDate",
						dates.get("begin"), dates.get("end"));
				break;
			case AFTER:
				criterion = DatabaseUtil.addCriterion(criterion,
						RestrictionType.GE, "validationDate",
						dates.get("begin"), null);
				break;
			case BEFORE:
				criterion = DatabaseUtil.addCriterion(criterion,
						RestrictionType.LE, "validationDate", dates.get("end"),
						null);
				break;
			default:
				break;
			}
		}
		if (inSchematron != null && inSchematron.length() > 0) {
			criterion = DatabaseUtil.addCriterion(criterion,
					RestrictionType.EQ, "schematron", inSchematron, null);
		}
		if (inResult != null && inResult.length() > 0) {
			criterion = DatabaseUtil.addCriterion(criterion,
					RestrictionType.EQ, "validationStatus", inResult, null);
		}

		if (criterion == null)
			return getAllObjects(clazz);
		else {
			c.add(criterion);
			List<T> messages = c.list();
			if (messages != null && !messages.isEmpty())
				return messages;
			else
				return new ArrayList<>();
		}

	}

	/**
	 * 
	 * @param <T>
	 * @param objectClass
	 *            : class of the object for which we need the attributes values
	 * @param attributeClazz
	 *            : class of the attribute
	 * @param parameterName
	 *            : attribute name
	 * @param inSchematron
	 * @param inResult
	 * @param inService
	 * @param inStandard
	 * @param inWhen
	 * @param inDate
	 * @return
	 */
	public static <T> List<T> getAttributePossibleValues(Class<?> objectClass,
			Class<T> attributeClazz, String parameterName, String inSchematron,
			String inResult,
			 When inWhen, Date inDate) {
		if (attributeClazz == null || parameterName == null || parameterName.length() == 0) {
			return null;
		}

		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		StringBuilder request = new StringBuilder();
		request.append("SELECT distinct obj." + parameterName + " FROM " + objectClass.getCanonicalName() + " obj ");
		String order;
		if (parameterName.equals("standard")) {
			order = " ORDER BY obj.standard.label ASC";
		}
		else if (parameterName.equals("service")) {
			order = " ORDER BY obj.service.name ASC";
		}
		else {
			order = " ORDER BY obj." + parameterName + " ASC";
		}

		StringBuilder parameters = new StringBuilder();

		HashMap<String, Object> parameterValues = new HashMap<String, Object>();

		if (inSchematron != null && inSchematron.length() > 0) {
			DatabaseUtil.addANDorWHERE(parameters);
			parameters.append("obj.schematron = :schematron");
			parameterValues.put("schematron", inSchematron);
		}
		if (inResult != null && inResult.length() > 0) {
			DatabaseUtil.addANDorWHERE(parameters);
			parameters.append("obj.validationStatus = :result");
			parameterValues.put("result", inResult);
		}
		
		if (inWhen != null && inDate != null) {
			Map<String, Date> dates = DatabaseUtil.getBeginAndEnd(inDate);
			DatabaseUtil.addANDorWHERE(parameters);
			parameters.append("obj.validationDate ");
			switch (inWhen) {
			case ON:
				parameters.append(">= :date1");
				parameters.append(" AND obj.validationDate < :date2");
				parameterValues.put("date1", dates.get("begin"));
				parameterValues.put("date2", dates.get("end"));
				break;
			case AFTER:
				parameters.append(">= :date1");
				parameterValues.put("date1", dates.get("begin"));
				break;
			case BEFORE:
				parameters.append("< :date2");
				parameterValues.put("date2", dates.get("end"));
				break;
			default:
				parameters.append("= :date1");
				parameterValues.put("date1", dates.get("begin"));
				break;
			}
		}
		if (parameters.length() == 0) {
			request.append(order);
			Query query = em.createQuery(request.toString());
			List<T> returnedValues = (List<T>) query.getResultList();
			if (returnedValues != null && returnedValues.size() > 0) {
				return returnedValues;
			}
			return null;
		} else {
			request.append(parameters.toString());
			request.append(order);
			Query query = em.createQuery(request.toString());
			for (String param : parameterValues.keySet()) {
				query.setParameter(param, parameterValues.get(param));
			}
			List<T> returnedValues = (List<T>) query.getResultList();
			if (returnedValues != null && !returnedValues.isEmpty()) {
				return returnedValues;
			}
			return new ArrayList<>();
		}
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result
                + ((detailedResult == null) ? 0 : detailedResult.hashCode());
        result = prime * result
                + ((metadata == null) ? 0 : metadata.hashCode());
        result = prime * result + ((oid == null) ? 0 : oid.hashCode());
        result = prime * result
                + ((privateUser == null) ? 0 : privateUser.hashCode());
        result = prime * result + ((userIp == null) ? 0 : userIp.hashCode());
        result = prime
                * result
                + ((validationContext == null) ? 0 : validationContext
                        .hashCode());
        result = prime * result
                + ((validationDate == null) ? 0 : validationDate.hashCode());
        result = prime
                * result
                + ((validationStatus == null) ? 0 : validationStatus.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ValidatedObject other = (ValidatedObject) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (detailedResult == null) {
            if (other.detailedResult != null)
                return false;
        } else if (!detailedResult.equals(other.detailedResult))
            return false;
        if (metadata == null) {
            if (other.metadata != null)
                return false;
        } else if (!metadata.equals(other.metadata))
            return false;
        if (oid == null) {
            if (other.oid != null)
                return false;
        } else if (!oid.equals(other.oid))
            return false;
        if (privateUser == null) {
            if (other.privateUser != null)
                return false;
        } else if (!privateUser.equals(other.privateUser))
            return false;
        if (userIp == null) {
            if (other.userIp != null)
                return false;
        } else if (!userIp.equals(other.userIp))
            return false;
        if (validationContext == null) {
            if (other.validationContext != null)
                return false;
        } else if (!validationContext.equals(other.validationContext))
            return false;
        if (validationDate == null) {
            if (other.validationDate != null)
                return false;
        } else if (!validationDate.equals(other.validationDate))
            return false;
        if (validationStatus == null) {
            if (other.validationStatus != null)
                return false;
        } else if (!validationStatus.equals(other.validationStatus))
            return false;
        return true;
    }
}
