package net.ihe.gazelle.cdagen.simulator.ws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.SOAPException;

import org.apache.tika.Tika;
import org.jboss.seam.util.Base64;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.utils.DecodeUtil;

public abstract class AbstractModelBasedValidation
		implements
			ModelBasedValidationRemote {
	
	
	private static Logger log = LoggerFactory.getLogger(AbstractModelBasedValidation.class);

	@WebMethod
	@WebResult(name = "about")
	public String about() {
		String disclaimer = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate XML documents using model based validation.\n";
		disclaimer = disclaimer
				+ "For more information please contact the manager of Gazelle project: eric.poiseau@inria.fr";
		return disclaimer;
	}

	/**
     * Validate a document based on the validator name, the document is in B64 format.
     * @param document : the CDA document to be validated, in B64 format.
     * @param validator : the validator name.validateBase64Document
     * @return A string containing the validation result
     */
	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateBase64Document(
			@WebParam(name = "base64Document") String base64Document,
			@WebParam(name = "validator") String validator)
			throws SOAPException {
		if ((null == base64Document) || (null == validator)){
			throw new SOAPException("The validator or the document can't be empty !");
		}
		byte[] buffer = DatatypeConverter.parseBase64Binary(base64Document);
		if (buffer != null) {
			Tika tika = new Tika();
			String mediaType = tika.detect(buffer);
			log.info("detected mediaType: " + mediaType);
			String document = DecodeUtil.decodeDocument(buffer, mediaType);
			return this.validateDocument(document, validator);
		}
		throw new SOAPException("The tool was not able to understand the content of the file");
	}
	
	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateB64Document(
			@WebParam(name = "base64Document") String base64Document,
			@WebParam(name = "validator") String validator,
			@WebParam(name = "resultMediaType") String resultMediaType)
			throws SOAPException {
		if ((null == base64Document) || (null == validator)){
			throw new SOAPException("The validator or the document can't be empty !");
		}
		byte[] buffer = DatatypeConverter.parseBase64Binary(base64Document);
		if (buffer != null) {
			Tika tika = new Tika();
			String mediaType = tika.detect(buffer);
			log.info("detected mediaType: " + mediaType);
			String document = DecodeUtil.decodeDocument(buffer, mediaType);
			String res = this.validateDocument(document, validator);
			if (res != null) {
				try (ByteArrayInputStream is = new ByteArrayInputStream(res.getBytes());
						ByteArrayOutputStream os = new ByteArrayOutputStream();){
					DecodeUtil.encodeDocument(is, os, resultMediaType);
					return DatatypeConverter.printBase64Binary(os.toByteArray());
				} catch (IOException e) {
					log.error("Issue to parse the res", e.getMessage());
				}
			}
			return res;
		}
		throw new SOAPException("The tool was not able to understand the content of the file");
	}
	
	public abstract List<String> getListSupportedMediaTypes();
	
}
