package net.ihe.gazelle.cdagen.model;

public enum CDAType {
    FRIENDLY("friendly"), PIVOT("pivot");
    
    private String value;

    public String getValue() {
        return value;
    }

    private CDAType(String val) {
        this.value = val;
    }
    
}
