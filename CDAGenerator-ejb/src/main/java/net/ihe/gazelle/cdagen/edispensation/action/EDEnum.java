package net.ihe.gazelle.cdagen.edispensation.action;

public enum EDEnum {
    
    ID("id"),EXTENSION("extension"), AA("aa"), EFFECTIVETIME("effectivetime"), LANGUAGE("language"), 
    PHARMA_ID_ROOT("pharma.id.root"), PHARMA_ID_EXTENSION("pharma.id.extension"), 
    PHARMA_ID_ASSIGNINGAUTHORITYNAME("pharma.id.assigningAuthorityName"), PHARMA_GIVEN("pharma.given"), 
    PHARMA_FAMILY("pharma.family"), PHARMA_ORG_ID_ROOT("pharma.org.id.root"),
    PHARMA_ORG_ID_EXTENSION("pharma.org.id.extension"), PHARMA_ORG_ID_NAME("pharma.org.name"),
    CUTODIAN_ID_ROOT("custodian.id.root"), CUTODIAN_ID_EXTENSION("custodian.id.extension"),
    CUTODIAN_NAME("custodian.name");
    
    private String value;
    
    
    
    public String getValue() {
        return value;
    }



    EDEnum(String val){
        this.value = val;
    }

}
