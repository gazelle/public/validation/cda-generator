package net.ihe.gazelle.cdagen.eprescription.action;

import org.jdom.Element;
import org.jdom.Namespace;

import net.ihe.gazelle.cdagen.eprescription.model.EpFormCode;

public final class EntryGenerator {
	
	private static final String NULL_FLAVOR = "nullFlavor";


	private static final String CLASS_CODE = "classCode";


	private static final String CODE = "code";


	private static final String DISPLAY_NAME = "displayName";


	private static final String CODE_SYSTEM_NAME = "codeSystemName";


	private static final String CODE_SYSTEM = "codeSystem";


	private static final String TEMPLATE_ID = "templateId";

	
    public static final String EPSOSXMLNS = "urn:epsos-org:ep:medication";
    
    private EntryGenerator() {
		// private constructor
	}
    
    private static Element generateEl(String name, String attr, String valAttr){
        Element el = new Element(name, PrescGenerator.NAMESPACE);
        el.setAttribute(attr, valAttr);
        return el;
    }
    
    public static Element entry(){
        return new Element("entry", PrescGenerator.NAMESPACE);
    }
    
    public static Element substanceAdministration(String extension, String root){
        Element el = new Element("substanceAdministration", PrescGenerator.NAMESPACE);
        el.addContent(generateEl(TEMPLATE_ID, "root", "1.3.6.1.4.1.12559.11.10.1.3.1.3.2"));
        el.addContent(generateEl(TEMPLATE_ID, "root", "2.16.840.1.113883.10.20.1.24"));
        el.addContent(id(extension, root));
        el.addContent(generateEl("statusCode", CODE, "active"));
        el.addContent(effectiveTime());
        el.addContent(effectiveTime2());
        el.addContent(routeCode());
        el.addContent(generateEl("doseQuantity", NULL_FLAVOR, "NI"));
        return el;
    }
    
    public static Element id(String extension, String root){
        Element el = new Element("id", PrescGenerator.NAMESPACE);
        el.setAttribute("extension", extension);
        el.setAttribute("root", root);
        return el;
    }
    
    public static Element effectiveTime(){
        Element el = new Element("effectiveTime", PrescGenerator.NAMESPACE);
        el.setAttribute("type", "IVL_TS", Namespace.getNamespace("http://www.w3.org/2001/XMLSchema-instance"));
        el.addContent(generateEl("low", NULL_FLAVOR, "UNK"));
        el.addContent(generateEl("high", NULL_FLAVOR, "UNK"));
        return el;
    }
    
    public static Element effectiveTime2(){
        Element el = new Element("effectiveTime", PrescGenerator.NAMESPACE);
        el.setAttribute("type", "PIVL_TS", Namespace.getNamespace("http://www.w3.org/2001/XMLSchema-instance"));
        el.setAttribute("operator", "A");
        el.addContent(generateEl("period", NULL_FLAVOR, "NI"));
        return el;
    }
    
    public static Element routeCode(){
        Element el = new Element("routeCode", PrescGenerator.NAMESPACE);
        el.setAttribute(CODE, "20053000");
        el.setAttribute(CODE_SYSTEM, "1.3.6.1.4.1.12559.11.10.1.3.1.44.1");
        el.setAttribute(CODE_SYSTEM_NAME, "EDQM");
        el.setAttribute(DISPLAY_NAME, "orale");
        return el;
    }
    
    public static Element consumable(){
        return new Element("consumable", PrescGenerator.NAMESPACE);
    }
    
    public static Element manufacturedProduct(){
        Element el = new Element("manufacturedProduct", PrescGenerator.NAMESPACE);
        el.setAttribute(CLASS_CODE, "MANU");
        el.setNamespace(Namespace.getNamespace(EPSOSXMLNS));
        el.addContent(generateEl(TEMPLATE_ID, "root", "1.3.6.1.4.1.19376.1.5.3.1.4.7.2"));
        el.addContent(generateEl(TEMPLATE_ID, "root", "2.16.840.1.113883.10.20.1.53"));
        
        return el;
    }
    
    public static Element manufacturedMaterial(){
        Element el = new Element("manufacturedProduct", PrescGenerator.NAMESPACE);
        el.setAttribute(CLASS_CODE, "MMAT");
        el.setAttribute("determinerCode", "KIND");
        el.addContent(generateEl(TEMPLATE_ID, "root", "1.3.6.1.4.1.12559.11.10.1.3.1.3.1"));
        
        return el;
    }
    
    public static Element code(String code, String codeSystem, 
            String codeSystemName, String displayName, String ref){
        Element el = new Element(CODE, PrescGenerator.NAMESPACE);
        el.setAttribute(CODE, code);
        el.setAttribute(CODE_SYSTEM, codeSystem);
        el.setAttribute(CODE_SYSTEM_NAME, codeSystemName);
        el.setAttribute(DISPLAY_NAME, displayName);
        el.addContent(originalText(ref));
        return el;
    }
    
    public static Element originalText(String ref){
        Element el = new Element("originalText", PrescGenerator.NAMESPACE);
        el.addContent(generateEl("reference", "value", ref));
        return el;
    }
    
    public static Element name(String name){
        Element el = new Element("name", PrescGenerator.NAMESPACE);
        el.setText(name);
        return el;
    }
    
    public static Element formCode(EpFormCode fc){
        Element el = new Element("formCode", EPSOSXMLNS);
        if (fc != null){
            el.setAttribute(CODE, fc.code);
            el.setAttribute(CODE_SYSTEM, fc.codeSystem);
            el.setAttribute(CODE_SYSTEM_NAME, fc.codeSystemName);
            el.setAttribute(DISPLAY_NAME, fc.displayName);
        }
        else{
            el.setAttribute(NULL_FLAVOR, "NI");
        }
        return el;
    }
    
    public static Element asContent(EpFormCode fc, String name){
        Element el = new Element("asContent", EPSOSXMLNS);
        el.setAttribute(CLASS_CODE, "COINT");
        el.addContent(containerPackagedMedicine(fc, name));
        return el;
    }
    
    public static Element containerPackagedMedicine(EpFormCode fc,
            String name){
        Element el = new Element("containerPackagedMedicine", EPSOSXMLNS);
        el.setAttribute(CLASS_CODE, "CONT");
        el.setAttribute("determinerCode", "INSTANCE");
        el.addContent(epname(name));
        el.addContent(formCode(fc));
        el.addContent(capacityQuantity("1", "1"));
        return el;
    }
    
    public static Element epname(String name){
        Element el = new Element("name", EPSOSXMLNS);
        el.setText(name);
        return el;
    }
    
    public static Element capacityQuantity(String unit, String value){
        Element el = new Element("capacityQuantity", EPSOSXMLNS);
        el.setAttribute("unit", unit);
        el.setAttribute("value", value);
        return el;
    }
    
    
    
    

}
