package net.ihe.gazelle.cdagen.eprescription.action;

import org.jdom.Element;

import net.ihe.gazelle.cdagen.tools.JdomHelper;

public class ComponentGenerator {
    
    private static Element generateEl(String name, String attr, String valAttr){
        Element el = new Element(name, PrescGenerator.NAMESPACE);
        el.setAttribute(attr, valAttr);
        return el;
    }
    
    public static Element component(String extension, String root){
        Element el = new Element("component", PrescGenerator.NAMESPACE);
        el.setAttribute("contextConductionInd", "true");
        el.setAttribute("typeCode", "COMP");
        el.addContent(structuredBody(extension, root));
        return el;
    }
    
    public static Element structuredBody(String extension, String root){
        Element el = new Element("structuredBody", PrescGenerator.NAMESPACE);
        el.setAttribute("classCode", "DOCBODY");
        el.setAttribute("moodCode", "EVN");
        el.addContent(component2(extension, root));
        return el;
    }
    
    public static Element component2(String extension, String root){
        Element el = new Element("component", PrescGenerator.NAMESPACE);
        el.addContent(section(extension, root));
        return el;
    }
    
    public static Element section(String extension, String root){
        Element el = new Element("section", PrescGenerator.NAMESPACE);
        el.setAttribute("classCode", "DOCSECT");
        el.setAttribute("moodCode", "EVN");
        el.addContent(generateEl("templateId", "root", "1.3.6.1.4.1.12559.11.10.1.3.1.2.1"));
        el.addContent(id(extension, root));
        el.addContent(code());
        el.addContent(title());
        return el;
    }
    
    public static Element id(String extension, String root){
        Element el = new Element("id", PrescGenerator.NAMESPACE);
        el.setAttribute("extension", extension);
        el.setAttribute("root", root);
        return el;
    }
    
    public static Element code(){
        Element el = new Element("code", PrescGenerator.NAMESPACE);
        el.setAttribute("code", "57828-6");
        el.setAttribute("codeSystem", "2.16.840.1.113883.6.1");
        el.setAttribute("codeSystemName", "LOINC");
        el.setAttribute("displayName", "Prescriptions");
        return el;
    }
    
    public static Element title(){
        Element el = new Element("title", PrescGenerator.NAMESPACE);
        el.setText("Prescription");
        return el;
    }
    
    public static void main(String[] args) {
        Element e = ComponentGenerator.component("extension", "root");
        System.out.println(JdomHelper.element2String(e));
        
    }

}
