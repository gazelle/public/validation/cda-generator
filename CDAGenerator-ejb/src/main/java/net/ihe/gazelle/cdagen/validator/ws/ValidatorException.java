package net.ihe.gazelle.cdagen.validator.ws;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.validation.Notification;

public class ValidatorException extends JAXBException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Notification> diagnostic;

	public List<Notification> getDiagnostic() {
		if (diagnostic == null) {
			diagnostic = new ArrayList<>();
		}
		return diagnostic;
	}

	public void setDiagnostic(List<Notification> diagnostic) {
		this.diagnostic = diagnostic;
	}

	public ValidatorException(String message) {
		super(message);
	}

}
