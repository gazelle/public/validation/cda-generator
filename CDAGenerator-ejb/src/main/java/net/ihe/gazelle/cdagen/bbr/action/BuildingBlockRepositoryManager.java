package net.ihe.gazelle.cdagen.bbr.action;

import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.cdagen.bbr.model.JavaVersionEnum;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.OtherFile;
import net.ihe.gazelle.cdagen.bbr.model.StatusEnum;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * Created by xfs on 25/11/16.
 */

@Name("buildingBlockRepositoryManager")
@Scope(ScopeType.PAGE)
public class BuildingBlockRepositoryManager {

    private static Logger LOG = LoggerFactory.getLogger(BuildingBlockRepositoryManager.class);

    private BuildingBlockRepository selectedBuildingBlockRepository;

    private boolean editMode;

    public static String readBashLogs(String name) throws IOException {
        StringBuilder res = new StringBuilder();
        try (BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name), Charset.forName("UTF-8")))) {
            for (String line = scanner.readLine(); line != null; line = scanner.readLine()) {
                // Remove ansi char color
                line = line.replaceAll("(.)[\\[0-9;]+m", "");
                res.append(line).append("\n");
            }
        }
        return res.toString();
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public BuildingBlockRepository getSelectedBuildingBlockRepository() {
        return selectedBuildingBlockRepository;
    }

    public void setSelectedBuildingBlockRepository(BuildingBlockRepository selectedBuildingBlockRepository) {
        this.selectedBuildingBlockRepository = selectedBuildingBlockRepository;
    }

    public void deleteSelectedBuildingBlockRepository() {
        EntityManager em = EntityManagerService.provideEntityManager();
        BuildingBlockRepository bbrToDelete = em.find(BuildingBlockRepository.class,
                selectedBuildingBlockRepository.getId());
        em.remove(bbrToDelete);
    }

    public StatusEnum[] getAvailableStatus() {
        return StatusEnum.values();
    }

    public JavaVersionEnum[] getAvailableJavaVersions(){
        return JavaVersionEnum.values();
    }

    public String save() {
        EntityManager em = EntityManagerService.provideEntityManager();
        selectedBuildingBlockRepository.setCreationDate(new Date());
        if (!editMode) {
            selectedBuildingBlockRepository.setStatus(StatusEnum.AUTOMATIC);
        }
        byte[] contentxsd = this.selectedBuildingBlockRepository.getXsdZipFile() != null ?
                this.selectedBuildingBlockRepository.getXsdZipFile().getContent() : null;
        List<OtherFile> lof = this.selectedBuildingBlockRepository.getOtherFiles();
        this.selectedBuildingBlockRepository = em.merge(this.selectedBuildingBlockRepository);
        em.flush();
        updateContentOtherFiles(this.selectedBuildingBlockRepository, lof);
        savePossibleXSDFile(contentxsd, selectedBuildingBlockRepository.getXsdZipFile());
        savePossibleOtherFiles(selectedBuildingBlockRepository.getOtherFiles());
        return "/admin/buildingBlockRepository/list.seam";
    }


    private void updateContentOtherFiles(BuildingBlockRepository selectedBuildingBlockRepository2,
                                         List<OtherFile> lof) {
        if (selectedBuildingBlockRepository2 != null) {
            for (OtherFile otherFile : selectedBuildingBlockRepository2.getOtherFiles()) {
                OtherFile orig = detectOriginalFromName(lof, otherFile.getFileName());
                if (orig != null) {
                    otherFile.setContent(orig.getContent());
                }
            }
        }
    }

    private OtherFile detectOriginalFromName(List<OtherFile> lof, String fileName) {
        if (fileName != null && lof != null) {
            for (OtherFile otherFile : lof) {
                if (fileName.equals(otherFile.getFileName())) {
                    return otherFile;
                }
            }
        }
        return null;
    }

    private void savePossibleOtherFiles(List<OtherFile> otherFiles) {
        for (OtherFile otherFile : otherFiles) {
            if (otherFile.getContent() != null && otherFile.getId() != null) {
                String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
                String filepath = repo + File.separator + otherFile.getId() + "_" + otherFile.getFileName();
                try {
                    FileUtils.writeByteArrayToFile(new File(filepath), otherFile.getContent());
                } catch (IOException e) {
                    LOG.info("Problem to write on file :" + filepath, e);
                }
            } else {
                LOG.error("Problem to detect the content of the file and its id");
            }
        }
    }

    private void savePossibleXSDFile(byte[] contentxsd, OtherFile xsdZipFile) {
        if (xsdZipFile != null && xsdZipFile.getId() != null && contentxsd != null &&
                xsdZipFile.getFileName() != null) {
            String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
            String filepath = repo + File.separator + xsdZipFile.getId() + "_" + xsdZipFile.getFileName();
            try {
                FileUtils.writeByteArrayToFile(new File(filepath), contentxsd);
            } catch (IOException e) {
                LOG.info("Problem to write on file :" + filepath, e);
            }
        }
    }

    public void initSelectedBuildingBlockRepository() {

        EntityManager em = EntityManagerService.provideEntityManager();
        final FacesContext fc = FacesContext.getCurrentInstance();
        final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
        if (requestParameterMap.get("bbrId") != null) {
            editMode = true;
            Integer id = Integer.parseInt(requestParameterMap.get("bbrId"));
            selectedBuildingBlockRepository = em.find(BuildingBlockRepository.class, id);
        } else {
            editMode = false;
            selectedBuildingBlockRepository = new BuildingBlockRepository();
        }

    }

    public void uploadBbrListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        selectedBuildingBlockRepository.setBbrContent(item.getData());
    }

    public void uploadOtherFilesListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        OtherFile of = new OtherFile();
        of.setContent(item.getData());
        of.setFileName(item.getName());
        Tika tika = new Tika();
        String mediaType = tika.detect(of.getContent());
        of.setMimeType(mediaType);
        selectedBuildingBlockRepository.getOtherFiles().add(of);
    }

    public String displayLogs() {

        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        File fileOutputLevel = new File(
                bbrPathString + "/generatedValidators/execs/" + selectedBuildingBlockRepository.getId() + "/logs.txt");
        String content = "";
        try {
            content = readBashLogs(fileOutputLevel.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Problem to display logs", e);
        }
        return content;
    }

    public void downloadXSDFile(BuildingBlockRepository bbr) throws IOException {
        if (bbr != null && bbr.getXsdZipFile() != null && bbr.getXsdZipFile().getContent() != null) {
            InputStream is = new BufferedInputStream(new ByteArrayInputStream(bbr.getXsdZipFile().getContent()));
            String mimeType = URLConnection.guessContentTypeFromStream(is);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType(mimeType);
            response.setContentLength(bbr.getXsdZipFile().getContent().length);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + bbr.getXsdZipFile().getFileName() + "\"");
            servletOutputStream.write(bbr.getXsdZipFile().getContent());
            servletOutputStream.flush();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        }
    }

    public void deleteOtherFile(OtherFile of) {
        if (of != null && this.selectedBuildingBlockRepository != null) {
            this.selectedBuildingBlockRepository.getOtherFiles().remove(of);
        }
    }

    public void deleteXSDFile(BuildingBlockRepository bbr) throws IOException {
        if (bbr != null && bbr.getXsdZipFile() != null) {
            String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
            String filepath = repo + File.separator + bbr.getXsdZipFile().getId() + "_" + bbr.getXsdZipFile().getFileName();
            if (!(new File(filepath).delete())) {
                LOG.info("Deletion of the file {} was not performed", filepath);
            }
            bbr.setXsdZipFile(null);
            FacesMessages.instance().add(Severity.WARN, "XSD file was deleted from the database, do not forget to save the BBR configuration.");
        }
    }

    public void uploadXSDFileListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        selectedBuildingBlockRepository.setXsdZipFile(new OtherFile());
        selectedBuildingBlockRepository.getXsdZipFile().setContent(item.getData());
        selectedBuildingBlockRepository.getXsdZipFile().setFileName(item.getName());
        selectedBuildingBlockRepository.getXsdZipFile().setMimeType("application/zip");
    }

    public void validateVersion(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("validateVersion");
        }
        String version = (String) value;
        String spacePattern = "\\s";
        Pattern regex = Pattern.compile(spacePattern, Pattern.DOTALL);

        Matcher regexMatcher = regex.matcher(version);
        if (regexMatcher.find()) {
            LOG.warn("Version contains one or more space and it's not allowed");
            FacesMessage.Severity severity = FacesMessage.SEVERITY_WARN;
            FacesMessage message = new FacesMessage(severity, "Version contains one or more space and it's not allowed", null);
            FacesMessages.instance().add(message);
            throw new ValidatorException(message);
        }
    }



}
