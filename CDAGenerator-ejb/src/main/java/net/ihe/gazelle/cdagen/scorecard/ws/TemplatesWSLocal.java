package net.ihe.gazelle.cdagen.scorecard.ws;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * <b>Class Description : </b>TemplatesWSLocal<br>
 * <br>
 *
 * @author Abderrazek Boufahja / IHE-Europe development Project
 * @class TemplatesWSLocal
 * @package net.ihe.gazelle.cda.scorecard.ws
 */

@Local
@Path("/templates")
@Api("/templates")
public interface TemplatesWSLocal {

    @GET
    @Path("/hello")
    @Produces("text/plain")
    @ApiOperation(value = "hello operation", responseClass = "javax.ws.rs.core.Response")
    Response hello(@ApiParam(value = "Hello to templates MM")  @Context HttpServletRequest req);

    @GET
    @Path("/mindmap")
    @Produces("application/xml")
    @ApiOperation(value = "compute the the mindmap for a specific TemplatesContainment", responseClass = "java.lang.String")
    String computeTemplatesAsMM(@ApiParam(value = "templateId value") @QueryParam("templateId") String templateId, 
    		@ApiParam(value = "TemplatesContainment ID from CDAGenerator") @QueryParam("tcName") String tcName, 
    		@ApiParam(value = "targeted TemplatesContainment : orginial or flattened") @QueryParam("target") String target);

}
