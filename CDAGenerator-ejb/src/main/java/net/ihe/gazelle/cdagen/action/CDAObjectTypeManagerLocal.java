package net.ihe.gazelle.cdagen.action;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remove;

import org.jboss.seam.annotations.Destroy;

import net.ihe.gazelle.cdagen.datamodel.CDAObjectTypeDataModel;
import net.ihe.gazelle.cdagen.model.CDAObjectType;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;

@Local
public interface CDAObjectTypeManagerLocal {

	@Destroy
	@Remove
	public abstract void destroy();

	public abstract List<IntegrationProfile> getPossibleIntegrationProfilesForCreator();

	public abstract List<Domain> getPossibleDomains();

	public abstract void setSelectedCDAObjectType(CDAObjectType selectedCDAObjectType);

	public abstract CDAObjectType getSelectedCDAObjectType();

	public abstract void setFoundCDAObjectTypes(CDAObjectTypeDataModel foundCDAObjectTypes);

	public abstract CDAObjectTypeDataModel getFoundCDAObjectTypes();

	public abstract String editCDAObjectType(CDAObjectType cdaobj);

	public abstract void addNewCreatorOfcurrentObjectType();

	public abstract void resetSelectionValuesForCreator(int i);

	public abstract List<IntegrationProfileOption> getPossibleOptionsForCreator();

	public abstract List<Actor> getPossibleActorsForCreator();

	public abstract void setSelectedIntegrationProfileOptionForCreator(
			IntegrationProfileOption selectedIntegrationProfileOptionForCreator);

	public abstract IntegrationProfileOption getSelectedIntegrationProfileOptionForCreator();

	public abstract void setSelectedActorForCreator(Actor selectedActorForCreator);

	public abstract Actor getSelectedActorForCreator();

	public abstract void setSelectedIntegrationProfileForCreator(
			IntegrationProfile selectedIntegrationProfileForCreator);

	public abstract IntegrationProfile getSelectedIntegrationProfileForCreator();

	public abstract void setSelectedDomainForCreators(Domain selectedDomainForCreators);

	public abstract Domain getSelectedDomainForCreators();

	public ActorIntegrationProfileOption getSelectedAIPO();

	public void setSelectedAIPO(ActorIntegrationProfileOption selectedAIPO);

	public void deleteSelectedAIPOFromCreator();

}