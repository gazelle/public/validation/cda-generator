package net.ihe.gazelle.cdagen.bbr.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;

/**
 * Created by xfs on 02/02/17.
 */

@Entity
@Name("otherFiles")
@Table(name = "other_files")
@SequenceGenerator(name = "other_files_sequence", sequenceName = "other_files_id_seq", allocationSize = 1)
public class OtherFile implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger log = LoggerFactory.getLogger(OtherFile.class);
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "other_files_sequence")
    private Integer id;
	
	@Transient
    private byte[] content;
	
    private String fileName;
    private String mimeType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    
//    @PrePersist
//    @PreUpdate
//    public void prePersist() {
//    	if (this.content != null) {
//    		String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
//    		String filepath = repo  + File.separator + this.getId() + "_" + this.fileName;
//    		try {
//				FileUtils.writeByteArrayToFile(new File(filepath), this.content);
//			} catch (IOException e) {
//				log.info("Problem to write on file :" + filepath, e);
//			}
//    	}
//    }
    
    @PostLoad
    public void postLoad() {
    	if (this.content == null) {
    		String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
    		String filepath = repo  + File.separator + this.getId() + "_" + this.fileName;
    		if (new File(filepath).exists()) {
	    		try {
					this.content = FileUtils.readFileToByteArray(new File(filepath));
				} catch (IOException e) {
					log.info("Problem to read the file :" + filepath, e);
				}
    		}
    	}
    }
    
    public String getFilePath() {
    	String repo = ApplicationConfiguration.getValueOfVariable("other_files_path");
    	return repo  + File.separator + this.getId() + "_" + this.fileName;
    }
    
}
