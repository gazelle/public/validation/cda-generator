package net.ihe.gazelle.cdagen.hl7templates.validator.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.util.Base64;

import net.ihe.gazelle.cdagen.hl7templates.validation.HL7TemplValidation;
import net.ihe.gazelle.cdagen.hl7templates.validation.HL7TemplValidationQuery;
import net.ihe.gazelle.cdagen.hl7templates.validation.HL7TemplateValManager;
import net.ihe.gazelle.cdagen.hl7templates.validation.ProcessValidationInterface;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.utils.FileReadWrite;

@Stateless
@Name("GOCTemplatesValidatorRS")
public class GOCTemplatesValidatorRS implements GOCTemplatesValidatorAPI {
	
	@In(create=true,required = false, value = "processValidation")
	ProcessValidationInterface validationProcessor;
	
	@Override
	public String validateHL7TemplatesURL(String decorXMLURL, String username, String artDecorInstanceIdentifier) {
		if (decorXMLURL == null || decorXMLURL.trim().equals("")) {
			return "[ERROR] decorXMLURL is not provided";
		}
		if (username == null || username.trim().equals("")) {
			return "[ERROR] username is not provided";
		}
		if (artDecorInstanceIdentifier == null || artDecorInstanceIdentifier.trim().equals("")) {
			return "[ERROR] artDecorInstanceIdentifier is not provided";
		}
		HL7TemplateValManager val = new HL7TemplateValManager();
		val.setValidationProcessor(this.validationProcessor);
		val.initializeDownloadFile();
		val.setDownloadFile(HL7TemplateValManager.WRITE);
		val.setSampleContentURL(decorXMLURL);
		try {
			val.validateHL7Templates();
			return val.getCurrentHL7TemplValidation().getToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String validateHL7Templates(String decorXMLContentB64, String username, String artDecorInstanceIdentifier) {
		if (decorXMLContentB64 == null || decorXMLContentB64.trim().equals("")) {
			return "[ERROR] decorXMLContentB64 is not provided";
		}
		if (username == null || username.trim().equals("")) {
			return "[ERROR] username is not provided";
		}
		if (artDecorInstanceIdentifier == null || artDecorInstanceIdentifier.trim().equals("")) {
			return "[ERROR] artDecorInstanceIdentifier is not provided";
		}
		HL7TemplateValManager val = new HL7TemplateValManager();
		val.setValidationProcessor(this.validationProcessor);
		val.initializeDownloadFile();
		val.setDownloadFile(HL7TemplateValManager.UPLOAD);
		File fileTMP = fileUploadListener(decorXMLContentB64, val);
		val.setUploadedFile(fileTMP);
		try {
			val.validateHL7Templates();
			return val.getCurrentHL7TemplValidation().getToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private File fileUploadListener(String decorXMLContentB64, HL7TemplateValManager val) {
		try {
			String decorXMLContent = new String(Base64.decode(decorXMLContentB64));
			val.setUploadedFile(File.createTempFile("sample", extractDecorName(decorXMLContent)));
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(val.getUploadedFile());
				fos.write(decorXMLContent.getBytes());
			}finally {
				fos.close();
			}
			return val.getUploadedFile();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String extractDecorName(String decorXMLContent) {
		Pattern pat = Pattern.compile("<project.*?prefix=\"(.*?)\"");
		Matcher mat = pat.matcher(decorXMLContent);
		if (mat.find()) {
			return mat.group(1);
		}
		return null;
	}

	@Override
	public String validateHL7Template(String decorXMLContentB64, String username, String templateIdentifierId,
			String templateIdentifierName,
			String templateIdentifierVersionLabel, String artDecorInstanceIdentifier) {
		// TODO Auto-generated method stub
		return "not implemented yet";
	}

	@Override
	public ValidationStatus validationStatus(String validationToken) {
		if (validationToken == null || validationToken.trim().equals("")) {
			return ValidationStatus.UNKNOWN;
		}
		HL7TemplValidationQuery quer = new HL7TemplValidationQuery();
		quer.token().eq(validationToken);
		HL7TemplValidation tempVal = quer.getUniqueResult();
		if (tempVal != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue("output-hl7temp-validation");
			File fileOutputLevel = new File(pathOutput + File.separator + tempVal.getId() + "/logs.txt");
			try {
				String logFileContent = FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
				String progress = HL7TemplateValManager.extractProgressPercentage(logFileContent);
				if (progress != null && (Integer.valueOf(progress)<100)) {
					return ValidationStatus.IN_PROGRESS;
				}
				else if (progress != null && (Integer.valueOf(progress)==100)) {
					return ValidationStatus.COMPLETED;
				}
			}
			catch(Exception e) {
				e.printStackTrace();
				return ValidationStatus.ABORTED;
			}
		}
		return ValidationStatus.UNKNOWN;
	}

	@Override
	public String validationResult(String validationToken) {
		if (validationToken == null || validationToken.trim().equals("")) {
			return "[ERROR] validationToken is not provided";
		}
		HL7TemplValidationQuery quer = new HL7TemplValidationQuery();
		quer.token().eq(validationToken);
		HL7TemplValidation tempVal = quer.getUniqueResult();
		if (tempVal != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue("output-hl7temp-validation");
			File fileOutputLevel = new File(pathOutput + File.separator + tempVal.getId() + "/res.xml");
			try {
				String logFileContent = FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
				return logFileContent;
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String validationLogs(String validationToken) {
		if (validationToken == null || validationToken.trim().equals("")) {
			return "[ERROR] validationToken is not provided";
		}
		HL7TemplValidationQuery quer = new HL7TemplValidationQuery();
		quer.token().eq(validationToken);
		HL7TemplValidation tempVal = quer.getUniqueResult();
		if (tempVal != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue("output-hl7temp-validation");
			File fileOutputLevel = new File(pathOutput + File.separator + tempVal.getId() + "/logs.txt");
			try {
				String logFileContent = FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
				return logFileContent;
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String validationProgress(String validationToken) {
		if (validationToken == null || validationToken.trim().equals("")) {
			return "0";
		}
		HL7TemplValidationQuery quer = new HL7TemplValidationQuery();
		quer.token().eq(validationToken);
		HL7TemplValidation tempVal = quer.getUniqueResult();
		if (tempVal != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue("output-hl7temp-validation");
			File fileOutputLevel = new File(pathOutput + File.separator + tempVal.getId() + "/logs.txt");
			try {
				String logFileContent = FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
				String progress = HL7TemplateValManager.extractProgressPercentage(logFileContent);
				if (progress != null) {
					return progress;
				}
			}
			catch(Exception e) {
				e.printStackTrace();
				return "0";
			}
		}
		return "0";
	}

}
