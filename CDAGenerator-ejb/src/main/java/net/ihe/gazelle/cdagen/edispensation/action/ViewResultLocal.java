package net.ihe.gazelle.cdagen.edispensation.action;

import javax.ejb.Local;

@Local
public interface ViewResultLocal {

    public abstract String getHtmlResultValidation();

    public abstract void setHtmlResultValidation(String htmlResultValidation);
    
    public String getePrescriptionHtml1();
    public void setePrescriptionHtml1(String ePrescriptionHtml1);
    public String getePrescriptionHtml2();
    public void setePrescriptionHtml2(String ePrescriptionHtml2);
    public String geteDispensationHtml1();
    public void seteDispensationHtml1(String eDispensationHtml1);
    
    public void destroy();
    
}