package net.ihe.gazelle.cdagen.validator.ws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import net.ihe.gazelle.cdagen.bbr.action.BBRUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lu.esante.agence.luxl1.LUXL1PackValidator;
import net.ihe.gazelle.adapters.DoubleAdapter;
import net.ihe.gazelle.anapath.apsr.APSRPackValidator;
import net.ihe.gazelle.asip.CDARCPValidator;
import net.ihe.gazelle.asip.MODELCONTENUValidator;
import net.ihe.gazelle.asiparb.CdarcpParser;
import net.ihe.gazelle.asiparb.ModelcontenuParser;
import net.ihe.gazelle.ccd.ccd.CCDPackValidator;
import net.ihe.gazelle.ccd.ccd.CCDTemplatesPackValidator;
import net.ihe.gazelle.ccda.ccda.CCDAPackValidator;
import net.ihe.gazelle.ccdaarb.CCDAParser;
import net.ihe.gazelle.ccdarb.CcdParser;
import net.ihe.gazelle.cda.CDAValidator;
import net.ihe.gazelle.cda.cdabasic.CDABASICPackValidator;
import net.ihe.gazelle.cda.cdadt.CDADTPackValidator;
import net.ihe.gazelle.cda.cdanblock.CDANBLOCKPackValidator;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.cdagen.simulator.common.utils.XMLValidation;
import net.ihe.gazelle.cdakh.cdakh.CDAKHPackValidator;
import net.ihe.gazelle.cdamin.cdamin.CDAMINPackValidator;
import net.ihe.gazelle.cdaminarb.CdaminParser;
import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.crc.crc.CRCPackValidator;
import net.ihe.gazelle.crcarb.CrcParser;
import net.ihe.gazelle.datatypes.datatypes.DATATYPESPackValidator;
import net.ihe.gazelle.epsos.common.COMMONPackValidator;
import net.ihe.gazelle.epsos.commonpivot.COMMONPIVOTPackValidator;
import net.ihe.gazelle.epsos.edispensation.EDISPENSATIONPackValidator;
import net.ihe.gazelle.epsos.edispensationpivot.EDISPENSATIONPIVOTPackValidator;
import net.ihe.gazelle.epsos.eedcommon.EEDCOMMONPackValidator;
import net.ihe.gazelle.epsos.eprescription.EPRESCRIPTIONPackValidator;
import net.ihe.gazelle.epsos.eprescriptionpivot.EPRESCRIPTIONPIVOTPackValidator;
import net.ihe.gazelle.epsos.epsostemplates.EPSOSTEMPLATESPackValidator;
import net.ihe.gazelle.epsos.hcer.HCERPackValidator;
import net.ihe.gazelle.epsos.mro.MROPackValidator;
import net.ihe.gazelle.epsos.patientsummary.PATIENTSUMMARYPackValidator;
import net.ihe.gazelle.epsos.patientsummary.PATIENTSUMMARYTemplatesPackValidator;
import net.ihe.gazelle.epsos.patientsummarypivot.PATIENTSUMMARYPIVOTPackValidator;
import net.ihe.gazelle.epsos.patientsummarypivot.PATIENTSUMMARYPIVOTTemplatesPackValidator;
import net.ihe.gazelle.epsos.scanneddoc.SCANNEDDOCPackValidator;
import net.ihe.gazelle.epsosad.epsosad.EPSOSADPackValidator;
import net.ihe.gazelle.epsosadarb.EpsosadParser;
import net.ihe.gazelle.epsosarb.CommonParser;
import net.ihe.gazelle.epsosarb.EDispensationParser;
import net.ihe.gazelle.epsosarb.EPrescriptionParser;
import net.ihe.gazelle.epsosarb.EpSOSTemplatesParser;
import net.ihe.gazelle.epsosarb.HcerParser;
import net.ihe.gazelle.epsosarb.MroParser;
import net.ihe.gazelle.epsosarb.PatientSummaryParser;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.gen.common.TemplateParser;
import net.ihe.gazelle.kbppc.kbppc.KBPPCPackValidator;
import net.ihe.gazelle.klabc.klabccom.KLABCCOMPackValidator;
import net.ihe.gazelle.klabc.klabcrep.KLABCREPPackValidator;
import net.ihe.gazelle.klabc.klaborder.KLABORDERPackValidator;
import net.ihe.gazelle.klabcarb.KlabccomParser;
import net.ihe.gazelle.klabcarb.KlabcrepParser;
import net.ihe.gazelle.klabcarb.KlaborderParser;
import net.ihe.gazelle.kradr.bsrr.BSRRPackValidator;
import net.ihe.gazelle.kradr.drr.DRRPackValidator;
import net.ihe.gazelle.kradr.kradrcommon.KRADRCOMMONPackValidator;
import net.ihe.gazelle.lux.luxh.LUXHPackValidator;
import net.ihe.gazelle.luxarb.LuxhParser;
import net.ihe.gazelle.pcc.pccdocument.PCCDOCUMENTPackValidator;
import net.ihe.gazelle.pcc.pccdocument.PCCDOCUMENTTemplatesPackValidator;
import net.ihe.gazelle.pcc.pccentry.PCCENTRYPackValidator;
import net.ihe.gazelle.pcc.pccentry.PCCENTRYTemplatesPackValidator;
import net.ihe.gazelle.pcc.pccsection.PCCSECTIONPackValidator;
import net.ihe.gazelle.pcc.pccsection.PCCSECTIONTemplatesPackValidator;
import net.ihe.gazelle.pccarb.PCCdocumentParser;
import net.ihe.gazelle.pccarb.PCCentryParser;
import net.ihe.gazelle.pccarb.PCCsectionParser;
import net.ihe.gazelle.rcsc.rcsc.RCSCPackValidator;
import net.ihe.gazelle.rcscarb.RcscParser;
import net.ihe.gazelle.rcsep.rcsep.RCSEPPackValidator;
import net.ihe.gazelle.rcseparb.RcsepParser;
import net.ihe.gazelle.rcsepvs.rcsepvs.RCSEPVSPackValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xdlab.xdlabpatch.XDLABPATCHPackValidator;
import net.ihe.gazelle.xdlab.xdlabspec.XDLABSPECPackValidator;
import net.ihe.gazelle.xdlab.xdlabspec.XDLABSPECTemplatesPackValidator;
import net.ihe.gazelle.xdlabarb.XDLABSpecParser;
import net.ihe.gazelle.xdssd.xdssd.XDSSDPackValidator;
import net.ihe.gazelle.xdssd.xdssd.XDSSDTemplatesPackValidator;
import net.ihe.gazelle.xdssd.xdssdbppc.XDSSDBPPCPackValidator;
import net.ihe.gazelle.xdssd.xdssdcode.XDSSDCODEPackValidator;
import net.ihe.gazelle.xdssd.xdssdrad.XDSSDRADPackValidator;
import net.ihe.gazelle.xdssdarb.XdssdParser;
import net.ihe.gazelle.xdssdarb.XdssdbppcParser;
import net.ihe.gazelle.xdssdarb.XdssdcodeParser;
import net.ihe.gazelle.xdssdarb.XdssdradParser;

/**
 * @author abderrazek boufahja
 */
public final class MicroDocumentValidation {

    private static final String ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT = "Errors when trying to parse the document";
    private static final String FAILED = "FAILED";
    private static final String PASSED = "PASSED";
    private static final String CLINICAL_DOCUMENT = "/ClinicalDocument";
    private static Logger log = LoggerFactory.getLogger(MicroDocumentValidation.class);

    enum CDATYPE {
        BASIC,
        EPSOS,
        LAB,
        PHARM
    }

    static {
        log.info("Initiating the CDA validator of the application");
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return ApplicationConfiguration.getValueOfVariable("svs_endpoint");
            }
        });
    }

    private MicroDocumentValidation() {
    }

    private static void validateMBWithBasicCDAValidator(String req,
                                                        List<ConstraintValidatorModule> listConstraintValidatorModule, List<Notification> ln)
            throws InternalValidationExeption, ValidatorException, UnsupportedEncodingException {
        net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clin = loadCDA(new ByteArrayInputStream(req.getBytes("UTF8")));
        if (clin == null) {
            throw new InternalValidationExeption();
        }
        for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
            net.ihe.gazelle.cda.POCDMT000040ClinicalDocument.validateByModule(clin, CLINICAL_DOCUMENT,
                    constraintValidatorModule, ln);
        }
    }

    public static DetailedResult validateWithBasicCDAValidator(String req, Validators val,
                                                               List<ConstraintValidatorModule> listConstraintValidatorModule, List<TemplateParser>
                                                                       listParser) {
        List<Notification> ln = new ArrayList<>();
        return validateWithBasicCDAValidator(req, val, listConstraintValidatorModule, listParser, ln);
    }

    private static DetailedResult validateWithBasicCDAValidator(String req, Validators val,
                                                                List<ConstraintValidatorModule> listConstraintValidatorModule, List<TemplateParser>
                                                                        listParser, List<Notification> ln) {
        DetailedResult res = new DetailedResult();
        net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clin = null;
        try {
            if (req != null) {
                clin = loadCDA(new ByteArrayInputStream(req.getBytes("UTF8")));
            }
            if (clin == null) {
                throw new InternalValidationExeption();
            }
            for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
                net.ihe.gazelle.cda.POCDMT000040ClinicalDocument.validateByModule(clin, CLINICAL_DOCUMENT,
                        constraintValidatorModule, ln);
            }
            Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser,
                    CLINICAL_DOCUMENT, ln);
            res.setTemplateDesc(temp);
        } catch (ValidatorException e) {
            errorWhenExtracting(e, ln);
        } catch (Exception e) {
            errorWhenExtracting(ln);
        }
        validateToSchema(res, req, CDATYPE.BASIC);
        return updateValidationResult(req, val, res, ln);
    }

    public static DetailedResult validateWithCDALABValidator(String req, Validators val,
                                                             List<ConstraintValidatorModule> listConstraintValidatorModule, List<TemplateParser>
                                                                     listParser,
                                                             List<Notification> ln) {
        DetailedResult res = new DetailedResult();
        net.ihe.gazelle.cdalab.POCDMT000040ClinicalDocument clin = null;
        try {
            if (req != null) {
                clin = loadCDALab(new ByteArrayInputStream(req.getBytes("UTF8")));
            }
            if (clin == null) {
                throw new InternalValidationExeption();
            }
            for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
                net.ihe.gazelle.cdalab.POCDMT000040ClinicalDocument.validateByModule(clin, CLINICAL_DOCUMENT,
                        constraintValidatorModule, ln);
            }
            Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser,
                    CLINICAL_DOCUMENT, ln);
            res.setTemplateDesc(temp);
        } catch (ValidatorException e) {
            errorWhenExtracting(e, ln);
        } catch (Exception e) {
            errorWhenExtracting(ln);
        }
        validateToSchema(res, req, CDATYPE.LAB);
        return updateValidationResult(req, val, res, ln);
    }

    public static DetailedResult validateWithCDAPHARMValidator(String req, Validators val,
                                                               List<ConstraintValidatorModule> listConstraintValidatorModule, List<TemplateParser>
                                                                       listParser) {
        DetailedResult res = new DetailedResult();
        net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clin = null;
        List<Notification> ln = new ArrayList<>();
        try {
            clin = loadCDA(new ByteArrayInputStream(req.getBytes("UTF8")));
            if (clin == null) {
                throw new InternalValidationExeption();
            }
            for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
                net.ihe.gazelle.cda.POCDMT000040ClinicalDocument.validateByModule(clin, CLINICAL_DOCUMENT,
                        constraintValidatorModule, ln);
            }
            Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser,
                    CLINICAL_DOCUMENT, ln);
            res.setTemplateDesc(temp);
        } catch (ValidatorException e) {
            errorWhenExtracting(e, ln);
        } catch (Exception e) {
            errorWhenExtracting(ln);
        }
        validateToSchema(res, req, CDATYPE.PHARM);
        return updateValidationResult(req, val, res, ln);
    }

    public static DetailedResult validateWithCDAEPSOSValidator(String req, Validators val,
                                                               List<ConstraintValidatorModule> listConstraintValidatorModule, List<TemplateParser>
                                                                       listParser,
                                                               List<Notification> lnold) {
        DetailedResult res = new DetailedResult();
        net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument clin = null;
        List<Notification> ln = new ArrayList<>();
        try {
            clin = loadCDAEpsos(new ByteArrayInputStream(req.getBytes("UTF8")));
            if (clin == null) {
                throw new InternalValidationExeption();
            }
            for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
                net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument.validateByModule(clin, CLINICAL_DOCUMENT,
                        constraintValidatorModule, ln);
            }
            if (lnold != null) {
                ln.addAll(lnold);
            }
            Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser,
                    CLINICAL_DOCUMENT, ln);
            res.setTemplateDesc(temp);
        } catch (ValidatorException e) {
            errorWhenExtracting(e, ln);
        } catch (Exception e) {
            errorWhenExtracting(ln);
        }
        validateToSchema(res, req, CDATYPE.EPSOS);

        return updateValidationResult(req, val, res, ln);
    }

    private static DetailedResult updateValidationResult(String req, Validators val, DetailedResult res,
                                                         List<Notification> ln) {
        if (res == null || ln == null) {
            return null;
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        Notification not = validateCDAIDs(req);
        ln.add(not);
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
        updateMDAValidation(res.getMDAValidation(), ln);
        summarizeDetailedResult(res, val);

        return res;
    }

    private static Notification validateCDAIDs(String req) {
        NodeList vals = evaluate(new ByteArrayInputStream(req.getBytes()), " //cda:reference/@value",
                new DatatypesNamespaceContext());
        NodeList ids = evaluate(new ByteArrayInputStream(req.getBytes()), " //@ID", new DatatypesNamespaceContext());
        StringBuilder sb = new StringBuilder();
        List<String> listId = new ArrayList<>();
        if (ids != null) {
            for (int i = 0; i < ids.getLength(); i++) {
                listId.add(ids.item(i).getNodeValue());
            }
        }

        if (vals != null) {
            for (int i = 0; i < vals.getLength(); i++) {
                String val = vals.item(i).getNodeValue();
                if (val.length() > 1 && (val.charAt(0) == '#') && !listId.contains(val.substring(1))) {
                    if (!sb.toString().equals("")) {
                        sb.append(",");
                    }
                    sb.append(val.substring(1));
                }
            }
        }
        return createNotificationForIDsChecks(sb);
    }

    private static Notification createNotificationForIDsChecks(StringBuilder sb) {
        String badValues = sb.toString();
        Notification not = null;
        if (badValues.equals("")) {
            not = new Note();
            not.setTest("test_IDs");
            not.setLocation(CLINICAL_DOCUMENT);
            not.setDescription("Success: Found all IDs referenced");
        } else {
            not = new Error();
            not.setTest("test_IDs");
            not.setLocation(CLINICAL_DOCUMENT);
            not.setDescription("Error: Cannot find all the IDs referenced. Missing IDs are : " + badValues);
        }
        return not;
    }

    public static NodeList evaluate(InputStream stream, String expression, NamespaceContext namespace) {
        NodeList string = null;
        try {
            InputSource source = new InputSource(stream);

            XPathFactory fabrique = XPathFactory.newInstance();
            XPath xpath = fabrique.newXPath();
            if (namespace != null) {
                xpath.setNamespaceContext(namespace);
            }

            XPathExpression exp = xpath.compile(expression);
            string = (NodeList) exp.evaluate(source, XPathConstants.NODESET);

        } catch (Exception xpee) {
            log.info("Error when trying to validate the references of IDs" + xpee.getClass().getSimpleName(), xpee);
        }
        return string;
    }

    public static DetailedResult validateIHEXDLAB(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new XDLABSPECPackValidator());
        listConstraintValidatorModule.add(new XDLABPATCHPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XDLABSpecParser());
        List<Notification> ln = new ArrayList<>();
        return MicroDocumentValidation.validateWithCDALABValidator(req, Validators.IHE_XDLAB,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateIHEBASICCDA(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.IHE_BASIC_CDA,
                listConstraintValidatorModule, null);
    }

    public static DetailedResult validateIHEBASICCDASTRICTED(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDABASICPackValidator());
        listConstraintValidatorModule.add(new CDADTPackValidator());
        listConstraintValidatorModule.add(new CDANBLOCKPackValidator());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.IHE_BASIC_CDA_STRICT,
                listConstraintValidatorModule, null);
    }

    public static DetailedResult validateEPSOSPSPIVOT(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());
        listConstraintValidatorModule.add(new PATIENTSUMMARYPIVOTPackValidator());
        listConstraintValidatorModule.add(new PATIENTSUMMARYPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new COMMONPIVOTPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PatientSummaryParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_PS_PIVOT", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_PS_PIVOT,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSPSFRIENDLY(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new PATIENTSUMMARYPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PatientSummaryParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_PS_FRIENDLY", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_PS_FRIENDLY,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSHCER(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new PATIENTSUMMARYTemplatesPackValidator());
        listConstraintValidatorModule.add(new PATIENTSUMMARYPIVOTTemplatesPackValidator());
        listConstraintValidatorModule.add(new EEDCOMMONPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());
        listConstraintValidatorModule.add(new HCERPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PatientSummaryParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());
        listParser.add(new HcerParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_HCER", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_HCER,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSMRO(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new PATIENTSUMMARYTemplatesPackValidator());
        listConstraintValidatorModule.add(new PATIENTSUMMARYPIVOTTemplatesPackValidator());
        listConstraintValidatorModule.add(new EEDCOMMONPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());
        listConstraintValidatorModule.add(new MROPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PatientSummaryParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());
        listParser.add(new MroParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_MRO", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_MRO,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSSCAN(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new SCANNEDDOCPackValidator());
        listConstraintValidatorModule.add(new XDSSDPackValidator());
        listConstraintValidatorModule.add(new XDSSDCODEPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdParser());
        listParser.add(new XdssdcodeParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.EPSOS_SCAN,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateIHEBPPC(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new XDSSDBPPCPackValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTPackValidator());
        listConstraintValidatorModule.add(new XDSSDTemplatesPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdParser());
        listParser.add(new XdssdcodeParser());
        listParser.add(new XdssdbppcParser());
        listParser.add(new PCCdocumentParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.IHE_BPPC,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateIHEXDSSDXDSIB(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new XDSSDTemplatesPackValidator());
        listConstraintValidatorModule.add(new XDSSDRADPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdParser());
        listParser.add(new XdssdradParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.IHE_XDS_SD_XDSIB,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateEPSOSEPPIVOT(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new EPRESCRIPTIONPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new EPRESCRIPTIONPIVOTPackValidator());
        listConstraintValidatorModule.add(new COMMONPIVOTPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new EPrescriptionParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_EP_PIVOT", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_EP_PIVOT,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSEPFRIENDLY(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new EPRESCRIPTIONPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new EPrescriptionParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_EP_FRIENDLY", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_EP_FRIENDLY,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSEDPIVOT(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new EDISPENSATIONPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new EDISPENSATIONPIVOTPackValidator());
        listConstraintValidatorModule.add(new COMMONPIVOTPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new EDispensationParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_ED_PIVOT", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_ED_PIVOT,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateEPSOSEDFRIENDLY(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new EDISPENSATIONPackValidator());
        listConstraintValidatorModule.add(new COMMONPackValidator());
        listConstraintValidatorModule.add(new EPSOSTEMPLATESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new EDispensationParser());
        listParser.add(new CommonParser());
        listParser.add(new EpSOSTemplatesParser());
        listParser.add(new CcdParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCCD.add(new CDAValidator());
        listConstraintValidatorModuleCCD.add(new DATATYPESPackValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EPSOS_ED_FRIENDLY", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EPSOS_ED_FRIENDLY,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateCCDBASIC(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CCDPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new CcdParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.CCD_BASIC,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateASIPCDAMIN(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAMINPackValidator());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.ASIP_CDA_MIN,
                listConstraintValidatorModule, null);
    }

    public static DetailedResult validateASIPFRCP(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDARCPValidator());
        listConstraintValidatorModule.add(new MODELCONTENUValidator());
        listConstraintValidatorModule.add(new CDAMINPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTPackValidator());
        listConstraintValidatorModule.add(new PCCSECTIONPackValidator());
        listConstraintValidatorModule.add(new PCCENTRYPackValidator());
        listConstraintValidatorModule.add(new CCDTemplatesPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new CdarcpParser());
        listParser.add(new CdaminParser());
        listParser.add(new ModelcontenuParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());
        listParser.add(new CcdParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.ASIP_FRCP,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateIHECRC(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CCDAPackValidator());
        listConstraintValidatorModule.add(new CRCPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new CCDAParser());
        listParser.add(new CrcParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.IHE_CRC,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateLUXHEADER(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new LUXHPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new LuxhParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.LUX_HEADER,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateLUXBODYL1(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new LUXL1PackValidator());
        listConstraintValidatorModule.add(new LUXHPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new LuxhParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.LUX_BODYL1,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateKSAHEADER(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAKHPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.KSA_HEADER,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateKSABPPC(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        listConstraintValidatorModule.add(new KBPPCPackValidator());
        listConstraintValidatorModule.add(new CDAKHPackValidator());

        listConstraintValidatorModule.add(new XDSSDBPPCPackValidator());
        listConstraintValidatorModule.add(new XDSSDTemplatesPackValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTTemplatesPackValidator());

        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdbppcParser());
        listParser.add(new XdssdParser());
        listParser.add(new PCCdocumentParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.KSA_BPPC,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateKSADRR(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        listConstraintValidatorModule.add(new DRRPackValidator());
        listConstraintValidatorModule.add(new KRADRCOMMONPackValidator());
        listConstraintValidatorModule.add(new CDAKHPackValidator());

        listConstraintValidatorModule.add(new XDSSDPackValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTTemplatesPackValidator());

        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdParser());
        listParser.add(new PCCdocumentParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.KSA_DRR,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateKSABSRR(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        listConstraintValidatorModule.add(new BSRRPackValidator());
        listConstraintValidatorModule.add(new KRADRCOMMONPackValidator());
        listConstraintValidatorModule.add(new CDAKHPackValidator());

        listConstraintValidatorModule.add(new XDSSDTemplatesPackValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTTemplatesPackValidator());

        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CDAValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new XdssdParser());
        listParser.add(new PCCdocumentParser());
        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.KSA_BSRR,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateKSALABR(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCDABASIC = new ArrayList<>();
        listConstraintValidatorModuleCDABASIC.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new CDAKHPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new CDAValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCDABASIC, ln);
        } catch (Exception e) {
            log.info("Problem to process KSA_LABR", e);
            ln = new ArrayList<>();
        }

        listConstraintValidatorModule.add(new KLABCREPPackValidator());
        listConstraintValidatorModule.add(new KLABCCOMPackValidator());

        listConstraintValidatorModule.add(new XDLABSPECTemplatesPackValidator());

        listConstraintValidatorModule.add(new DATATYPESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PCCdocumentParser());
        listParser.add(new XDLABSpecParser());
        listParser.add(new KlabccomParser());
        listParser.add(new KlabcrepParser());
        return MicroDocumentValidation.validateWithCDALABValidator(req, Validators.KSA_LABR,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateKSALABO(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCDABASIC = new ArrayList<>();
        listConstraintValidatorModuleCDABASIC.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new CDAKHPackValidator());
        listConstraintValidatorModuleCDABASIC.add(new CDAValidator());
        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCDABASIC, ln);
        } catch (Exception e) {
            log.info("Problem to process KSA_LABO", e);
            ln = new ArrayList<>();
        }

        listConstraintValidatorModule.add(new KLABORDERPackValidator());
        listConstraintValidatorModule.add(new KLABCCOMPackValidator());
        listConstraintValidatorModule.add(new XDLABSPECTemplatesPackValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new PCCdocumentParser());
        listParser.add(new XDLABSpecParser());
        listParser.add(new KlabccomParser());
        listParser.add(new KlaborderParser());

        return MicroDocumentValidation.validateWithCDALABValidator(req, Validators.KSA_LABO,
                listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateACCRCSC(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new RCSCPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new RcscParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.ACC_RCS_C,
                listConstraintValidatorModule, listParser);
    }

    public static DetailedResult validateACCRCSEP(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDABASICPackValidator());
        listConstraintValidatorModule.add(new CDADTPackValidator());
        listConstraintValidatorModule.add(new CDANBLOCKPackValidator());
        listConstraintValidatorModule.add(new RCSEPPackValidator());
        listConstraintValidatorModule.add(new RCSEPVSPackValidator());

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new RcsepParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.ACC_RCS_EP,
                listConstraintValidatorModule, listParser);
    }


    public static DetailedResult validateEXPANDCDA(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();

        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new EpsosadParser());

        List<Notification> ln = new ArrayList<>();
        List<ConstraintValidatorModule> listConstraintValidatorModuleCCD = new ArrayList<>();
        listConstraintValidatorModuleCCD.add(new EPSOSADPackValidator());
        listConstraintValidatorModuleCCD.add(new CDABASICPackValidator());
        listConstraintValidatorModuleCCD.add(new CDADTPackValidator());
        listConstraintValidatorModuleCCD.add(new CDANBLOCKPackValidator());

        try {
            MicroDocumentValidation.validateMBWithBasicCDAValidator(req, listConstraintValidatorModuleCCD, ln);
        } catch (Exception e) {
            log.info("Problem to process EXPAND_CDA", e);
            ln = new ArrayList<>();
        }
        return MicroDocumentValidation.validateWithCDAEPSOSValidator(req, Validators.EXPAND_CDA, listConstraintValidatorModule, listParser, ln);
    }

    public static DetailedResult validateANAPATHAPSR(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
        listConstraintValidatorModule.add(new CDAValidator());
        listConstraintValidatorModule.add(new DATATYPESPackValidator());
        listConstraintValidatorModule.add(new CCDTemplatesPackValidator());
        listConstraintValidatorModule.add(new PCCDOCUMENTTemplatesPackValidator());
        listConstraintValidatorModule.add(new PCCSECTIONTemplatesPackValidator());
        listConstraintValidatorModule.add(new PCCENTRYTemplatesPackValidator());
        listConstraintValidatorModule.add(new APSRPackValidator());
        List<TemplateParser> listParser = new ArrayList<>();
        listParser.add(new CCDAParser());
        listParser.add(new PCCdocumentParser());
        listParser.add(new PCCsectionParser());
        listParser.add(new PCCentryParser());

        return MicroDocumentValidation.validateWithBasicCDAValidator(req, Validators.ANAPATH_APSR,
                listConstraintValidatorModule, listParser);
    }

    private static void updateMDAValidation(MDAValidation mda, List<Notification> ln) {
        mda.setResult(PASSED);
        for (Notification notification : ln) {
            if (notification instanceof net.ihe.gazelle.validation.Error) {
                mda.setResult(FAILED);
            }
        }
    }

    static void errorWhenExtracting(ValidatorException vexp, List<Notification> ln) {
        if (ln == null) {
            return;
        }
        if (vexp != null && vexp.getDiagnostic() != null) {
            for (Notification notification : vexp.getDiagnostic()) {
                ln.add(notification);
            }
        }
    }

    static void errorWhenExtracting(List<Notification> ln) {
        if (ln == null) {
            return;
        }
        Error err = new Error();
        err.setTest("structure");
        err.setLocation("All the document");
        err.setDescription("The tool is not able to find urn:hl7-org:v3:ClinicalDocument element as the root of the validated document.");
        ln.add(err);
    }

    static void summarizeDetailedResult(DetailedResult dr, Validators val) {
        String validator = val.getValue();
        summarizeBbrDetailedResult(dr, validator);

    }

    static void summarizeBbrDetailedResult(DetailedResult dr, String val) {
        if (dr != null) {
            Date dd = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
            DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
            dr.setValidationResultsOverview(new ValidationResultsOverview());
            dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
            dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
            dr.getValidationResultsOverview().setValidationEngine("Gazelle CDA Validation");
            dr.getValidationResultsOverview().setValidationServiceName(val);
            dr.getValidationResultsOverview().setValidationTestResult(PASSED);
            if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null)
                    && (dr.getDocumentValidXSD().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() != null)
                    && (dr.getDocumentWellFormed().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null)
                    && (dr.getMDAValidation().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            dr.getValidationResultsOverview().setValidationEngineVersion(POMVersion.getVersion());
            dr.getValidationResultsOverview().setValidationServiceVersion(BBRUtil.getBBRByLabel(val));
        }
    }

    private static void validateToSchema(DetailedResult res, String sub, CDATYPE type) {
        DocumentWellFormed dd = XMLValidation.isXMLWellFormed(sub);
        res.setDocumentWellFormed(dd);
        if (res.getDocumentWellFormed().getResult().equals(PASSED)) {
            try {
                switch (type) {
                    case BASIC:
                    case LAB:
                        res.setDocumentValidXSD(XMLValidation.isXSDValid(sub));
                        break;
                    case EPSOS:
                        res.setDocumentValidXSD(XMLValidation.isXSDValidEpsos(sub));
                        break;
                    case PHARM:
                        res.setDocumentValidXSD(XMLValidation.isXSDValidPharm(sub));
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                log.info("error when validating with the schema", e);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PrintStream ps = new PrintStream(baos);
                e.printStackTrace(ps);
                DocumentValidXSD docv = new DocumentValidXSD();
                docv.setResult(FAILED);
                docv.getXSDMessage().add(new XSDMessage());
                docv.getXSDMessage().get(0).setSeverity("error");
                docv.getXSDMessage()
                        .get(0)
                        .setMessage(
                                "error when validating with the schema. The error is : "
                                        + StringUtils.substring(baos.toString(), 0, 300));
                res.setDocumentValidXSD(docv);
            }
        }
    }

    private static net.ihe.gazelle.cda.POCDMT000040ClinicalDocument loadCDA(InputStream is) throws ValidatorException {
        final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT);
        try {
            JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cda");
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(new ValidationEventHandler() {

                @Override
                public boolean handleEvent(ValidationEvent event) {
                    return MicroDocumentValidation.handleEvent(event, vexp);
                }
            });
            u.setAdapter(new DoubleAdapter());
            return (net.ihe.gazelle.cda.POCDMT000040ClinicalDocument) u.unmarshal(is);
        } catch (JAXBException e) {
            throw vexp;
        }
    }

    private static net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument loadCDAEpsos(InputStream is)
            throws ValidatorException {
        final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT);
        try {
            JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cdaepsos");
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(new ValidationEventHandler() {

                @Override
                public boolean handleEvent(ValidationEvent event) {
                    return MicroDocumentValidation.handleEvent(event, vexp);
                }
            });
            return (net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument) u.unmarshal(is);
        } catch (JAXBException e) {
            throw vexp;
        }
    }

    private static net.ihe.gazelle.cdalab.POCDMT000040ClinicalDocument loadCDALab(InputStream is)
            throws ValidatorException {
        final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT);
        try {
            JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cdalab");
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(new ValidationEventHandler() {

                @Override
                public boolean handleEvent(ValidationEvent event) {
                    return MicroDocumentValidation.handleEvent(event, vexp);
                }
            });
            return (net.ihe.gazelle.cdalab.POCDMT000040ClinicalDocument) u.unmarshal(is);
        } catch (JAXBException e) {
            throw vexp;
        }
    }

    protected static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
        if (event.getSeverity() != ValidationEvent.WARNING) {
            ValidationEventLocator vel = event.getLocator();
            Notification not = new Error();
            not.setDescription("Line:Col[" + vel.getLineNumber() + ":" + vel.getColumnNumber() + "]:"
                    + event.getMessage());
            not.setTest("message_parsing");
            not.setLocation("All the document");
            vexp.getDiagnostic().add(not);
        }
        return true;
    }

}
