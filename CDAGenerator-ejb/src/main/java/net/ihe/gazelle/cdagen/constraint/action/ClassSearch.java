package net.ihe.gazelle.cdagen.constraint.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.constraint.model.Classe;
import net.ihe.gazelle.cdagen.constraint.model.Constraints;
import net.ihe.gazelle.cdagen.constraint.model.Packag;
import net.ihe.gazelle.cdagen.constraint.model.Property;

public class ClassSearch {
	
	private static Logger log = LoggerFactory.getLogger(ClassSearch.class);
	
	static Constraints getConstraintsFromFileString(String xmlString) throws JAXBException{
		JAXBContext jj = JAXBContext.newInstance("net.ihe.gazelle.cda.constraint.model");
		Unmarshaller um = jj.createUnmarshaller();
		return(Constraints) um.unmarshal(new ByteArrayInputStream(xmlString.getBytes()));
	}
	
	static String marshullToString(Constraints cc) throws JAXBException{
		JAXBContext jj = JAXBContext.newInstance("net.ihe.gazelle.cda.constraint.model");
		Marshaller m = jj.createMarshaller();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		m.marshal(cc, baos);
		return baos.toString();
	}
	
	public static void searchRepeated(Map<String,String> lname, String contraintsPath) throws IOException, JAXBException {
		Map<String,String> lname2 =  copyMap(lname);
		String xx = FileReadWrite.readDoc(contraintsPath);
		Constraints cc = getConstraintsFromFileString(xx);
		boolean added = false;
		for (Packag pack : cc.getPackage()) {
			if (pack.getClasse() != null){
				for (Classe cl : pack.getClasse()) {
					for(Entry<String, String> entr : lname2.entrySet()) {
						if (doesPropertiesContainsType(entr.getKey(), entr.getValue(), cl) && lname.get(cl.getName()) == null){
							lname.put(cl.getName(), pack.getName());
							added = true;
						}
					}
				}
			}
		}
		if (added){
			searchRepeated(lname, contraintsPath);
		}
	}
	
	static Map<String, String> copyMap(Map<String, String> map){
		Map<String,String> lname2 =  new HashMap<>();
		for (Entry<String, String> sss : map.entrySet()) {
			lname2.put(sss.getKey(), sss.getValue());
		}
		return lname2;
	}
	
	public static void main(String[] args) {
		String conspath = "/Users/aboufahj/Documents/workspace/2/cda-models/xsd/constraints.xml";
		Map<String, String> hh = new HashMap<>();
		hh = Collections.synchronizedMap(hh);
		hh.put("POCDMT000040DocumentationOf", "cda");
		hh.put("POCDMT000040Observation", "cda");
		try {
			searchRepeated(hh, conspath);
		} catch (IOException | JAXBException e) {
			log.error("Problem to rearch for members", e);
		}
	}
	
	static Packag getPackageFromName(String name, Constraints cc){
		if (cc==null) {
			return null;
		}
		for (Packag pack : cc.getPackage()) {
			if (pack.getName().equals(name)){
				return pack;
			}
		}
		return null;
	}
	
	static Classe getClasseFromPack(String name, Packag pack){
		if (pack == null) return null;
		for (Classe cl : pack.getClasse()) {
			if (cl.getName().equals(name)){
				return cl;
			}
		}
		return null;
	}
	
	static Classe getClassFromPackAndName(String namePack, String name, Constraints cc){
		Packag pack = getPackageFromName(namePack, cc);
		if (pack == null) return null;
		return getClasseFromPack(name, pack);
	}
	
	static boolean doesPropertiesContainsType(String className, String packName, Classe cl){
		if (cl.getProperties()==null) return false;
		for (Property pr : cl.getProperties()) {
			if (pr.getType().equals(packName + "-" + className)){
				return true;
			}
		}
		return false;
	}

}
