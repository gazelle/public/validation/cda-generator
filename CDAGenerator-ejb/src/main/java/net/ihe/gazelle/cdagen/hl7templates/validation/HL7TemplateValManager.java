/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cdagen.hl7templates.validation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.xerces.util.EncodingMap;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.handler.model.CheckResult;
import net.ihe.gazelle.handler.model.ProblemType;
import net.ihe.gazelle.handler.model.ValidationResult;
import net.ihe.gazelle.utils.FileReadWrite;

/**
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 */
@Name("HL7TemplateValManager")
@Scope(ScopeType.PAGE)
public class HL7TemplateValManager implements Serializable {

	private static final String OUTPUT_HL7TEMP_VALIDATION = "output-hl7temp-validation";

	/**
	 * 
	 */
	private static final long serialVersionUID = -8734856733751607120L;

	public static final String UPLOAD = "upload";

	public static final String WRITE = "write";

	// ~ Statics variables and Class initializer /////////////////////////////////////////

	private static Logger log = LoggerFactory.getLogger(HL7TemplateValManager.class);

	// ~ Attribute ///////////////////////////////////////////////////////////////////////

	private File uploadedFile;

	private String downloadFile = UPLOAD;

	private transient String uploadedFilename;

	private String sampleContentURL;

	private HL7TemplValidation currentHL7TemplValidation;

	private boolean reportAllChecks;

	@In(create=true,required = false, value = "processValidation")
	ProcessValidationInterface validationProcessor;
	
	private String currentProgressValue = "0";
	
	private Boolean showLogs = false;

	public ProcessValidationInterface getValidationProcessor() {
		return validationProcessor;
	}

	public void setValidationProcessor(ProcessValidationInterface validationProcessor) {
		this.validationProcessor = validationProcessor;
	}

	public Boolean getShowLogs() {
		return showLogs;
	}

	public void setShowLogs(Boolean showLogs) {
		this.showLogs = showLogs;
	}

	public String getCurrentProgressValue() {
		return currentProgressValue;
	}

	public void setCurrentProgressValue(String currentProgressValue) {
		this.currentProgressValue = currentProgressValue;
	}

	public HL7TemplValidation getCurrentHL7TemplValidation() {
		return currentHL7TemplValidation;
	}

	public void setCurrentHL7TemplValidation(HL7TemplValidation currentHL7TemplValidation) {
		this.currentHL7TemplValidation = currentHL7TemplValidation;
	}

	public File getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(File uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public boolean isReportAllChecks() {
		return reportAllChecks;
	}

	public void setReportAllChecks(boolean reportAllChecks) {
		this.reportAllChecks = reportAllChecks;
	}

	public void listener(FileUploadEvent event) throws IOException {
		UploadedFile item = event.getUploadedFile();
		uploadedFilename = item.getName();
		uploadedFile = File.createTempFile("sample", uploadedFilename);
		try (FileOutputStream fos = new FileOutputStream(uploadedFile)) {
			fos.write(item.getData());
		}
		catch(Exception e) {
			log.info("Problem to save uploaded file", e);
		}
	}

	public void initializeDownloadFile() {
		this.setDownloadFile("upload");
		this.setSampleContentURL(null);
		this.currentHL7TemplValidation = new HL7TemplValidation();
	}

	public String getSampleContentURL() {
		return sampleContentURL;
	}

	public void setSampleContentURL(String sampleContentURL) {
		this.sampleContentURL = sampleContentURL;
	}

	public String getDownloadFile() {
		return downloadFile;
	}

	public void setDownloadFile(String downloadFile) {
		this.downloadFile = downloadFile;
	}

	public boolean canUploadFile() {
		return UPLOAD.equals(this.downloadFile);
	}

	public void validateHL7Templates() throws IOException, InterruptedException, JAXBException {
		log.info("validation beginning");
		String path = null;
		if (this.downloadFile != null) {
			if (this.downloadFile.equals(UPLOAD)) {
				path = this.uploadedFile.getAbsolutePath();
			}
			else {
				path = this.sampleContentURL;
			}
		}
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		currentHL7TemplValidation = new HL7TemplValidation();
		this.currentProgressValue = "5";
		this.showLogs = false;
		if (UPLOAD.equals(this.downloadFile)) {
			currentHL7TemplValidation.setName(this.uploadedFilename);
		}
		else {
			currentHL7TemplValidation.setUrl(path);
			Pattern pat = Pattern.compile("prefix=([^&]*)-&");
			Matcher mat = pat.matcher(path);
			if (mat.find()) {
				currentHL7TemplValidation.setName(mat.group(1));
			}
		}
		String validationExecPath = ApplicationPreferenceManager.getStringValue("hl7temp-validation-exec");
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		em.persist(currentHL7TemplValidation);
		validationProcessor.processJarValidation(path, pathOutput, validationExecPath, this.currentHL7TemplValidation.getId(), reportAllChecks);
		log.info("validation ended");
	}

	private void updateValidationResult(String pathOutput) throws IOException, JAXBException {
		File fileOutput = new File(pathOutput + File.separator + this.currentHL7TemplValidation.getId() );
		this.currentHL7TemplValidation.setValidationResult(FileReadWrite.readDoc(fileOutput.getAbsolutePath() + "/res.xml"));
		this.currentHL7TemplValidation.setValidationResultChecks(extractChecks());
		// TODO update ended
		log.info("validateHL7Templates ended");
	}

	

	public String extractValidatedLevel(String level) {
		if (this.currentHL7TemplValidation != null && this.currentHL7TemplValidation.getId() != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
			File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + File.separator + level + ".xml");
			if (fileOutputLevel.exists() && fileOutputLevel.isFile()) {
				try {
					return FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
				}
				catch(Exception e) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					PrintStream s = new PrintStream(baos);
					e.printStackTrace(s);
					return baos.toString();
				}
			}
		}
		return null;
	}

	public List<CheckResult> extractChecks() throws JAXBException {
		return extractChecks(this.currentHL7TemplValidation);
	}

	public List<CheckResult> extractChecks(HL7TemplValidation currentHL7TemplValidation) throws JAXBException {
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + "/res.xml");
		JAXBContext jc = JAXBContext.newInstance(ValidationResult.class);
		Unmarshaller m = jc.createUnmarshaller();
		EncodingMap.putIANA2JavaMapping( "UTF8", "UTF-8" );
		ValidationResult vr = (ValidationResult) m.unmarshal(fileOutputLevel);
		if (vr != null) {
			List<CheckResult> rr = new ArrayList<>();
			for (CheckResult checkResult : vr.getCheckResults()) {
				rr.add(checkResult);
			}
			return rr;
		}
		return null;
	}

	public int extractNumberCheck(String type) throws JAXBException {
		return extractNumberCheck(type, currentHL7TemplValidation);
	}

	public int extractNumberCheck(String type, HL7TemplValidation currentHL7TemplValidation) {
		if (currentHL7TemplValidation != null && currentHL7TemplValidation.getValidationResultChecks() != null) {
			int res = 0;
			for (CheckResult checkResult : currentHL7TemplValidation.getValidationResultChecks()) {
				if (checkResult.getProblemType().toString().equalsIgnoreCase(type)) {
					res++;
				}
			}
			return res;
		}
		return 0;
	}
	
	public int extractNumberCDACheck(String type) {
		if (currentHL7TemplValidation != null && currentHL7TemplValidation.getValidationResultChecks() != null) {
			int res = 0;
			if (type.equals("all")) {
				for (CheckResult checkResult : currentHL7TemplValidation.getValidationResultChecks()) {
					if (checkResult.getTest().contains("CDATEMP")) {
						res++;
					}
				}
			}
			else {
				for (CheckResult checkResult : currentHL7TemplValidation.getValidationResultChecks()) {
					if (checkResult.getProblemType().toString().equalsIgnoreCase(type) && checkResult.getTest().contains("CDATEMP")) {
						res++;
					}
				}
			}
			return res;
		}
		return 0;
	}

	public List<CheckResult> extractFailedCheck() {
		if (this.currentHL7TemplValidation != null && this.currentHL7TemplValidation.getValidationResultChecks() != null) {
			Set<CheckResult> res = new TreeSet<>();
			for (CheckResult checkResult : this.currentHL7TemplValidation.getValidationResultChecks()) {
				if (checkResult.getProblemType() != ProblemType.REPORT) {
					res.add(checkResult);
				}
			}
			return new ArrayList<>(res);
		}
		return new ArrayList<>();
	}

	public String extractCSSClass(CheckResult checkResult) {
		if (checkResult != null) {
			if (checkResult.getProblemType() == ProblemType.ERROR) {
				return "errortemp";
			}
			else if (checkResult.getProblemType() == ProblemType.FATALERROR) {
				return "fataltemp";
			}
			else if (checkResult.getProblemType() == ProblemType.WARNING) {
				return "warningtemp";
			}
			else if (checkResult.getProblemType() == ProblemType.INFO) {
				return "note";
			}
			else if (checkResult.getProblemType() == ProblemType.REPORT) {
				return "report";
			}
		}
		return "unknown";
	}

	public void downloadLevel(String level) throws FileNotFoundException {
		String fileName = null;
		if (this.currentHL7TemplValidation != null){
			String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
			fileName = pathOutput + File.separator + currentHL7TemplValidation.getId() + File.separator + level + ".xml";
			File fil = new File(fileName);
			if (fil.exists()) {
				DocumentDownloadTool.showFile(new FileInputStream(fil), fileName, true);
			}
		}
	}
	public void initCurrentHL7TemplValidation() throws IOException, JAXBException {
		if (this.currentHL7TemplValidation != null) {
			this.initCurrentHL7TemplValidation(currentHL7TemplValidation);
		}
	}

	private void initCurrentHL7TemplValidation(HL7TemplValidation currentHL7TemplValidation) throws IOException, JAXBException {
		this.currentHL7TemplValidation = currentHL7TemplValidation;
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		File fileOutput = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() );
		try {
			this.currentHL7TemplValidation.setValidationResult(FileReadWrite.readDoc(fileOutput.getAbsolutePath() + "/res.xml"));
			this.currentHL7TemplValidation.setValidationResultChecks(extractChecks());
		}
		catch(Exception e) {
			log.info("The result is not found in the server");
		}
	}

	public void initRes() {
		FacesContext fc = FacesContext.getCurrentInstance();
		String id = fc.getExternalContext().getRequestParameterMap().get("id");
		EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
		try{
			Integer idd = Integer.valueOf(id);
			this.currentHL7TemplValidation = entityManager.find(HL7TemplValidation.class, idd);
		}
		catch(Exception e){
			FacesMessages.instance().add("You have to use a good id.");
			this.currentHL7TemplValidation = null;
			return;
		}
	}
	
	public String logsOfValidation() {
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + "/logs.txt");
		String content = "";
		try {
			content = FileReadWrite.readDoc(fileOutputLevel.getAbsolutePath());
			this.currentProgressValue = extractProgressPercentage(content);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return content;
	}

	public static String extractProgressPercentage(String content) {
		String res = "0";
		if (content != null) {
			if (content.contains("LEVEL FV")) {
				res = "100";
			}
			else if (content.contains("LEVEL 5")) {
				res = "85";
			}
			else if (content.contains("LEVEL 4")) {
				res = "70";
			}
			else if (content.contains("LEVEL 3")) {
				res = "50";
			}
			else if (content.contains("LEVEL 2")) {
				res = "30";
			}
			else if (content.contains("LEVEL 1")) {
				res = "20";
			}
			else if (content.contains("LEVEL 0")) {
				res = "10";
			}
		}
		return res;
	}
	
	public void updateEndedFlag() throws IOException, JAXBException {
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + "/res.xml");
		if (fileOutputLevel.exists()) {
			this.currentHL7TemplValidation.setEnded(true);
			EntityManager em=(EntityManager)Component.getInstance("entityManager");
			currentHL7TemplValidation = em.merge(currentHL7TemplValidation);
			this.updateValidationResult(pathOutput);
		}
	}
	
	public boolean canViewResult() {
		if (this.currentHL7TemplValidation == null || this.currentHL7TemplValidation.getId() == null) {
			return false;
		}
		String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
		File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + "/res.xml");
		return fileOutputLevel.exists();
	}
	
	public void showLogs() {
		this.showLogs = true;
	}
	
	public void hideLogs() {
		this.showLogs = false;
	}
	
	public boolean canShowLogs() {
		return this.showLogs;
	}
	
	public boolean thereAreLogs() {
		if (this.currentHL7TemplValidation != null && this.currentHL7TemplValidation.getId() != null) {
			String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
			File fileOutputLevel = new File(pathOutput + File.separator + currentHL7TemplValidation.getId() + "/logs.txt");
			return fileOutputLevel.exists();
		}
		return false;
	}
	
	public void downloadResultValidation() throws FileNotFoundException {
		String fileName = null;
		if (this.currentHL7TemplValidation != null){
			String pathOutput = ApplicationPreferenceManager.getStringValue(OUTPUT_HL7TEMP_VALIDATION);
			fileName = pathOutput + File.separator + currentHL7TemplValidation.getId() + "/res.xml";
			File fil = new File(fileName);
			if (fil.exists()) {
				DocumentDownloadTool.showFile(new FileInputStream(fil), fileName, true);
			}
		}
	}

}
