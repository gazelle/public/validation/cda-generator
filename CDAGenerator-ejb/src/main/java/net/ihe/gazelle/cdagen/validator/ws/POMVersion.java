package net.ihe.gazelle.cdagen.validator.ws;

import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @author abderrazek boufahja
 * extract the pom version of CDAGenerator
 *
 */
public final class POMVersion {
	
	private POMVersion() {}
	
	private static String version = null;
	
	public static synchronized String getVersion() {
	    if (version != null) {
	        return version;
	    }

	    // try to load from maven properties first
	    try {
	        Properties p = new Properties();
	        InputStream is = POMVersion.class.getResourceAsStream("/META-INF/maven/net.ihe.gazelle/CDAGenerator-ejb/pom.properties");
	        if (is != null) {
	            p.load(is);
	            version = p.getProperty("version", "");
	        }
	    } catch (Exception e) {
	        // ignore
	    }

	    // fallback to using Java API
	    if (version == null) {
	        Package aPackage = POMVersion.class.getPackage();
	        if (aPackage != null) {
	            version = aPackage.getImplementationVersion();
	            if (version == null) {
	                version = aPackage.getSpecificationVersion();
	            }
	        }
	    }

	    if (version == null) {
	        // we could not compute the version so use a blank
	        version = "";
	    }

	    return version;
	}

}
