package net.ihe.gazelle.cdagen.validator.ws;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class XpathUtils {
	
	private XpathUtils() {
		// private constructor
	}
	
	public static String evaluateByString(String string, String nodeName, String expression) throws Exception{
		Node clin = getNodeFromString(string, nodeName);
		return evaluateByNode(clin, expression);
	}
	
	public static String evaluateByNode(Node node, String expression) throws XPathExpressionException {
		String b = null;
		XPathFactory fabrique = XPathFactory.newInstance();
		XPath xpath = fabrique.newXPath();
		xpath.setNamespaceContext(new DatatypesNamespaceContext());
		b = (String)xpath.evaluate(expression, node, XPathConstants.STRING);
		return b;
	}
	
	private static Node getNodeFromString(String string, String nodeName) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(string.getBytes()));
		NodeList dd = doc.getElementsByTagName(nodeName);
		return dd.item(0);
	}

}
