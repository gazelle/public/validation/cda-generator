package net.ihe.gazelle.cdagen.bbr.action;

import java.util.Map;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepositoryQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * Created by xfs on 28/11/16.
 */
public class BuildingBlockRepositoryFilter extends Filter<BuildingBlockRepository> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuildingBlockRepositoryFilter(Map<String, String> requestParameterMap) {
		super(getHQLCriterions(), requestParameterMap);
	}

	private static HQLCriterionsForFilter<BuildingBlockRepository> getHQLCriterions() {

		BuildingBlockRepositoryQuery query = new BuildingBlockRepositoryQuery(
				EntityManagerService.provideEntityManager());
		HQLCriterionsForFilter<BuildingBlockRepository> result = query.getHQLCriterionsForFilter();

		result.addPath("label", query.label());
		result.addPath("active", query.active());
		result.addPath("url", query.url());
		result.addPath("version", query.version());
		result.addPath("status", query.status());
		result.addPath("creationDate", query.creationDate());
		result.addPath("buildingDate", query.buildingDate());
		return result;
	}

	@Override
	public EntityManager getEntityManager() {
		return EntityManagerService.provideEntityManager();
	}

}
