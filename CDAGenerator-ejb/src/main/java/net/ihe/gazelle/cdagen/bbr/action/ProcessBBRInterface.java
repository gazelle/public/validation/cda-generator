package net.ihe.gazelle.cdagen.bbr.action;

import java.io.IOException;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;

/**
 * Created by xfs on 30/11/16.
 */
@Local
public interface ProcessBBRInterface {

	@Asynchronous
	public void processJarValidation(String rootPathString, BuildingBlockRepository bbr)
			throws IOException, InterruptedException;
}
