package net.ihe.gazelle.cdagen.eprescription.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Element;
import org.jdom.Namespace;

import net.ihe.gazelle.cdagen.eprescription.model.PrescAuthor;
import net.ihe.gazelle.cdagen.tools.JdomHelper;

public class PrescGenerator {

    private static final String VALUE = "value";
	private static final String DISPLAY_NAME = "displayName";
	private static final String CODE_SYSTEM_NAME = "codeSystemName";
	private static final String CODE_SYSTEM = "codeSystem";
	private static final String EXTENSION2 = "extension";
	static final String NAMESPACE = "urn:hl7-org:v3";
    
    public static void main(String[] args) {
        String res = null;
        res = PrescGenerator.generatePrescription();
        System.out.println(res);
    }

    public static String generatePrescription() {
        
        PrescAuthor pa = new PrescAuthor("aassigningAuthorityName", "adisplayable", "aextension", "aroot", "afamily", "agiven");
        
        String extension = "test";
        String root = "roottest";
        String assigningAuthorityName = "IHE";
        String extensionId = "CDAGEN";
        String rootId = "1.3.6.1.4.1.12559.11.1.2.2.8.1";
        String currentDate = getCurrentDate();
        Element ee = PrescGenerator.createClinicalDocument(extension, root, assigningAuthorityName,
                extensionId, rootId, currentDate, assigningAuthorityName, 
                extensionId, rootId, "H","state", "city", "country", "zip", 
                "Street", "LN", "ee", "M", "", "", "", "fcode", "fcodeSystem", "fcodeSystemName", "fdisplayName", 
                pa, "S",
                "custroot");
        return JdomHelper.element2String(ee);
    }
    
    public static String getCurrentDate(){
        String dateformat = "yyyyMMdd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
        return sdf.format(new Date());
    }
    
    public static Element createClinicalDocument(String extension, String root,
            String assigningAuthorityName, String extensionId, String rootId, 
            String currentDate, 
            String recassigningAuthorityName, String recextension,
            String recroot, String recuse, String state, String city, String country, 
            String postalCode, String streetAddressLine, String family, String given,
            String reccode, String recCodeSystem, String recCodeSystemName, 
            String birthTime, String fcodeSystem, String fcodeSystemName, String fdisplayName, String time,
           PrescAuthor pa, String fcode,
            String custroot){
        Element el = new Element("ClinicalDocument", NAMESPACE);
        el.addNamespaceDeclaration(Namespace.getNamespace("sch","http://www.ascc.net/xml/schematron"));
        el.addNamespaceDeclaration(Namespace.getNamespace(NAMESPACE));
        el.addNamespaceDeclaration(Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
        el.addContent(PrescGenerator.typeId(extension, root));
        el.addContent(PrescGenerator.templateId("1.3.6.1.4.1.12559.11.10.1.3.1.1.1"));
        el.addContent(PrescGenerator.id(assigningAuthorityName, "true", extensionId, rootId));
        el.addContent(PrescGenerator.code("57833-6", "2.16.840.1.113883.6.1", "LOINC", "ePrescription"));
        el.addContent(PrescGenerator.title("ePrescription"));
        el.addContent(PrescGenerator.effectiveTime(currentDate));
        el.addContent(PrescGenerator.confidentialityCode("N", "2.16.840.1.113883.5.25", "Confidentiality", "Normale"));
        el.addContent(PrescGenerator.languageCode("fr-FR"));
        el.addContent(PrescGenerator.setId(assigningAuthorityName, "true", extensionId, rootId));
        el.addContent(PrescGenerator.versionNumber("1"));
        el.addContent(PrescGenerator.recordTarget(recassigningAuthorityName, "true", recextension, recroot,
                recuse, state, city, country, postalCode, streetAddressLine, 
                family, given, reccode, recCodeSystem, recCodeSystemName, "true", birthTime));
        el.addContent(PrescGenerator.author(fcode, fcodeSystem, fcodeSystemName, fdisplayName, time,
                pa));
        el.addContent(PrescGenerator.custodian());
        el.addContent(PrescGenerator.legalAuthenticator(time, pa));
        return el;
    }

    public static Element typeId(String extension, String root){
        Element el = new Element("typeId", NAMESPACE);
        el.setAttribute(EXTENSION2, extension);
        el.setAttribute("root", root);
        return el;
    }

    public static Element id(String assigningAuthorityName, String displayable, String extension, String root){
        Element el = new Element("id", NAMESPACE);
        el.setAttribute("assigningAuthorityName", assigningAuthorityName);
        el.setAttribute("displayable", displayable);
        el.setAttribute(EXTENSION2, extension);
        el.setAttribute("root", root);
        return el;
    }
    
    public static Element templateId(String root){
        Element el = new Element("templateId", NAMESPACE);
        el.setAttribute("root", root);
        return el;
    }

    public static Element code(String code, String codeSystem, String codeSystemName, String displayName){
        Element el = new Element("code", NAMESPACE);
        el.setAttribute("code", code);
        el.setAttribute(CODE_SYSTEM, codeSystem);
        el.setAttribute(CODE_SYSTEM_NAME, codeSystemName);
        el.setAttribute(DISPLAY_NAME, displayName);
        return el;
    }

    public static Element title(String title){
        Element el = new Element("title", NAMESPACE);
        el.setText(title);
        return el;
    }

    public static Element effectiveTime(String date){
        Element el = new Element("effectiveTime", NAMESPACE);
        el.setText(date);
        return el;
    }

    public static Element confidentialityCode(String code, String codeSystem, String codeSystemName, String displayName){
        Element el = new Element("confidentialityCode", NAMESPACE);
        el.setAttribute(CODE_SYSTEM, codeSystem);
        el.setAttribute(CODE_SYSTEM_NAME, codeSystemName);
        el.setAttribute(DISPLAY_NAME, displayName);
        return el;
    }
    
    public static Element languageCode(String code){
        Element el = new Element("languageCode", NAMESPACE);
        el.setAttribute("code", code);
        return el;
    }
    
    public static Element setId(String assigningAuthorityName, String displayable, String extension, String root){
        Element el = new Element("setId", NAMESPACE);
        el.setAttribute("assigningAuthorityName", assigningAuthorityName);
        el.setAttribute("displayable", displayable);
        el.setAttribute(EXTENSION2, extension);
        el.setAttribute("root", root);
        return el;
    }
    
    public static Element versionNumber(String number){
        Element el = new Element("versionNumber", NAMESPACE);
        el.setText(number);
        return el;
    }
    
    public static Element recordTarget(String assigningAuthorityName, String displayable, String extension, String root,
            String use, String state, String city, String country, String postalCode, String streetAddressLine,
            String family, String given, 
            String code, String codeSystem, String codeSystemName, String displayName,
            String birthTime){
        Element el = new Element("recordTarget", NAMESPACE);
        Element patientRole = PrescGenerator.patientRole(assigningAuthorityName, displayable, extension, root, use, state, city, country, postalCode, streetAddressLine, family, given, code, codeSystem, codeSystemName, displayName, birthTime);
        el.addContent(patientRole);
        return el;
    }
    
    public static Element patientRole(String assigningAuthorityName, String displayable, String extension, String root,
            String use, String state, String city, String country, String postalCode, String streetAddressLine,
            String family, String given, 
            String code, String codeSystem, String codeSystemName, String displayName,
            String birthTime){
        Element el = new Element("patientRole", NAMESPACE);
        el.addContent(PrescGenerator.id(assigningAuthorityName, displayable, extension, root));
        if (state != null || city != null || country != null || postalCode != null || streetAddressLine != null){
            el.addContent(PrescGenerator.addr(use, state, city, country, postalCode, streetAddressLine));
        }
        if (family != null || given != null || code != null){
            el.addContent(PrescGenerator.patient(family, given, code, codeSystem, codeSystemName, displayName, birthTime));
        }
        return el;
    }
    
    public static Element addr(String use, String state, String city, String country, String postalCode, String streetAddressLine){
        Element el = new Element("addr", NAMESPACE);
        if (use != null) el.setAttribute("use", use);
        if (state != null) el.addContent(PrescGenerator.state(state));
        if (city != null) el.addContent(PrescGenerator.city(city));
        if (country != null) el.addContent(PrescGenerator.country(country));
        if (postalCode != null) el.addContent(PrescGenerator.postalCode(postalCode));
        if (streetAddressLine != null) el.addContent(PrescGenerator.streetAddressLine(streetAddressLine));
        return el;
    }
    
    public static Element state(String state){
        Element el = new Element("state", NAMESPACE);
        el.setText(state);
        return el;
    }
    
    public static Element city(String city){
        Element el = new Element("city", NAMESPACE);
        el.setText(city);
        return el;
    }
    
    public static Element country(String country){
        Element el = new Element("country", NAMESPACE);
        el.setText(country);
        return el;
    }
    
    public static Element postalCode(String postalCode){
        Element el = new Element("postalCode", NAMESPACE);
        el.setText(postalCode);
        return el;
    }
    
    public static Element streetAddressLine(String streetAddressLine){
        Element el = new Element("streetAddressLine", NAMESPACE);
        el.setText(streetAddressLine);
        return el;
    }
    
    public static Element telecom(){
        Element el = new Element("telecom", NAMESPACE);
        el.setAttribute("nullFlavor", "NI");
        return el;
    }
    
    public static Element patient(String family, String given, 
            String code, String codeSystem, String codeSystemName, String displayName,
            String birthTime){
        Element el = new Element("telecom", NAMESPACE);
        if (family != null && given != null) el.addContent(PrescGenerator.name(family, given));
        if (code != null) el.addContent(PrescGenerator.administrativeGenderCode(code, codeSystem, codeSystemName, displayName));
        if (birthTime != null) el.addContent(PrescGenerator.birthTime(birthTime));
        return el;
    }
    
    public static Element name(String family, String given){
        Element el = new Element("name", NAMESPACE);
        if (family != null) el.addContent(PrescGenerator.family(family));
        if (given != null) el.addContent(PrescGenerator.given(given));
        return el;
    }
    
    public static Element family(String family){
        Element el = new Element("family", NAMESPACE);
        el.setText(family);
        return el;
    }
    
    public static Element given(String given){
        Element el = new Element("given", NAMESPACE);
        el.setText(given);
        return el;
    }
    
    public static Element administrativeGenderCode(String code, String codeSystem, String codeSystemName, String displayName){
        Element el = new Element("administrativeGenderCode", NAMESPACE);
        el.setAttribute("code", code);
        el.setAttribute(CODE_SYSTEM, codeSystem);
        el.setAttribute(CODE_SYSTEM_NAME, codeSystemName);
        el.setAttribute(DISPLAY_NAME, displayName);
        return el;
    }
    
    public static Element birthTime(String birthTime){
        Element el = new Element("birthTime", NAMESPACE);
        el.setAttribute(VALUE, birthTime);
        return el;
    }
    
    public static Element author(String fcode, String fcodeSystem, String fcodeSystemName, String fdisplayName,
            String time, 
            PrescAuthor pa){
        Element el = new Element("author", NAMESPACE);
        el.addContent(PrescGenerator.functionCode(fcode, fcodeSystem, fcodeSystemName, fdisplayName));
        el.addContent(PrescGenerator.time(time));
        el.addContent(PrescGenerator.assignedAuthor(pa));
        return el;
    }
    
    public static Element functionCode(String fcode, String fcodeSystem, String fcodeSystemName, String fdisplayName){
        Element el = new Element("functionCode", NAMESPACE);
        el.setAttribute("code", fcode);
        el.setAttribute(CODE_SYSTEM, fcodeSystem);
        el.setAttribute(CODE_SYSTEM_NAME, fcodeSystemName);
        el.setAttribute(DISPLAY_NAME, fdisplayName);
        return el;
    }
    
    public static Element time(String value){
        Element el = new Element("time", NAMESPACE);
        el.setAttribute(VALUE, value);
        return el;
    }
    
    public static Element assignedAuthor(PrescAuthor pa){
        if (pa != null){
            return assignedAuthor(pa.aassigningAuthorityName, pa.adisplayable, pa.aextension, pa.aroot, pa.afamily, pa.agiven);
        }
        return null;
    }
    
    public static Element assignedAuthor(String aassigningAuthorityName,String adisplayable, String aextension, 
            String aroot, String afamily, String agiven){
        Element el = new Element("assignedAuthor", NAMESPACE);
        el.setAttribute("classCode", "ASSIGNED");
        el.addContent(PrescGenerator.id(aassigningAuthorityName, adisplayable, aextension, aroot));
        el.addContent(PrescGenerator.telecom());
        el.addContent(PrescGenerator.assignedPerson(afamily, agiven));
        el.addContent(PrescGenerator.representedOrganization());
        return el;
    }
    
    public static Element assignedPerson(String afamily, String agiven){
        Element el = new Element("assignedPerson", NAMESPACE);
        el.addContent(PrescGenerator.name(afamily, agiven));
        return el;
    }
    
    public static Element representedOrganization(){
        Element el = new Element("representedOrganization", NAMESPACE);
        el.addContent(PrescGenerator.nullFlavorElement("id"));
        el.addContent(PrescGenerator.nullFlavorElement("name"));
        el.addContent(PrescGenerator.nullFlavorElement("telecom"));
        el.addContent(PrescGenerator.nullFlavorElement("addr"));
        return el;
    }
    
    public static Element nullFlavorElement(String elementName){
        Element el = new Element(elementName, NAMESPACE);
        el.setAttribute("nullFlavor", "NI");
        return el;
    }
    
    public static Element custodian(){
        Element el = new Element("custodian", NAMESPACE);
        Element ass = new Element("assignedCustodian", NAMESPACE);
        Element rep = new Element("representedCustodianOrganization", NAMESPACE);
        Element id = new Element("id", NAMESPACE);
        rep.addContent(id);
        rep.addContent(PrescGenerator.nullFlavorElement("name"));
        rep.addContent(PrescGenerator.nullFlavorElement("telecom"));
        rep.addContent(PrescGenerator.nullFlavorElement("addr"));
        ass.addContent(rep);
        el.addContent(ass);
        
        return el;
    }
    
    public static Element legalAuthenticator(String time, PrescAuthor pa){
        Element el = new Element("legalAuthenticator", NAMESPACE);
        el.addContent(generateEl("time", VALUE, time));
        el.addContent(generateEl("signatureCode", "code", "S"));
        el.addContent(assignedEntity(pa));
        return el;
    }
    
    private static Element generateEl(String name, String attr, String valAttr){
        Element el = new Element(name, NAMESPACE);
        el.setAttribute(attr, valAttr);
        return el;
    }
    
    public static Element assignedEntity(PrescAuthor pa){
        if (pa != null){
            return assignedEntity(pa.aassigningAuthorityName, pa.adisplayable, pa.aextension, pa.aroot, pa.afamily, pa.agiven);
        }
        return null;
    }
    
    public static Element assignedEntity(String aassigningAuthorityName,String adisplayable, String aextension, 
            String aroot, String afamily, String agiven){
        Element el = new Element("assignedEntity", NAMESPACE);
        el.setAttribute("classCode", "ASSIGNED");
        el.addContent(PrescGenerator.id(aassigningAuthorityName, adisplayable, aextension, aroot));
        el.addContent(PrescGenerator.telecom());
        el.addContent(PrescGenerator.assignedPerson(afamily, agiven));
        el.addContent(PrescGenerator.representedOrganization());
        return el;
    }
    
    


}
