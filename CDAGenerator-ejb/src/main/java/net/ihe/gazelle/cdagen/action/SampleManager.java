package net.ihe.gazelle.cdagen.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import net.ihe.gazelle.cda.POCDMT000040ClinicalDocument;
import net.ihe.gazelle.cda.editor.action.cda.CDAPOCDMT000040ClinicalDocumentEditorLocal;
import net.ihe.gazelle.cdagen.datamodel.CDADataModel;
import net.ihe.gazelle.cdagen.model.CDADoc;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;

@Name("sampleManager")
@Scope(ScopeType.PAGE)
public class SampleManager implements Serializable, UserAttributeCommon {
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	private transient CDADataModel cdas;

	private CDADoc selectedCDADocument;

	@In(value = "gumUserService")
	private UserService userService;

	public void setSelectedCDADocument(CDADoc selectedCDADocument) {
		this.selectedCDADocument = selectedCDADocument;
	}

	public CDADoc getSelectedCDADocument() {
		return selectedCDADocument;
	}

	public CDADataModel getCdas() {
		if (cdas == null) {
			cdas = new CDADataModel();
		}
		return cdas;
	}

	public void setCdas(CDADataModel cdas) {
		this.cdas = cdas;
	}

	public void initCDADoc() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		EntityManager em = (EntityManager) org.jboss.seam.Component.getInstance("entityManager");
		String pageid = params.get("id");
		if (pageid != null && pageid != "") {
			try {
				int id = Integer.parseInt(pageid);
				this.selectedCDADocument = em.find(CDADoc.class, id);
			} catch (NumberFormatException e) {
				log.info("The Id has not a good format");
				FacesMessages.instance().add("The Id has not a good format");
			}
		}
	}

	public String permanentLink(CDADoc cda) {
		if (cda != null) {
			return ApplicationConfiguration.getValueOfVariable("application_url") + "/cda.seam?id=" + cda.getId();
		}
		return null;
	}

	public String editCDADocument(CDADoc cda) {
		if (cda != null) {
			String content = cda.getContent();
			try {
				POCDMT000040ClinicalDocument xx = load(new ByteArrayInputStream(content.getBytes()));
				CDAPOCDMT000040ClinicalDocumentEditorLocal ll = (CDAPOCDMT000040ClinicalDocumentEditorLocal) Component
						.getInstance("cDAPOCDMT000040ClinicalDocumentEditorManager");
				ll.setSelectedCDAPOCDMT000040ClinicalDocument(xx);
			} catch (JAXBException e) {
				FacesMessages.instance().add("The document is not well formed. The edition is impossible to do.");
				return null;
			}
			return "/editor/cda/POCDMT000040ClinicalDocument.xhtml";
		}
		return null;
	}

	public boolean canRender(CDADoc cda) {
		return Identity.instance().getCredentials().getUsername() != null
				&& Identity.instance().getCredentials().getUsername().equals(cda.getAuthor());
	}

	public static POCDMT000040ClinicalDocument load(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cda");
		Unmarshaller u = jc.createUnmarshaller();
		return (POCDMT000040ClinicalDocument) u.unmarshal(is);
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
