package net.ihe.gazelle.cdagen.bbr.ws;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Created by xfs on 31/01/17.
 */

@Local
@Path("/bbr")
@Api("/bbr")
public interface BBRWSLocal {

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "hello operation", responseClass = "javax.ws.rs.core.Response")
    Response hello(@ApiParam(value = "Hello to scorecard")  @Context HttpServletRequest req);

    @GET
    @Path("/downloadBBRZip/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(value = "Download BBR in ZIP", responseClass = "javax.ws.rs.core.Response")
    Response downloadBBRZIP(@ApiParam(value = "Download BBR in ZIP format") @PathParam("id") String bbrId);


}
