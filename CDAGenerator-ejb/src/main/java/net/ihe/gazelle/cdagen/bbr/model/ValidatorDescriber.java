package net.ihe.gazelle.cdagen.bbr.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.constraints.xsd.Package;

/**
 * Created by xfs on 07/12/16.
 */

@Entity
@Name("validatorDescriber")
@Table(name = "validator_describer")
@SequenceGenerator(name = "validator_describer_sequence", sequenceName = "validator_describer_id_seq", allocationSize = 1)
public class ValidatorDescriber {
	
	
	private static Logger log = LoggerFactory.getLogger(ValidatorDescriber.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validator_describer_sequence")
    private int id;

    @Column(name="validator_name")
    private String validatorName;

    @ManyToMany(fetch=FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name="validator_association_parents_link",
            joinColumns=@JoinColumn(name="validator_describer_id", referencedColumnName = "id"),
            inverseJoinColumns=@JoinColumn(name="validator_parent_id",referencedColumnName = "id"))
    private List<ValidatorDescriber> validatorAssociationParents;

    @ManyToMany(fetch=FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name="validator_describer_packages_link",
            joinColumns=@JoinColumn(name="validator_describer_id", referencedColumnName = "id"),
            inverseJoinColumns=@JoinColumn(name="package_id",referencedColumnName = "id")
    )
    private List<Package> packageList;
    
    @ManyToOne
	@JoinColumn(name="tc_describer_id")
	private TCDescriber tcDescriber;

    public TCDescriber getTcDescriber() {
		return tcDescriber;
	}

	public void setTcDescriber(TCDescriber tcDescriber) {
		this.tcDescriber = tcDescriber;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValidatorName() {
        return validatorName;
    }

    public void setValidatorName(String validatorName) {
        this.validatorName = validatorName;
    }

    public List<ValidatorDescriber> getValidatorAssociationParents() {
        return validatorAssociationParents;
    }

    public void setValidatorAssociationParents(List<ValidatorDescriber> validatorAssociationParents) {
        this.validatorAssociationParents = validatorAssociationParents;
    }

    public List<Package> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<Package> packageList) {
        this.packageList = packageList;
    }

    public static boolean isBbr(String validator) {
    	try {
	        BuildingBlockRepositoryQuery buildingBlockRepositoryQuery = new BuildingBlockRepositoryQuery();
	        buildingBlockRepositoryQuery.label().eq(validator);
	        BuildingBlockRepository result = buildingBlockRepositoryQuery.getUniqueResult();
	        if(result == null){
	            return false;
	        }
    	}
    	catch (Exception e) {
    		log.info("problem to find if the validation tool is bbr", e);
    	}
        return true;
    }
}
