/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.cdagen.converter;


import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.model.Concept;


/**
 * 
 * @author abderrazek boufahja
 *
 */
@BypassInterceptors
@Name("conceptConverter")
@Converter(forClass=Concept.class)
public class ConceptConverter implements javax.faces.convert.Converter, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private static Logger log = LoggerFactory.getLogger(ConceptConverter.class);
	
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		if (value instanceof Concept)
		{
			Concept cc = (Concept) value;
			return cc.toString();
		}
		else
		{
			return null;
		}

	}


	@Transactional
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null) {
			try {
				EntityManager entityManager = (EntityManager)org.jboss.seam.Component.getInstance("entityManager",true);
				return entityManager.find(Concept.class, value) ;
			}
			catch (NumberFormatException e) {
				log.info("problem to format a number : " + value, e);
			}
		}
		return null;
	}


}
