/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.cdagen.converter;


import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.action.ConstraintDefManager;
import net.ihe.gazelle.cdagen.model.CDAType;
import net.ihe.gazelle.common.log.ExceptionLogging;
 

 
/**
 * 
 * @author abderrazek boufahja
 *
 */
@BypassInterceptors
@Name("cdaTypeConverter")
@Converter(forClass= CDAType.class)
public class CDATypeConverter implements javax.faces.convert.Converter, Serializable {
  
   /** Serial ID version of this object     */
   private static final long serialVersionUID = 12393988987567L;


	private static Logger log = LoggerFactory.getLogger(ConstraintDefManager.class);
 

   /**
    * 
    */
    
 
   public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
      if (value.getClass() == CDAType.class  )
      {
          CDAType cdaType = (CDAType) value;
         return String.valueOf(cdaType.ordinal());
      }
      else
      {
         return null;
      }

   }


   @Transactional
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
         if (value != null)
      {
         try
         {
            Integer id = Integer.parseInt(value);
            if (id != null)
            {

               for(CDAType ct:CDAType.values()){
                   if (ct.ordinal() == id){
                       return ct;
                   }
               }
            }
         }
         catch (NumberFormatException e) {
            ExceptionLogging.logException(e, log);
         }
      }
      return null;
   }


}
