package net.ihe.gazelle.cdagen.hl7templates.validation;

import java.io.File;
import java.io.IOException;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.async.Asynchronous;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.utils.FileReadWrite;

@Stateless
@Name("processValidation")
public class ProcessValidation implements ProcessValidationInterface {

	private static Logger log = LoggerFactory.getLogger(ProcessValidation.class);

	@Asynchronous
	public void processJarValidation(String path, String pathOutput, String validationExecPath, Integer currentHL7TemplValidationId, boolean reportAllChecks) throws IOException,
	InterruptedException {
		File fileOutput = new File(pathOutput + File.separator + currentHL7TemplValidationId );
		fileOutput.mkdirs();
		String outputFilePath = fileOutput.getAbsolutePath() + "/logs.txt";
		String request = validationExecPath;
		if(!reportAllChecks){
			request+= " -prb";
		}
		request+= " -out \"" + fileOutput.getAbsolutePath() + "\" -bbr \"" + path + "\" > " + outputFilePath + " 2>&1 &";
		String requestPath = fileOutput.getAbsolutePath() + "/exec.sh";
		log.info("request::>> {}", request);
		FileReadWrite.printDoc(request, requestPath);

		File execFile = new File(requestPath);
		if (!execFile.setExecutable(true)) {
			log.info("Problem to set the file %s executable", execFile.getAbsolutePath());
		}
		final Process processRm = Runtime.getRuntime().exec("sh " + requestPath);
		
		processRm.waitFor();
		processRm.destroy();
		log.info("processJarValidation ended");
	}
	
}
