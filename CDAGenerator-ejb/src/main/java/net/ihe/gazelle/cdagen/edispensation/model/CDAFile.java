/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.cdagen.edispensation.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.cdagen.model.CDADocument;
import net.ihe.gazelle.cdagen.model.CDAType;


@Entity
@Name("cdaFile")
@Table(name="cda_file", schema="public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cda_file_sequence", sequenceName = "cda_file_id_seq", allocationSize=1)
public class CDAFile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3435397463635134710L;
	
	@Id
	@NotNull
	@Column(name="id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cda_file_sequence")
	private Integer id;
	
	@Lob
	@Type(type = "text")
	@Column(name="cda_file_content")
	private String cdaFileContent;
	
	@Column(name="file_document")
	private CDADocument fileDocument; 
	
	@Column(name="cda_type")
	private CDAType cdaType;
	
	@Column(name="date_creation")
    private Date dateCreation;
	
	@Column(name="creator")
    private String creator;
	
	@OneToMany(mappedBy = "prescription", fetch = FetchType.LAZY)
    private List<CDAGeneration> cdaGenerationsAsPrescription;
	
	@OneToMany(mappedBy = "dispensation", fetch = FetchType.LAZY)
    private List<CDAGeneration> cdaGenerationsAsDispensation;
	
	/**
	 * Constructor
	 */
	public CDAFile(){
		// default constructor
	}
	


    public CDAFile(String cdaFileContent, CDADocument fileDocument,
            CDAType cdaType, Date dateCreation) {
        super();
        this.cdaFileContent = cdaFileContent;
        this.fileDocument = fileDocument;
        this.cdaType = cdaType;
        this.dateCreation = dateCreation;
    }



    /**
	 * Getters and Setters
	 */
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CDADocument getFileDocument() {
        return fileDocument;
    }



    public void setFileDocument(CDADocument fileDocument) {
        this.fileDocument = fileDocument;
    }



    public CDAType getCdaType() {
        return cdaType;
    }



    public void setCdaType(CDAType cdaType) {
        this.cdaType = cdaType;
    }



    public Date getDateCreation() {
        return dateCreation;
    }


    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public List<CDAGeneration> getCdaGenerationsAsPrescription() {
        return cdaGenerationsAsPrescription;
    }

    public void setCdaGenerationsAsPrescription(
            List<CDAGeneration> cdaGenerationsAsPrescription) {
        this.cdaGenerationsAsPrescription = cdaGenerationsAsPrescription;
    }

    public List<CDAGeneration> getCdaGenerationsAsDispensation() {
        return cdaGenerationsAsDispensation;
    }

    public void setCdaGenerationsAsDispensation(
            List<CDAGeneration> cdaGenerationsAsDispensation) {
        this.cdaGenerationsAsDispensation = cdaGenerationsAsDispensation;
    }

    public String getCdaFileContent() {
        return cdaFileContent;
    }

    public void setCdaFileContent(String cdaFileContent) {
        this.cdaFileContent = cdaFileContent;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((cdaFileContent == null) ? 0 : cdaFileContent.hashCode());
        result = prime * result + ((cdaType == null) ? 0 : cdaType.hashCode());
        result = prime * result
                + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result
                + ((fileDocument == null) ? 0 : fileDocument.hashCode());
        return result;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CDAFile other = (CDAFile) obj;
        if (cdaFileContent == null) {
            if (other.cdaFileContent != null)
                return false;
        } else if (!cdaFileContent.equals(other.cdaFileContent))
            return false;
        if (cdaType != other.cdaType)
            return false;
        if (dateCreation == null) {
            if (other.dateCreation != null)
                return false;
        } else if (!dateCreation.equals(other.dateCreation))
            return false;
        if (fileDocument != other.fileDocument)
            return false;
        return true;
    }



    @Override
    public String toString() {
        return "CDAFile [id=" + id + ", cdaFileContent=" + cdaFileContent
                + ", fileDocument=" + fileDocument + ", cdaType=" + cdaType
                + ", dateCreation=" + dateCreation + "]";
    }




	
}
