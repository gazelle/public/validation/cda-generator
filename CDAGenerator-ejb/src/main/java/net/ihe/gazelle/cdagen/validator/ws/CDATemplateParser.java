package net.ihe.gazelle.cdagen.validator.ws;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jfree.util.Log;

import net.ihe.gazelle.cda.POCDMT000040ClinicalDocument;
import net.ihe.gazelle.cdagen.constraint.action.FileReadWrite;
import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.com.templates.TemplateId;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.epsosarb.CommonParser;
import net.ihe.gazelle.epsosarb.EDispensationParser;
import net.ihe.gazelle.epsosarb.EPrescriptionParser;
import net.ihe.gazelle.epsosarb.PatientSummaryParser;
import net.ihe.gazelle.gen.common.TemplateParser;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;

public class CDATemplateParser{

	private CDATemplateParser(){}

	public static Template generateTemplateDescriberFromObject(Object obj, List<TemplateParser> listParser, String location, List<? extends Object> listNotifications){
		if ((listParser == null) || (listParser.isEmpty())){
			return null;
		}
		Template template = new Template();
		template.setValidation("Report");
		if (listNotifications != null){
			for (Object object : listNotifications) {
				if (object instanceof Error){
					template.setValidation("Error");
					break;
				}
				if (object instanceof Warning){
					template.setValidation("Warning");
				}
			}
		}
		extractTemplatesFromObject(obj, template, listNotifications, location);
		updateTemplateByNames(template, listParser);
		return template;
	}

	protected static void extractTemplatesFromObject(Object obj, Template tempParent, List<? extends Object> listNotifications, String location){
		if (objHasTemplateIdField(obj)){
			Template temp = new Template();
			temp.setType(obj.getClass().getSimpleName());
			temp.setLocation(location);
			temp.setValidation(extractResultValidation(location, listNotifications));
			try {
				Field f = obj.getClass().getField("templateId");
				Object templates = f.get(obj);
				if (templates instanceof List){
					List<?> temps = (List<?>)templates;
					for (Object object : temps) {
						if (object instanceof II){
							II ii = (II)object;
							TemplateId tid = new TemplateId();
							tid.setId(ii.getRoot());
							temp.getTemplateId().add(tid);
						}
					}
				}
				else{
					if (templates instanceof II){
						II ii = (II)templates;
						TemplateId tid = new TemplateId();
						tid.setId(ii.getRoot());
						temp.getTemplateId().add(tid);
					}
				}
			} catch (Exception e) {}
			tempParent.getTemplate().add(temp);
			Map<String, Object> listFils = getAllsubAttributes(obj);
			if (listFils != null){
				for (Entry<String, Object> entry : listFils.entrySet()) {
					extractTemplatesFromObject(entry.getValue(), temp, listNotifications, location + "/" + entry.getKey());
				}
			}
		}
		else{
			Map<String, Object> listFils = getAllsubAttributes(obj);
			if (listFils != null){
				for (String fieldName : listFils.keySet()) {
					extractTemplatesFromObject(listFils.get(fieldName), tempParent,  listNotifications, location + "/" + fieldName);
				}
			}
		}
	}
	
	protected static String extractResultValidation (String location, List<? extends Object> listNotifications){
		String res = "Report";
		if (listNotifications != null){
			for (Object not : listNotifications) {
				if (not instanceof Notification){
					Notification notification = (Notification)not;
					if (notification.getLocation().contains(location)){
						if (notification instanceof Error){
							res =  "Error";
							break;
						}
						if (notification instanceof Warning){
							res =  "Warning";
						}
					}
				}
			}
		}
		return res;
	}
	
	protected static void updateTemplateByNames(Template template, List<TemplateParser> listParser){
		if (listParser != null){
			for (TemplateParser templateParser : listParser) {
				templateParser.updateTemplatesTree(template);
			}
		}
	}

	protected static Map<String, Object> getAllsubAttributes(Object obj){
		if (obj == null) return null;
		if (obj.getClass().getPackage().getName().equals("java.lang")) return null;
		if (obj.getClass().isEnum()) return null;
		Map<String, Object> res = new LinkedHashMap<>();
		Field[] fs = obj.getClass().getFields();
		for (Field ff : fs) {
			try {
				Object objfils = ff.get(obj);
				if (objfils instanceof List){
					int i = 0;
					for (Object oo : (List<?>)objfils) {
						res.put(ff.getName() + "[" + i + "]", oo);
						i++;
					}
				}
				else{
					res.put(ff.getName(), objfils);
				}
			} catch (Exception e) {} 

		}
		return res;
	}

	protected static boolean objHasTemplateIdField(Object obj){
		boolean res = false;
		try {
			Field f = obj.getClass().getField("templateId");
			Object templates = f.get(obj);
			if (templates != null){
				if (templates instanceof List){
					if (((List)templates).size()>0){
						res = true;
					}
				}
				else if (templates instanceof II){
					res = true;
				}
			}
		} catch (Exception e) {}
		return res;
	}


	protected static POCDMT000040ClinicalDocument load(InputStream is)
			throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cda");
		Unmarshaller u = jc.createUnmarshaller();
		return (POCDMT000040ClinicalDocument) u.unmarshal(is);
	}

	public static void main(String[] args){
		POCDMT000040ClinicalDocument cda = null;
		try {
			cda = load(new FileInputStream("/home/aboufahj/Téléchargements/ch.80756999999999993000.L3.F.1(1).xml"));
			ArrayList<TemplateParser> listParser = new ArrayList<>();
			listParser.add(new CommonParser());
			listParser.add(new PatientSummaryParser());
			listParser.add(new EDispensationParser());
			listParser.add(new EPrescriptionParser());

			DetailedResult res = MicroDocumentValidation.validateEPSOSPSPIVOT(
					FileReadWrite.readDoc("/home/aboufahj/Téléchargements/ch.80756999999999993000.L3.F.1(1).xml"));

			Template temp = generateTemplateDescriberFromObject(cda, listParser, "/ClinicalDocument", res.getMDAValidation().getWarningOrErrorOrNote());
			JAXBContext jj = JAXBContext.newInstance(DetailedResult.class);
			Marshaller m = jj.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(res, System.out);
		} catch (IOException | JAXBException e) {
			Log.error("error to process", e);
		}

	}



}

