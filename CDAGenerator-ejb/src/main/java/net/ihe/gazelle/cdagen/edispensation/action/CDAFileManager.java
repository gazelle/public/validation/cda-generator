package net.ihe.gazelle.cdagen.edispensation.action;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import net.ihe.gazelle.cdagen.datamodel.CDAFileDataModel;
import net.ihe.gazelle.cdagen.edispensation.model.CDAFile;
import net.ihe.gazelle.cdagen.model.CDADocument;
import net.ihe.gazelle.cdagen.model.CDAType;
import net.ihe.gazelle.cdagen.tools.XSLTransformer;

/**
 * @author abderrazek boufahja
 */
@Name("cdaFileManager")
@Scope(ScopeType.PAGE)
public class CDAFileManager implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Logger
    private static Log log;
    
    private CDAFileDataModel cdaFilesFounds;
    
    private boolean searchForIdentity = false;
    
    private CDAFile selectedCDAFile;
    
    private Date startDate;
    
    private Date endDate;
    
    private CDAType cdaType;
    
    private CDADocument cdaDocument;
    
    private String viewMode = "xml";
    
    public String getViewMode() {
        return viewMode;
    }

    public void setViewMode(String viewMode) {
        this.viewMode = viewMode;
    }

    public CDAType getCdaType() {
        return cdaType;
    }

    public void setCdaType(CDAType cdaType) {
        this.cdaType = cdaType;
        this.getCdaFilesFounds().setCdaType(cdaType);
        this.getCdaFilesFounds().resetCache();
    }

    public CDADocument getCdaDocument() {
        return cdaDocument;
    }

    public void setCdaDocument(CDADocument cdaDocument) {
        this.cdaDocument = cdaDocument;
        this.getCdaFilesFounds().setFileDocument(cdaDocument);
        this.getCdaFilesFounds().resetCache();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
        this.getCdaFilesFounds().setStartDate(startDate);
        this.getCdaFilesFounds().resetCache();
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
        this.getCdaFilesFounds().setEndDate(endDate);
        this.getCdaFilesFounds().resetCache();
    }

    public CDAFile getSelectedCDAFile() {
        return selectedCDAFile;
    }

    public void setSelectedCDAFile(CDAFile selectedCDAFile) {
        this.selectedCDAFile = selectedCDAFile;
    }

    public CDAFileDataModel getCdaFilesFounds() {
        if (cdaFilesFounds == null){
            cdaFilesFounds = new CDAFileDataModel(null);
        }
        return cdaFilesFounds;
    }

    public void setCdaFilesFounds(CDAFileDataModel cdaFilesFounds) {
        this.cdaFilesFounds = cdaFilesFounds;
    }

    public boolean isSearchForIdentity() {
        return searchForIdentity;
    }

    public void setSearchForIdentity(boolean searchForIdentity) {
        this.searchForIdentity = searchForIdentity;
        this.getCdaFilesFounds().setCreator(Identity.instance().getCredentials().getUsername());
        this.getCdaFilesFounds().resetCache();
    }
    
    public void displayCDAFile(CDAFile cdafile) {
        selectedCDAFile = cdafile;
    }

    public void applyFilter(){
        this.cdaFilesFounds.resetCache();
    }
    
    public void resetFilter(){
        this.setEndDate(null);
        this.setStartDate(null);
    }
    
    public List<CDAType> listCDATypes(){
        return Arrays.asList(CDAType.values());
    }
    
    public List<CDADocument> listFileDocuments(){
        return Arrays.asList(CDADocument.values());
    }
    
    public void updateViewForMessages(){
        if (this.selectedCDAFile != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance("viewResultBean");
            if ((this.viewMode != null) && (viewMode.equals("html"))){
                vrl.seteDispensationHtml1(this.getCDAModified(this.selectedCDAFile.getCdaFileContent()));
            }
        }
    }
    
    public String getCDAModified(String cda){
        if (cda != null){
            String res = "";
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            res = XSLTransformer.resultTransformation(cda, "cda.xsl", null, entityManager);
            return res;
        }
        return null;
    }
    

}
