package net.ihe.gazelle.cdagen.hl7templates.validator.ws;

import org.apache.commons.beanutils.ConvertUtils;

public class TemplateIdentifier {
	
	public TemplateIdentifier() {}
	
	public TemplateIdentifier(String templateIdentifierString) {
		TemplateIdentifier templateIdentifier = (TemplateIdentifier) ConvertUtils.convert(templateIdentifierString, TemplateIdentifier.class);
		this.id = templateIdentifier.id;
		this.name = templateIdentifier.name;
		this.versionLabel = templateIdentifier.versionLabel;
	}
	
	public TemplateIdentifier(TemplateIdentifier templateIdentifier) {
		this.id = templateIdentifier.id;
		this.name = templateIdentifier.name;
		this.versionLabel = templateIdentifier.versionLabel;
	}
	
	private String id;
	
	private String versionLabel;
	
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersionLabel() {
		return versionLabel;
	}

	public void setVersionLabel(String versionLabel) {
		this.versionLabel = versionLabel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((versionLabel == null) ? 0 : versionLabel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemplateIdentifier other = (TemplateIdentifier) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (versionLabel == null) {
			if (other.versionLabel != null)
				return false;
		} else if (!versionLabel.equals(other.versionLabel))
			return false;
		return true;
	}
	

}
