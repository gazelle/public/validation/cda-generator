package net.ihe.gazelle.cdagen.bbr.model;

/**
 * Created by xfs on 24/11/16.
 */
public enum StatusEnum {

    INITIAL("Initial"),
    BAD_URL("Bad URL"),
    COMPILING("Compiling"),
    COMPILED("Compiled"),
    AUTOMATIC("Automatic");


    private String label;

    StatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
