package net.ihe.gazelle.cdagen.edispensation.action;

import java.util.HashMap;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.tools.XSLTransformer;

public class DispGenerator {
    
    public static String generateDispensation(String prescription,
            String message_uuid, String effectiveTime,
            String messageExtension, String messageAssagningAuthority,
            String language, 
            String orgRoot, String orgExtension, String orgAA,
            String authorLN, String authorFN,  EntityManager entityManager){

        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put(EDEnum.ID.getValue(), message_uuid);
        hm.put(EDEnum.EXTENSION.getValue(), messageExtension);
        hm.put(EDEnum.AA.getValue(), messageAssagningAuthority);
        hm.put(EDEnum.EFFECTIVETIME.getValue(), effectiveTime);
        hm.put(EDEnum.LANGUAGE.getValue(), language);
        hm.put(EDEnum.PHARMA_ID_ROOT.getValue(), orgRoot);
        hm.put(EDEnum.PHARMA_ID_EXTENSION.getValue(), orgExtension);
        hm.put(EDEnum.PHARMA_ID_ASSIGNINGAUTHORITYNAME.getValue(), orgAA);
        hm.put(EDEnum.PHARMA_FAMILY.getValue(), authorLN);
        hm.put(EDEnum.PHARMA_GIVEN.getValue(), authorFN);
        hm.put(EDEnum.PHARMA_ORG_ID_EXTENSION.getValue(), orgExtension);
        hm.put(EDEnum.PHARMA_ORG_ID_ROOT.getValue(), orgRoot);
        hm.put(EDEnum.PHARMA_ORG_ID_NAME.getValue(), orgAA);
        hm.put(EDEnum.CUTODIAN_ID_ROOT.getValue(), orgRoot);
        hm.put(EDEnum.CUTODIAN_ID_EXTENSION.getValue(), orgExtension);
        hm.put(EDEnum.CUTODIAN_NAME.getValue(), orgAA);
        
        
        String dispensation = XSLTransformer.resultTransformation(prescription, "eP2eD.xsl", hm, entityManager);
        dispensation = net.ihe.gazelle.xmltools.xml.XmlFormatter.prettyFormat(dispensation);
        return dispensation;
    }

}
