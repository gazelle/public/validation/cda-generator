package net.ihe.gazelle.cdagen.simulator.common.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlUtil {
	
	private XmlUtil(){}

	private static final Charset UTF_8 = Charset.forName("UTF-8");

	static {
		org.apache.xml.security.Init.init();
	}

	public static Document parse(String document) throws ParserConfigurationException, SAXException, IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(document.getBytes(UTF_8));

		// Rebuild the document
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		return builder.parse(bais);
	}

}
