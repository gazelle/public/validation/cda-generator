package net.ihe.gazelle.cdagen.tools;

import java.io.File;
import java.net.URL;
import java.util.Map;

import javax.persistence.EntityManager;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

/**
 * @author abderrazek boufahja
 */
public class XSLTransformer {
	
	private XSLTransformer(){}
    
    public static String resultTransformation(String inDetailedResult, String inXslPath, Map<String, Object> map, EntityManager entityManager)
    {
       if (inXslPath == null || inXslPath.length() == 0){
            return null;
       }
       
       String absPath = ApplicationPreferenceManager.getStringValue("xsl_path")+ File.separator + inXslPath;
       System.setProperty("javax.xml.transform.TransformerFactory", 
    		   "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
       return net.ihe.gazelle.xmltools.xsl.XSLTransformer.resultTransformation(inDetailedResult, absPath, map);
    }
    
    public static String resultTransformation(String inDetailedResult, URL urlXslPath, Map<String, Object> map, EntityManager entityManager)
    {
       if (urlXslPath == null){
            return null;
       }
       
       String absPath = urlXslPath.toString();
       return net.ihe.gazelle.xmltools.xsl.XSLTransformer.resultTransformation(inDetailedResult, absPath, map);
    }

}
    