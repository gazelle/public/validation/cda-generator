package net.ihe.gazelle.cdagen.datamodel;

import net.ihe.gazelle.cdagen.model.Patient;
import net.ihe.gazelle.cdagen.model.PatientQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

public class PatientDataModel extends FilterDataModel<Patient>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String creator;
	
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public PatientDataModel(String creator) {
		super(new Filter<Patient>(getCriterionList()));
		this.creator = creator;
	}
	
	private static HQLCriterionsForFilter<Patient> getCriterionList() {
		PatientQuery query = new PatientQuery();
		return query.getHQLCriterionsForFilter();
	}
	
	
	@Override
	public void appendFiltersFields(HQLQueryBuilder<Patient> queryBuilder) 
	{
		  if (creator != null && !creator.isEmpty()) {
			  queryBuilder.addRestriction(HQLRestrictions.eq("creator", creator));
		  }
	 }
	

@Override
        protected Object getId(Patient t) {
        return t.getId();
        }
}
