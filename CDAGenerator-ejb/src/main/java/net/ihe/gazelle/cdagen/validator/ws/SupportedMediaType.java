package net.ihe.gazelle.cdagen.validator.ws;

import java.util.ArrayList;
import java.util.List;

public enum SupportedMediaType {
	
	TEXTPLAIN("text/plain"), APPLICATIONZIP("application/zip");
	
	private String mediaType;
	
	public String getMediaType() {
		return mediaType;
	}

	private SupportedMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
	public static List<String> getListSupportedMediaType() {
		ArrayList<String> res = new ArrayList<>();
		for (SupportedMediaType smt : SupportedMediaType.values()) {
			res.add(smt.getMediaType());
		}
		return res;
	}

}
