package net.ihe.gazelle.cdagen.simulator.common.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.ApplicationPreferenceQuery;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.SQLinjectionFilter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;

@Name("applicationConfiguration")
@Scope(ScopeType.PAGE)
public class ApplicationConfiguration implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String ENTITY_MANAGER = "entityManager";

	private static final String ADMIN_APPLICATION_CONFIGURATION_SEAM = "/admin/applicationConfiguration.seam";

	private static final String ADMIN_DISPLAY_APPLICATION_PREFERENCE_SEAM = "/admin/displayApplicationPreference.seam";

	private static Logger log = LoggerFactory.getLogger(ApplicationConfiguration.class);

    private ApplicationPreference selectedPreference;

    public ApplicationPreference getSelectedPreference() {
        return selectedPreference;
    }

    public void setSelectedPreference(ApplicationPreference selectedPreference) {
        this.selectedPreference = selectedPreference;
    }

    public static String getValueOfVariable(String value) {
        return ApplicationPreferenceManager.getStringValue(value);
    }

    public boolean isCas() {
        return !ApplicationPreferenceManager.getBooleanValue("ip_login");
    }

    public boolean isCasEnabled() {
        return ApplicationPreferenceManager.getBooleanValue("cas_enabled");
    }

    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

    public ApplicationConfiguration() {
    }

    public ApplicationConfiguration(String var, String val) {
        EntityManager em = (EntityManager) Component.getInstance(ENTITY_MANAGER);
        ApplicationPreference ap = new ApplicationPreference();
        ap.setClassName("java.lang.String");
        ap.setPreferenceName(var);
        ap.setPreferenceValue(val);
        em.merge(ap);
        em.flush();
    }

    public String getValue() {
        return null;
    }

    public String createNewApplicationPreference() {
        return ADMIN_DISPLAY_APPLICATION_PREFERENCE_SEAM;
    }

    public void createMissingKeys() {
        log.debug("Create missing keys");
        log.info("Create missing keys");
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ApplicationPreferenceQuery applicationPreferenceQuery;
        ApplicationPreference ap = null;
        for (PreferencesKey preference : PreferencesKey.values()) {
            applicationPreferenceQuery = new ApplicationPreferenceQuery();
            String friendlyName = preference.getFriendlyName();
            applicationPreferenceQuery.preferenceName().eq(friendlyName);
            ap = applicationPreferenceQuery.getUniqueResult();

            if (ap == null) {
                ap = new ApplicationPreference();
                ap.setClassName(preference.getType());
                ap.setDescription(preference.getDescription());
                ap.setPreferenceName(friendlyName);
                ap.setPreferenceValue(preference.getDefaultValue());
                entityManager.merge(ap);
                entityManager.flush();
            }
        }
        CSPHeaderFilter.clearCache();
        SQLinjectionFilter.resetInjectionFilter();
        FacesMessages.instance().add("Missing keys are now created !");
    }

    public GazelleListDataModel<ApplicationPreference> getApplicationPreferences() {
        List<ApplicationPreference> lpr = getApplicationPreferencesList();
        return new GazelleListDataModel<>(lpr);
    }

    public List<ApplicationPreference> getApplicationPreferencesList() {
        EntityManager entityManager = (EntityManager) Component.getInstance(ENTITY_MANAGER);
        HQLQueryBuilder<ApplicationPreference> builder = new HQLQueryBuilder<>(entityManager,
                ApplicationPreference.class);
        return builder.getList();
    }

    public String displayApplicationPreference(ApplicationPreference preference) {
        return "/admin/displayApplicationPreference.seam?id=" + preference.getId();
    }

    public String backToPreferenceList() {
        return ADMIN_APPLICATION_CONFIGURATION_SEAM;
    }

    public void initPreferenceApplicationDisplay() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                EntityManager entityManager = (EntityManager) Component.getInstance(ENTITY_MANAGER);
                selectedPreference = entityManager.find(ApplicationPreference.class,
                        Integer.parseInt(urlParams.get("id")));
            } catch (NumberFormatException e) {
                selectedPreference = null;
            }
        } else {
            selectedPreference = new ApplicationPreference();
            selectedPreference.setClassName("java.lang.String");
        }
    }

    public void save() {
        if (selectedPreference != null) {
            validateInputs();
            EntityManager entityManager = (EntityManager) Component.getInstance(ENTITY_MANAGER);
            selectedPreference = entityManager.merge(selectedPreference);
            entityManager.flush();
            FacesMessages.instance().add("Application preference saved with success.");
            CSPHeaderFilter.clearCache();
            SQLinjectionFilter.resetInjectionFilter();
        }
    }

    private void validateInputs() {
        boolean containsWhiteSpaces = false;
        if (!selectedPreference.getClassName().trim().equals(selectedPreference.getClassName())) {
            containsWhiteSpaces = true;
        } else if (!selectedPreference.getDescription().trim().equals(selectedPreference.getDescription())) {
            containsWhiteSpaces = true;
        } else if (!selectedPreference.getPreferenceName().trim().equals(selectedPreference.getPreferenceName())) {
            containsWhiteSpaces = true;
        } else if (!selectedPreference.getPreferenceValue().trim().equals(selectedPreference.getPreferenceValue())) {
            containsWhiteSpaces = true;
        }
        if (containsWhiteSpaces) {
            FacesMessages.instance().addFromResourceBundle(Severity.WARN, "Your inputs contain whitespaces !");
        }
    }

    public void resetHttpHeaders() {
        ApplicationPreferenceManager.instance().resetHttpHeaders();
        FacesMessages.instance().add("Http header is reseted !");
    }

    public boolean enableToolsMenu() {
        ApplicationPreferenceManager.instance();
        return ApplicationPreferenceManager.getBooleanValue("enable_tools_menu");
    }

}
