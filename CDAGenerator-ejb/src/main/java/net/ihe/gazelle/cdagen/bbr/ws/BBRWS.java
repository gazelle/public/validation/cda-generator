package net.ihe.gazelle.cdagen.bbr.ws;

import java.io.File;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.seam.annotations.Name;

import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

/**
 * Created by xfs on 31/01/17.
 */

@Stateless
@Name("bbrws")
public class BBRWS implements BBRWSLocal {

    public Response hello(@Context HttpServletRequest req) {
        return Response.ok("hello").build();
    }

    @Override
    @GET
    @Path("/downloadBBRZip/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(value = "Download BBR in ZIP", responseClass = "javax.ws.rs.core.Response")
    public Response downloadBBRZIP(@ApiParam(value = "Download BBR in ZIP format") @PathParam("id") String bbrId) {

        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        File file = new File(bbrPathString+"/generatedValidators/archives/"+bbrId+"/latest.zip");
        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition", "attachment;filename=bbr.zip");
        return response.build();

    }


}