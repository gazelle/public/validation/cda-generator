package net.ihe.gazelle.cdagen.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.model.Patient;

public final class PatientDAO {
	
	private PatientDAO() {
		// private constructor
	}
    
    public static Patient mergePatient(Patient pp, EntityManager em){
        Patient mergedpp = em.merge(pp);
        em.flush();
        return mergedpp;
    }
    
}
