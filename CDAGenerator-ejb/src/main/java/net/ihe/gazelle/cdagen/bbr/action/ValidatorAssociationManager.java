package net.ihe.gazelle.cdagen.bbr.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepositoryQuery;
import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.cdagen.bbr.model.TCDescriberQuery;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriber;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriberQuery;
import net.ihe.gazelle.cdagen.validator.ws.Validators;
import net.ihe.gazelle.constraints.xsd.Package;
import net.ihe.gazelle.constraints.xsd.PackageQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * Created by xfs on 07/12/16.
 */

@Name("validatorAssociationManager")
@Scope(ScopeType.PAGE)
public class ValidatorAssociationManager {

	private ValidatorDescriber selectedValidatorAssociation;

	public ValidatorDescriber getSelectedValidatorAssociation() {
		return selectedValidatorAssociation;
	}

	public void setSelectedValidatorAssociation(ValidatorDescriber selectedValidatorAssociation) {
		this.selectedValidatorAssociation = selectedValidatorAssociation;
	}

	public void deleteSelectedValidatorAssociation() {
		EntityManager em = EntityManagerService.provideEntityManager();
		ValidatorDescriber validatorAssociationToDelete = em.find(ValidatorDescriber.class,
				selectedValidatorAssociation.getId());
		em.remove(validatorAssociationToDelete);
		em.flush();
	}

	public String save() {

		EntityManager em = EntityManagerService.provideEntityManager();
		em.merge(selectedValidatorAssociation);
		return "/admin/validatorAssociation/list.seam";
	}

	public void initSelectedValidatorAssociation() {

		EntityManager em = EntityManagerService.provideEntityManager();
		final FacesContext fc = FacesContext.getCurrentInstance();
		final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
		if (requestParameterMap.get("validatorAssociationId") != null) {
			Integer id = Integer.parseInt(requestParameterMap.get("validatorAssociationId"));
			selectedValidatorAssociation = em.find(ValidatorDescriber.class, id);
		} else {
			selectedValidatorAssociation = new ValidatorDescriber();
		}
	}

	public List<Package> getAvailablePackages() {

		PackageQuery packageQuery = new PackageQuery();
		packageQuery.name().order(true);
		return packageQuery.getList();
	}

	public List<ValidatorDescriber> getAvailableValidatorAssociationParents() {

		ValidatorDescriberQuery validatorDescriberQuery = new ValidatorDescriberQuery();
		validatorDescriberQuery.validatorName().order(true);
		return validatorDescriberQuery.getList();
	}

	public List<String> getAvailableValidators() {
		List<String> res = new ArrayList<>();
		Validators[] validators = Validators.values();
		for (Validators currentValidator : validators) {
			res.add(currentValidator.getValue());
		}

		BuildingBlockRepositoryQuery bbrListQuery = new BuildingBlockRepositoryQuery();
		List<BuildingBlockRepository> allBbr = bbrListQuery.getList();
		for (BuildingBlockRepository bbr : allBbr) {
			res.add(bbr.getLabel());
		}
		Collections.sort(res);
		return res;
	}

	public boolean isDeletable(ValidatorDescriber validatorDescriber) {
		boolean res = true;
		ValidatorDescriberQuery validatorDescriberQuery = new ValidatorDescriberQuery();
		validatorDescriberQuery.validatorAssociationParents().id().eq(validatorDescriber.getId());
		if (!validatorDescriberQuery.getList().isEmpty()) {
			res = false;
		}
		return res;
	}

	public List<TCDescriber> getAvailableTCDescribers() {
		TCDescriberQuery tcdq = new TCDescriberQuery();
		return tcdq.getList();
	}

}
