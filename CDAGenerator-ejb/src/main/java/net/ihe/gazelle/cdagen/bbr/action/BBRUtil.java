package net.ihe.gazelle.cdagen.bbr.action;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepositoryQuery;

public final class BBRUtil {
	
	private BBRUtil() {}

	public static List<String> getListActiveBBR() {
		BuildingBlockRepositoryQuery buildingBlockRepositoryQuery = new BuildingBlockRepositoryQuery();
		buildingBlockRepositoryQuery.active().eq(true);
        List<BuildingBlockRepository> result = buildingBlockRepositoryQuery.getList();
        List<String> res = new ArrayList<>();
        if (result != null) {
	        for (BuildingBlockRepository bbr : result) {
				res.add(bbr.getLabel());
			}
        }
        return res;
	}

	public static String getBBRByLabel(String label) {
		String version = null;
		BuildingBlockRepositoryQuery buildingBlockRepositoryQuery = new BuildingBlockRepositoryQuery();
		buildingBlockRepositoryQuery.label().like(label);
		List<BuildingBlockRepository> result = buildingBlockRepositoryQuery.getList();
		List<String> res = new ArrayList<>();
		if (result != null) {
			for (BuildingBlockRepository bbr : result) {
				res.add(bbr.getVersion());
				version = bbr.getVersion();
			}
			if(res.size() != 1 ){
				return "N/A";
			}
			else {
				return version;
			}
		}
		return "N/A";
	}


}
