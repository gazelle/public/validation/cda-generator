package net.ihe.gazelle.cdagen.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;

@Entity
@Name("cdaDoc")
@Table(name = "cda_doc")
@SequenceGenerator(name = "cda_doc_sequence", sequenceName = "cda_doc_id_seq", allocationSize = 1)
public class CDADoc implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger log = LoggerFactory.getLogger(CDADoc.class);
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cda_doc_sequence")
	private Integer id;

	@Column(name="path")
	private String path;
	
	@Column(name="author")
	private String author;
	
	@Column(name="creationDate")
	private Date creationDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		CDADoc other = (CDADoc) obj;
		if (author == null) {
			if (other.author != null){
				return false;
			}
		} else if (!author.equals(other.author))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null){
				return false;
			}
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (path == null) {
			if (other.path != null){
				return false;
			}
		} else if (!path.equals(other.path)){
			return false;
		}
		return true;
	}
	
	public String getContent(){
		StringBuilder buf = new StringBuilder();
		String repo = ApplicationConfiguration.getValueOfVariable("doc_path");
		try (FileReader fr = new FileReader(new File(repo + File.separator + this.id + "_" + this.path));
				BufferedReader br = new BufferedReader(fr);) {
			String line = br.readLine();
			while (line != null){
				buf.append(line);
				buf.append("\n");
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			log.info("File not found on the server", e);
		} catch (IOException e) {
			log.info("Problem to read the content.", e);
		}
		return buf.toString();
	}
	
	
}
