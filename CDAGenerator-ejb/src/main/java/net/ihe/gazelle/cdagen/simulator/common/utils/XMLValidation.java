package net.ihe.gazelle.cdagen.simulator.common.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;

public class XMLValidation {
	
	
	private static Logger log = LoggerFactory.getLogger(XMLValidation.class);
	
	private static final String XML_SCHEMA_NAMESPACE = "http://www.w3.org/2001/XMLSchema";

	private XMLValidation(){}
	
	private static Map<SAXParserFactory, Exception> listErrorsInParsingSchema = new HashMap<SAXParserFactory, Exception>();

	private static SAXParserFactory factoryBASIC;
	
	private static SAXParserFactory factoryCDA;

	private static SAXParserFactory factoryCDAEpsos;
	
	private static SAXParserFactory factoryCDAPharm;

	static{
		try {
			factoryBASIC = SAXParserFactory.newInstance();
			factoryBASIC.setValidating(false);
			factoryBASIC.setNamespaceAware(true);
		} catch (Exception e){
			e.printStackTrace();
			listErrorsInParsingSchema.put(factoryBASIC, e);
		}
		
		try {
			factoryCDA = SAXParserFactory.newInstance();
			factoryCDA.setNamespaceAware( true);
			SchemaFactory sfactory = SchemaFactory.newInstance(XML_SCHEMA_NAMESPACE);
			String xsdpath = ApplicationConfiguration.getValueOfVariable("cda_xsd");
			if (xsdpath != null) {
				Schema schema = sfactory.newSchema(new File(ApplicationConfiguration.getValueOfVariable("cda_xsd")));
				factoryCDA.setSchema(schema);
			}
			else {
				log.error("The application variable cda_xsd is not defined");
			}
		} catch (Exception e){
			e.printStackTrace();
			listErrorsInParsingSchema.put(factoryCDA, e);
		}

		try {
			factoryCDAEpsos = SAXParserFactory.newInstance();
			factoryCDAEpsos.setNamespaceAware( true);
			SchemaFactory sfactory = SchemaFactory.newInstance(XML_SCHEMA_NAMESPACE);
			String xsdpath = ApplicationConfiguration.getValueOfVariable("cdaepsos_xsd");
			if (xsdpath != null) {
				Schema schema = sfactory.newSchema(new File(ApplicationConfiguration.getValueOfVariable("cdaepsos_xsd")));
				factoryCDAEpsos.setSchema(schema);
			}
			else {
				log.error("The application variable cdaepsos_xsd is not defined");
			}
		} catch (Exception e){
			e.printStackTrace();
			listErrorsInParsingSchema.put(factoryCDAEpsos, e);
		}
		
		try {
			factoryCDAPharm = SAXParserFactory.newInstance();
			factoryCDAPharm.setNamespaceAware( true);
			SchemaFactory sfactory = SchemaFactory.newInstance(XML_SCHEMA_NAMESPACE);
			String xsdpath = ApplicationConfiguration.getValueOfVariable("cdapharm_xsd");
			if (xsdpath != null) {
				Schema schema = sfactory.newSchema(new File(ApplicationConfiguration.getValueOfVariable("cdapharm_xsd")));
				factoryCDAPharm.setSchema(schema);
			}
			else {
				log.error("The application variable cdapharm_xsd is not defined");
			}
		} catch (Exception e){
			e.printStackTrace();
			listErrorsInParsingSchema.put(factoryCDAPharm, e);
		}
		
	}

	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 * @param file
	 * @return
	 */
	public static DocumentWellFormed isXMLWellFormed(String string){
		return validateIfDocumentWellFormedXML(string, "", factoryBASIC);
	}
	
	private static DocumentWellFormed validateIfDocumentWellFormedXML(String cdaDocument, String xsdpath, SAXParserFactory factory){
		DocumentWellFormed dv = new DocumentWellFormed();
		return validXMLUsingXSD(cdaDocument, factory, dv);
	}


	/**
	 * 
	 * @param file
	 * @param xsdLocation
	 * @return
	 */
	private static DocumentValidXSD validXMLUsingXSD(String xdwDocument)
	{
		return validXMLUsingXSD(xdwDocument, factoryCDA);
	}
	
	private static DocumentValidXSD validXMLUsingPHARMXSD(String xdwDocument)
	{
		return validXMLUsingXSD(xdwDocument, factoryCDAPharm);
	}

	private static DocumentValidXSD validXMLUsingXSDForEpsos(String xdwDocument)
	{
		return validXMLUsingXSD(xdwDocument, factoryCDAEpsos);
	}
	
	private static DocumentValidXSD validXMLUsingXSD(String cdaDocument, SAXParserFactory factory)
	{
		DocumentValidXSD dv = new DocumentValidXSD();
		return validXMLUsingXSD(cdaDocument, factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T validXMLUsingXSD(String cdaDocument, SAXParserFactory factory, T dv)
	{
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		if (listErrorsInParsingSchema.containsKey(factory)) {
			exceptions.add(handleException(listErrorsInParsingSchema.get(factory)));
			return extractValidationResult(exceptions, dv);
		}
		else {
			try {
				ByteArrayInputStream bais = new ByteArrayInputStream(cdaDocument.getBytes("UTF8"));
				exceptions =XSDValidator.validateUsingFactoryAndSchema(bais, null, factory);
			} catch(Exception e){
				exceptions.add(handleException(e));
			}
			return extractValidationResult(exceptions, dv);
		}
	}
	
	private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, T dv)
	{

		dv.setResult("PASSED");

		if (exceptions == null || exceptions.isEmpty())
		{	
			dv.setResult("PASSED");
			return dv;
		}
		else
		{
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			for (ValidationException ve : exceptions)
			{
				if (ve.getSeverity() == null){
					ve.setSeverity("error");
				}
				if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))){
					nbOfWarnings ++;
				}
				else {
					nbOfErrors ++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (StringUtils.isNumeric(ve.getLineNumber())){
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (StringUtils.isNumeric(ve.getColumnNumber())){
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0){
				dv.setResult("FAILED");
			}
			return dv;
		}

	}

	private static ValidationException handleException(Exception e)
	{
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null){
			ve.setMessage("error on validating : " + e.getMessage());
		} 
		else if (e != null && e.getCause() != null && e.getCause().getMessage() != null){
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		}
		else{
			String expKind = "";
			if (e != null && e.getClass() != null && e.getClass().getSimpleName() != null) {
				expKind = e.getClass().getSimpleName();
			}
			ve.setMessage("error on validating. The exception generated is of kind : " + expKind);
		}
		ve.setSeverity("error");
		return ve;
	}


	/**
	 * Checks that the XDW document is valid (uses XDW.xsd file)
	 * @param file
	 * @return
	 */
	public static DocumentValidXSD isXSDValid(String document)
	{
		if (document == null){
			return null;
		}
		else {
			return validXMLUsingXSD(document);
		}
	}
	
	/**
	 * Checks that the CDA document is valid (uses CDA.xsd file)
	 * @param file
	 * @return
	 */
	public static DocumentValidXSD isXSDValidPharm(String document)
	{
		if (document == null){
			return null;
		}
		else {
			return validXMLUsingPHARMXSD(document);
		}
	}

	public static DocumentValidXSD isXSDValidEpsos(String document)
	{
		if (document == null){
			return null;
		}
		else {
			return validXMLUsingXSDForEpsos(document);
		}
	}
	
}
