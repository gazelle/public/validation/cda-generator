/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.cdagen.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;



/**
 *  <b>Class Description :  </b>ObjectType<br><br>
 * This class describes the ObjectType object, used by the Gazelle application.
 * 
 * This class belongs to the TM module.
 *
 * ObjectType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectType id</li>
 * <li><b>object</b> : Object corresponding to ObjectType</li>
 * <li><b>keyword</b> : Keyword corresponding to ObjectType</li>
 * </ul></br>
 * <b>Example of ObjectType</b> : - <br>
 *
 *
 * @class          			ObjectType.java
 * @package        			net.ihe.gazelle.objects.model
 * @author					Abderrazek Boufahja - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see        > 			aboufahj@irisa.fr - Jchatel@irisa.fr  -  http://www.ihe-europe.org
 * @version 				1.0 - 2009, April 15th
 *
 */
@Entity
@Name("cdaObjectType")
@Table(name = "cdagen_object_type")
@SequenceGenerator(name = "cdagen_object_type_sequence", sequenceName = "cdagen_object_type_id_seq", allocationSize = 1)
public class CDAObjectType  implements Serializable,Comparable<CDAObjectType> {
	

	
   //~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

   /** Serial version Id of this object! */
   private static final long serialVersionUID = 1884422500711441951L;

   //~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

   /** ObjectType id */
   @Id
   @Column(name = "id", unique = true, nullable = false)
   @NotNull
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cdagen_object_type_sequence")
   private Integer id;

   /** Keyword */
   @Column(name = "keyword", unique = true, nullable = false)
   @Size(max=250)
   private String keyword;
   
   /** Description */
   @Column(name = "description", unique = false, nullable = true)
   @Size(max=250)
   private String description;
   
   @ManyToMany(fetch=FetchType.EAGER)
   private List<ActorIntegrationProfileOption> actorIntegrationProfileOptions;
   
   // constructor
   
   public CDAObjectType() {
   }
   
   
   //~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

	   
	public List<ActorIntegrationProfileOption> getActorIntegrationProfileOptions() {
        return actorIntegrationProfileOptions;
    }
    
    
    public void setActorIntegrationProfileOptions(
            List<ActorIntegrationProfileOption> actorIntegrationProfileOptions) {
        this.actorIntegrationProfileOptions = actorIntegrationProfileOptions;
    }


    public Integer getId() {
		return id;
	}
	
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	public String getKeyword() {
		return keyword;
	}
	
	
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}

	//~ hashCodes and equals ////////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        return result;
    }

	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CDAObjectType other = (CDAObjectType) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        return true;
    }

	//~ Methods ////////////////////////////////////////////////////////////////////

	public int compareTo(CDAObjectType o) {
		if (this.getKeyword().compareToIgnoreCase(o.getKeyword())>0){
			return 1;
		}
		if (this.getKeyword().compareToIgnoreCase(o.getKeyword())<0) {
			return (-1);
		}
		return 0;
	}
	
	public void addAIPO(ActorIntegrationProfileOption aipo){
	    if (aipo != null){
	        if (this.getActorIntegrationProfileOptions() == null){
	            this.actorIntegrationProfileOptions = new ArrayList<ActorIntegrationProfileOption>();
	        }
	        this.actorIntegrationProfileOptions.add(aipo);
	    }
	}
	
}
