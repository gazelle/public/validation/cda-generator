package net.ihe.gazelle.cdagen.datamodel;

import java.util.Date;

import net.ihe.gazelle.cdagen.edispensation.model.CDAFile;
import net.ihe.gazelle.cdagen.edispensation.model.CDAFileQuery;
import net.ihe.gazelle.cdagen.model.CDADocument;
import net.ihe.gazelle.cdagen.model.CDAType;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

public class CDAFileDataModel extends FilterDataModel<CDAFile> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String creator;

	private CDADocument fileDocument;

	private CDAType cdaType;

	private Date startDate;

	public CDADocument getFileDocument() {
		return fileDocument;
	}

	public void setFileDocument(CDADocument fileDocument) {
		this.fileDocument = fileDocument;
	}

	public CDAType getCdaType() {
		return cdaType;
	}

	public void setCdaType(CDAType cdaType) {
		this.cdaType = cdaType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	private Date endDate;

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public CDAFileDataModel(String creator) {
		super(new Filter<CDAFile>(getCriterionList()));
		this.creator = creator;
	}

	private static HQLCriterionsForFilter<CDAFile> getCriterionList() {
		CDAFileQuery query = new CDAFileQuery();
		return query.getHQLCriterionsForFilter();
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<CDAFile> queryBuilder) {
		if (creator != null && !creator.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.eq("creator", creator));
		}

		if (fileDocument != null) {
			queryBuilder.addEq("fileDocument", fileDocument);
		}

		if (cdaType != null) {
			queryBuilder.addEq("cdaType", cdaType);
		}

		if (startDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.ge("dateCreation", startDate));
		}

		if (endDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.le("dateCreation", endDate));
		}

	}

	@Override
	protected Object getId(CDAFile t) {
		return t.getId();
	}
}
