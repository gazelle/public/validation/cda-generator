package net.ihe.gazelle.cdagen.edispensation.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.edispensation.model.CDAFile;

public final class CDAFileDAO {
	
	private CDAFileDAO() {
		// private constructor
	}
    
    public static CDAFile mergeCDAFile(CDAFile cdafile, EntityManager em){
    	CDAFile cdafile2 = em.merge(cdafile);
        em.flush();
        return cdafile2;
    }

}
