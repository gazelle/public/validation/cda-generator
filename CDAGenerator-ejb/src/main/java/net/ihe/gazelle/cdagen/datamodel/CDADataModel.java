package net.ihe.gazelle.cdagen.datamodel;

import net.ihe.gazelle.cdagen.model.CDADoc;
import net.ihe.gazelle.cdagen.model.CDADocQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

public class CDADataModel extends FilterDataModel<CDADoc> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CDADataModel() {
		super(new Filter<CDADoc>(getCriterionList()));
	}

	private static HQLCriterionsForFilter<CDADoc> getCriterionList() {
		CDADocQuery query = new CDADocQuery();
		return query.getHQLCriterionsForFilter();
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<CDADoc> queryBuilder) {
		// nothin to append right now
	}

	@Override
	protected Object getId(CDADoc t) {
		return t.getId();
	}
}
