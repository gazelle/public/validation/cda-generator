package net.ihe.gazelle.cdagen.scorecard.ws;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.cdagen.bbr.action.TCDescriberUtil;
import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriber;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriberQuery;
import net.ihe.gazelle.cdagen.validator.ws.POMVersion;
import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.constraints.xsd.Package;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.scoring.action.ScoringFulfiller;
import net.ihe.gazelle.scoring.model.ContextualInformation;
import net.ihe.gazelle.scoring.model.ValidationStatistics;
import net.ihe.gazelle.scoring.model.templates.TemplatesContainment;
import net.ihe.gazelle.scoring.utils.ValidationStatisticsMarshaller;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.MDAValidation;

/**
 * <b>Class Description : </b>ScoringJob<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 06/12/16
 * @class ScoringJob
 * @package net.ihe.gazelle.cda.scorecard.ws
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public final class ScoringJob {
	
	private ScoringJob() {
		// private constructor
	}

    public static final String UTF_8 = "UTF-8";

    public  static String computeScorecard(DetailedResult detailedResult, String validatorName, String oid) 
    		throws ScoringJobException, JAXBException, UnsupportedEncodingException {
        ValidationStatistics statistics = new ValidationStatistics();
        if (detailedResult != null && detailedResult.getMDAValidation() != null) {
            MDAValidation mdaValidation = detailedResult.getMDAValidation();
            List<Package> packages = getPackageListForValidator(validatorName);
            TCDescriber tcd = getTCDescriber(validatorName);
            TemplatesContainment tc = TCDescriberUtil.extractTemplateContainment(tcd);
            TemplatesContainment tcf = TCDescriberUtil.extractTemplateContainmentFlattened(tcd);
            
            if (packages.isEmpty()){
                throw new ScoringJobException("Validator not registered or no validation package found for validator " + validatorName);
            } else {
            	Template temp = null;
            	if (detailedResult.getTemplateDesc() != null && !detailedResult.getTemplateDesc().getTemplate().isEmpty()) {
            		temp = detailedResult.getTemplateDesc().getTemplate().get(0);
            	}
                ScoringFulfiller.fulfillValidationStatistics(statistics, mdaValidation, packages, tc, tcf, temp);
                ByteArrayOutputStream scorecard = new ByteArrayOutputStream();
                addContextualInformation(statistics, detailedResult, oid);
                ValidationStatisticsMarshaller.marshallValidationStatistics(statistics, scorecard);
                if (scorecard.size() > 0) {
                    return scorecard.toString(UTF_8);
                } else {
                    throw new ScoringJobException("No scorecard computed");
                }
            }
        } else {
            throw new ScoringJobException("DetailedResult or its MDAValidation section shall not be null");
        }
    }

    private static TCDescriber getTCDescriber(String validatorName) {
    	EntityManager entityManager = EntityManagerService.provideEntityManager();
        ValidatorDescriberQuery query = new ValidatorDescriberQuery(entityManager);
        query.validatorName().eq(validatorName);
        ValidatorDescriber validator = query.getUniqueResult();
        if (validator != null) {
        	return validator.getTcDescriber();
        }
        return null;
	}

	private static void addContextualInformation(ValidationStatistics statistics, DetailedResult result, String oid) {
        ContextualInformation contextualInformation = statistics.getContextualInformation();
        contextualInformation.setValidationIdentifier(oid);
        contextualInformation.setValidationToolIdentifier(result.getValidationResultsOverview().getValidationEngine());
        contextualInformation.setValidationVersion(result.getValidationResultsOverview().getValidationServiceVersion());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss (Z)");
        contextualInformation.setEffectiveDate(sdf.format(new Date()));
        contextualInformation.setStatisticToolIdentifier("Gazelle CDA Validation");
        contextualInformation.setStatisticVersion(POMVersion.getVersion());
        contextualInformation.setStatisticsIdentifier(getNextOidForStatistics());
    }

    private static String getNextOidForStatistics(){
        String root = PreferenceService.getString("scorecard_root_oid");
        ApplicationPreference index = ApplicationPreference.get_preference_from_db("scorecard_next_index");
        Integer nextIndex = new Integer(index.getPreferenceValue());
        Integer newIndex = nextIndex + 1;
        index.setPreferenceValue(newIndex.toString());
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(index);
        entityManager.flush();
        return root.concat(nextIndex.toString());
    }

    private static List<Package> getPackageListForValidator(String validatorName){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        ValidatorDescriberQuery query = new ValidatorDescriberQuery(entityManager);
        query.validatorName().eq(validatorName);
        ValidatorDescriber validator = query.getUniqueResult();
        if (validator != null){
            List<Package> packages = getPackagesForValidator(validator);
            if (packages != null && !packages.isEmpty()){
                return packages;
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    private static List<Package> getPackagesForValidator(ValidatorDescriber validator){
        List<Package> packages = new ArrayList<>();
        if (validator.getPackageList() != null && !validator.getPackageList().isEmpty()) {
            for (Package pack: validator.getPackageList()){
                if (!packages.contains(pack)){
                    packages.add(pack);
                }
            }
        }
        if (validator.getValidatorAssociationParents() != null && !validator.getValidatorAssociationParents().isEmpty()){
           for (ValidatorDescriber parent: validator.getValidatorAssociationParents()) {
               List<Package> packagesFromParent = getPackagesForValidator(parent);
               if (packagesFromParent != null && !packagesFromParent.isEmpty()){
                   for (Package packFromParent: packagesFromParent){
                       if (!packages.contains(packFromParent)){
                           packages.add(packFromParent);
                       }
                   }
               }
           }
        }
        return packages;
    }
}
