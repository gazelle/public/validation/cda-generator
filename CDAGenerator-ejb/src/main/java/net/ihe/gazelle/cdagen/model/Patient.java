package net.ihe.gazelle.cdagen.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.cdagen.edispensation.model.CDAGeneration;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.PatientAddress;

@Entity
@Name("patientCDAGen")
@DiscriminatorValue("cda_patient")
public class Patient extends AbstractPatient {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="creator")
	private String creator;
	
	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
	private List<CDAGeneration> cdaGenerations;


	/**
	 * Constructors
	 */
	public Patient(){
		// basic constructor
	}

	public Patient(AbstractPatient abPatient) {
		this.firstName = abPatient.getFirstName();
		this.lastName = abPatient.getLastName();
		this.motherMaidenName = abPatient.getMotherMaidenName();
		this.countryCode = abPatient.getCountryCode();
		this.genderCode = abPatient.getGenderCode();
		this.religionCode = abPatient.getReligionCode();
		this.raceCode = abPatient.getRaceCode();
		this.dateOfBirth = abPatient.getDateOfBirth();
		this.creationDate = abPatient.getCreationDate();
		this.nationalPatientIdentifier = abPatient.getNationalPatientIdentifier();
		this.characterSet = abPatient.getCharacterSet();
		this.ddsIdentifier = abPatient.getDdsIdentifier();
		if (this.addressList == null) {
			this.addressList = new ArrayList<>();
		}
		this.addressList.add(new PatientAddress());
		this.addressList.get(0).setCity(abPatient.getCity());
		this.addressList.get(0).setState(abPatient.getState());
		this.addressList.get(0).setStreet(abPatient.getStreet());
		this.addressList.get(0).setZipCode(abPatient.getZipCode());
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}


	public List<CDAGeneration> getCdaGenerations() {
        return cdaGenerations;
    }

    public void setCdaGenerations(List<CDAGeneration> cdaGenerations) {
        this.cdaGenerations = cdaGenerations;
    }

    /**
	 * saves and returns the patient
	 * @param inPatient
	 * @param entityManager
	 * @return
	 */
	public static Patient savePatient(Patient inPatient, EntityManager entityManager) {
		if (inPatient != null) {
			Patient res = entityManager.merge(inPatient);
			entityManager.flush();
			return res;
		}
		return null;
	}

	
	public static String getPermanentLink(Patient inPatient)
	{
		if (inPatient != null && inPatient.getId() != null)
		{
			return "/patient.seam?id=" + inPatient.getId();
		}
		else
		{
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cdaGenerations == null) ? 0 : cdaGenerations.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (cdaGenerations == null) {
			if (other.cdaGenerations != null)
				return false;
		} else if (!cdaGenerations.equals(other.cdaGenerations))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		return true;
	}
	
}
