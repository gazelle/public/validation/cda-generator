package net.ihe.gazelle.cdagen.hl7templates.validation;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.handler.model.CheckResult;

@Entity
@Name("hL7TemplValidation")
@Table(name="hl7temp_validation", schema="public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "hl7temp_validation_sequence", sequenceName = "hl7temp_validation_id_seq", allocationSize=1)
public class HL7TemplValidation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Column(name="id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="hl7temp_validation_sequence")
	private Integer id;
	
	private String url;
	
	private String name;
	
	private Boolean ended = false;
	
	private String token = UUID.randomUUID().toString();
	
	private transient String validationResult;
	
	private transient List<CheckResult> validationResultChecks;
	
	public HL7TemplValidation() {
		// default constructor
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getEnded() {
		return ended;
	}

	public void setEnded(Boolean ended) {
		this.ended = ended;
	}

	public List<CheckResult> getValidationResultChecks() {
		return validationResultChecks;
	}

	public void setValidationResultChecks(List<CheckResult> validationResultChecks) {
		this.validationResultChecks = validationResultChecks;
	}

	public String getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HL7TemplValidation other = (HL7TemplValidation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
}
