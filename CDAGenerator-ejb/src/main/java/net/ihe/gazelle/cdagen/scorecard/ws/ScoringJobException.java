package net.ihe.gazelle.cdagen.scorecard.ws;

public class ScoringJobException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScoringJobException(String str) {
		super(str);
	}

}
