package net.ihe.gazelle.cdagen.bbr.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.*;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

/**
 * Created by xfs on 25/11/16.
 */

@Entity
@Name("buildingBlockRepository")
@Table(name = "building_block_repository")
@SequenceGenerator(name = "building_block_repository_sequence", sequenceName = "building_block_repository_id_seq", allocationSize = 1)
public class BuildingBlockRepository implements Serializable {

    private static final String ZIP = "zip";

    private static final String ATTACHMENT_FILENAME = "attachment;filename=\"";

    private static final String CONTENT_DISPOSITION = "Content-Disposition";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "building_block_repository_sequence")
    private int id;

    @Column(name = "label")
    private String label;

    @Column(name = "online")
    private boolean online;

    @Column(name = "url")
    private String url;

    @Column(name = "bbrContent")
    private byte[] bbrContent;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "buildingDate")
    private Date buildingDate;

    @Column(name = "version")
    private String version;

    @Column(name = "status")
    private StatusEnum status;

    @Column(name = "active")
    private boolean active;

    @Column(name = "version_label")
    private String versionLabel;

    @Column(name = "ignore_templateid_requirements")
    private boolean ignoreTemplateidRequirements;

    @Column(name = "root_class_name")
    private String rootClassName;

    @Column(name = "hl7temp_cdaconffoldername")
    private String HL7TEMP_CDACONFFOLDERNAME;

    @Column(name = "java_version")
    private JavaVersionEnum javaVersion;

    @OneToMany
    @Cascade({CascadeType.ALL})
    private List<OtherFile> otherFiles;

    private StatusEnum currentStatus;

    @OneToOne
    @JoinColumn(name = "xsd_zip_file_id")
    @Cascade({CascadeType.ALL})
    private OtherFile xsdZipFile;

    public OtherFile getXsdZipFile() {
        return xsdZipFile;
    }

    public void setXsdZipFile(OtherFile xsdZipFile) {
        this.xsdZipFile = xsdZipFile;
    }

    public boolean alreadyBuilt() {
        return buildingDate != null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getBuildingDate() {
        return buildingDate;
    }

    public void setBuildingDate(Date buildingDate) {
        this.buildingDate = buildingDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public byte[] getBbrContent() {
        return bbrContent;
    }

    public void setBbrContent(byte[] bbrContent) {
        this.bbrContent = bbrContent;
    }

    public boolean isOnline() {
        return online;
    }

    public StatusEnum getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(StatusEnum currentStatus) {
        this.currentStatus = currentStatus;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public List<OtherFile> getOtherFiles() {
        if (this.otherFiles == null) {
            this.otherFiles = new ArrayList<>();
        }
        return otherFiles;
    }

    public void setOtherFiles(List<OtherFile> otherFiles) {
        this.otherFiles = otherFiles;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel.trim();
    }

    public boolean isIgnoreTemplateidRequirements() {
        return ignoreTemplateidRequirements;
    }

    public void setIgnoreTemplateidRequirements(boolean ignoreTemplateidRequirements) {
        this.ignoreTemplateidRequirements = ignoreTemplateidRequirements;
    }

    public String getRootClassName() {
        return rootClassName;
    }

    public void setRootClassName(String rootClassName) {
        this.rootClassName = rootClassName.trim();
    }

    public String getHL7TEMP_CDACONFFOLDERNAME() {
        return HL7TEMP_CDACONFFOLDERNAME;
    }

    public void setHL7TEMP_CDACONFFOLDERNAME(String HL7TEMP_CDACONFFOLDERNAME) {
        this.HL7TEMP_CDACONFFOLDERNAME = HL7TEMP_CDACONFFOLDERNAME.trim();
    }

    public JavaVersionEnum getJavaVersion() {
        return javaVersion;
    }

    public void setJavaVersion(JavaVersionEnum javaVersion) {
        this.javaVersion = javaVersion;
    }

    public static BuildingBlockRepository getBbrFromLabel(String validator) {

        BuildingBlockRepositoryQuery buildingBlockRepositoryQuery = new BuildingBlockRepositoryQuery();
        buildingBlockRepositoryQuery.label().eq(validator);
        return buildingBlockRepositoryQuery.getUniqueResult();
    }

    public void download() throws IOException {

        if (!this.isOnline()) {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType("application/xml");
            response.setContentLength(bbrContent.length);
            response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + label + ".xml" + "\"");
            servletOutputStream.write(bbrContent);
            servletOutputStream.flush();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        }

    }

    public void downloadOtherFile(byte[] currentFile) throws IOException {

        InputStream is = new BufferedInputStream(new ByteArrayInputStream(currentFile));
        String mimeType = URLConnection.guessContentTypeFromStream(is);


        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType(mimeType);
        response.setContentLength(currentFile.length);
        response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + "file" + "\"");
        servletOutputStream.write(currentFile);
        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void downloadZip() throws IOException {

        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        Path zipPath = Paths.get(bbrPathString + "/generatedValidators/archives/" + this.getId() + "/latest.zip");
        byte[] zipContent = Files.readAllBytes(zipPath);

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/zip");
        response.setContentLength(zipContent.length);
        response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + label + ".zip" + "\"");
        servletOutputStream.write(zipContent);
        servletOutputStream.flush();
        servletOutputStream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    public boolean isCompiling() {
        if (currentStatus == null) {
            currentStatus = status;
        }
        return StatusEnum.COMPILING.equals(currentStatus);
    }

    public boolean isBuilt() {
        if (currentStatus == null) {
            currentStatus = status;
        }
        return StatusEnum.COMPILED.equals(currentStatus);
    }

    public boolean archiveIsAvailable() {
        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        File outputFolder = new File(bbrPathString + "/generatedValidators/archives/" + this.getId() + File.separator +
                "/latest.zip");
        return outputFolder.exists();
    }

    @PostLoad
    public void init() {
        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        if (this.getStatus().equals(StatusEnum.AUTOMATIC)) {
            File outputFolder = new File(bbrPathString + "/generator/hl7templates-packager-app/output-" + this.getId() + File.separator);
            if (!outputFolder.exists()) {
                this.setCurrentStatus(StatusEnum.INITIAL);
            } else {
                boolean hasZipFile = folderHasZipFile(outputFolder);
                if (hasZipFile) {
                    this.setCurrentStatus(StatusEnum.COMPILED);
                } else {
                    this.setCurrentStatus(StatusEnum.COMPILING);
                }
            }
        } else {
            this.setCurrentStatus(this.getStatus());
        }
    }

    private boolean folderHasZipFile(File outputFolder) {
        boolean hasZipFile = false;
        File[] listFiles = outputFolder.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isFile() && FilenameUtils.getExtension(file.getAbsolutePath()).equals(ZIP)) {
                    hasZipFile = true;
                }
            }
        }
        return hasZipFile;
    }
}
