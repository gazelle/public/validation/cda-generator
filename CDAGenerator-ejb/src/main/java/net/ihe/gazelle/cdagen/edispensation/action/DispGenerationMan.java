package net.ihe.gazelle.cdagen.edispensation.action;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import net.ihe.gazelle.cdagen.edispensation.model.CDAGeneration;
import net.ihe.gazelle.cdagen.tools.CDAValidator;
import net.ihe.gazelle.cdagen.tools.XSLTransformer;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

/**
 * @author abderrazek boufahja
 */
@Name("dispGenerationMan")
@Scope(ScopeType.PAGE)
public class DispGenerationMan implements Serializable, UserAttributeCommon {

    private static final String VIEW_RESULT_BEAN = "viewResultBean";

	private static final String ENTITY_MANAGER = "entityManager";

    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;

    private String ePViewMode = "xml";

    private String eDViewMode = "xml";

    private String xmlMetaDataReqForEDisp;

    private String xmlMetaDataReqForEPresc;

    @In(value = "gumUserService")
    private UserService userService;

    public String getXmlMetaDataReqForEDisp() {
        return xmlMetaDataReqForEDisp;
    }

    public void setXmlMetaDataReqForEDisp(String xmlMetaDataReqForEDisp) {
        this.xmlMetaDataReqForEDisp = xmlMetaDataReqForEDisp;
    }

    public String getXmlMetaDataReqForEPresc() {
        return xmlMetaDataReqForEPresc;
    }

    public void setXmlMetaDataReqForEPresc(String xmlMetaDataReqForEPresc) {
        this.xmlMetaDataReqForEPresc = xmlMetaDataReqForEPresc;
    }

    private CDAGeneration selectedCDAGeneration;

    public String getePViewMode() {
        return ePViewMode;
    }

    public void setePViewMode(String ePViewMode) {
        this.ePViewMode = ePViewMode;
    }

    public String geteDViewMode() {
        return eDViewMode;
    }

    public void seteDViewMode(String eDViewMode) {
        this.eDViewMode = eDViewMode;
    }

    public CDAGeneration getSelectedCDAGeneration() {
        return selectedCDAGeneration;
    }

    public void setSelectedCDAGeneration(CDAGeneration selectedCDAGeneration) {
        this.selectedCDAGeneration = selectedCDAGeneration;
    }

    public void initCDAGEN(){
        FacesContext fc = FacesContext.getCurrentInstance();
        String id = fc.getExternalContext().getRequestParameterMap().get("id");
        EntityManager entityManager=(EntityManager)Component.getInstance(ENTITY_MANAGER);
        try{
            Integer idd = Integer.valueOf(id);
            this.selectedCDAGeneration = entityManager.find(CDAGeneration.class, idd);
        }
        catch(Exception e){
            FacesMessages.instance().add("You have to add a good id.");
            this.selectedCDAGeneration = null;
            return;
        }
    }

    public void updateEPViewForMessages(){
        if (this.selectedCDAGeneration != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance(VIEW_RESULT_BEAN);
            if ((this.ePViewMode != null) && (ePViewMode.equals("html1"))){
                vrl.setePrescriptionHtml1(this.getCDAModified(this.selectedCDAGeneration.getPrescription().getCdaFileContent()));
            }
            if ((this.ePViewMode != null) && (ePViewMode.equals("html2"))){
                vrl.setePrescriptionHtml2(CDAGenUtil.getCDAModified2(this.selectedCDAGeneration.getPrescription().getCdaFileContent()));
            }
        }
    }

    public void updateEDViewForMessages(){
        if (this.selectedCDAGeneration != null){
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance(VIEW_RESULT_BEAN);
            if ((this.eDViewMode != null) && (eDViewMode.equals("html"))){
                vrl.seteDispensationHtml1(this.getCDAModified(this.selectedCDAGeneration.getDispensation().getCdaFileContent()));
            }
        }
    }

    public String getCDAModified(String cda){
        if (cda != null){
            String res = "";
            EntityManager entityManager = (EntityManager) Component.getInstance(ENTITY_MANAGER);
            res = XSLTransformer.resultTransformation(cda, "cda.xsl", null, entityManager);
            return res;
        }
        return null;
    }

    public String currentPermanentLink(){
        if (this.selectedCDAGeneration != null){
            return ApplicationPreferenceManager.getStringValue("application_url") + 
                "/cdagen.seam?id=" + 
                this.selectedCDAGeneration.getId().intValue();
        }
        return "";
    }

    public void validateEPrescription(){
        if (this.xmlMetaDataReqForEPresc != null){
            EntityManager em = (EntityManager)Component.getInstance(ENTITY_MANAGER);
            String resultValidation = CDAValidator.getValidationResultV3(this.xmlMetaDataReqForEPresc, 
                    this.selectedCDAGeneration.getPrescription().getCdaFileContent(), em);
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance(VIEW_RESULT_BEAN);
            vrl.setHtmlResultValidation(this.geteResultValidationAndFormatted(resultValidation));
        }
        else{
            FacesMessages.instance().add("Chose a type of validation.");
        }
    }
    
    public void validateEDispensation(){
        if (this.xmlMetaDataReqForEDisp != null){
            EntityManager em = (EntityManager)Component.getInstance(ENTITY_MANAGER);
            String resultValidation = CDAValidator.getValidationResultV3(this.xmlMetaDataReqForEDisp, 
                    this.selectedCDAGeneration.getDispensation().getCdaFileContent(), em);
            ViewResultLocal vrl = (ViewResultLocal) Component.getInstance(VIEW_RESULT_BEAN);
            vrl.setHtmlResultValidation(this.geteResultValidationAndFormatted(resultValidation));
        }
        else{
            FacesMessages.instance().add("Chose a type of validation.");
        }
    }

    public String geteResultValidationAndFormatted(String resultValidation){
        if (resultValidation != null){
            EntityManager em = (EntityManager)Component.getInstance(ENTITY_MANAGER);
            return CDAValidator.transformXMLToHTML(resultValidation, em);
        }
        return null;
    }


    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
