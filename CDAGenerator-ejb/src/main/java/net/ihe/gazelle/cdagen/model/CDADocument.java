package net.ihe.gazelle.cdagen.model;

public enum CDADocument {
    
    DISPENSATION("dispensation"), PRESCRIPTION("prescription");
    
    private String value;
    
    public String getValue() {
        return value;
    }

    private CDADocument(String val) {
        this.value = val;
    }

}
