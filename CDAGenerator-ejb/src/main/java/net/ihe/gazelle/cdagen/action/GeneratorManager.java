package net.ihe.gazelle.cdagen.action;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cda.POCDMT000040ClinicalDocument;
import net.ihe.gazelle.cda.editor.action.cda.CDAInitiator;
import net.ihe.gazelle.cda.editor.action.cda.CDAPOCDMT000040ClinicalDocumentEditorLocal;
import net.ihe.gazelle.cdagen.model.CDADoc;
import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;

@Name("generatorManagerBean")
@Scope(ScopeType.PAGE)
public class GeneratorManager implements GeneratorManagerLocal, Serializable {

	private static final String C_DAPOCDMT000040_CLINICAL_DOCUMENT_EDITOR_MANAGER = "cDAPOCDMT000040ClinicalDocumentEditorManager";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(GeneratorManager.class);

	private String fileToSaveName;

	public String getFileToSaveName() {
		return fileToSaveName;
	}

	public void setFileToSaveName(String fileToSaveName) {
		this.fileToSaveName = fileToSaveName;
	}

	public void initSaveAction() {
		this.fileToSaveName = null;
	}

	public void saveCDADoc(String doc) {
		if (doc != null) {
			String name = this.fileToSaveName;
			if (!((this.fileToSaveName.split("\\.").length >= 2)
					&& (this.fileToSaveName.split("\\.")[this.fileToSaveName.split("\\.").length - 1].equals("xml")))) {
				name = name + ".xml";
			}
			this.saveDocument(doc, name);
			FacesMessages.instance().add("CDA Document was saved.");
		} else {
			FacesMessages.instance().add("The doc is empty.");
		}
	}

	public String generateCDAClinicalDocument() {
		CDAPOCDMT000040ClinicalDocumentEditorLocal xx = (CDAPOCDMT000040ClinicalDocumentEditorLocal) (Component
				.getInstance(C_DAPOCDMT000040_CLINICAL_DOCUMENT_EDITOR_MANAGER));
		xx.setSelectedCDAPOCDMT000040ClinicalDocument(CDAInitiator.initPOCDMT000040ClinicalDocument());
		return "/editor/cda/POCDMT000040ClinicalDocument.xhtml";
	}

	public String generateCDAClinicalDocument(String cdaDoc) {
		try {
			POCDMT000040ClinicalDocument cda = load(new ByteArrayInputStream(cdaDoc.getBytes()));
			CDAPOCDMT000040ClinicalDocumentEditorLocal xx = (CDAPOCDMT000040ClinicalDocumentEditorLocal) (Component
					.getInstance(C_DAPOCDMT000040_CLINICAL_DOCUMENT_EDITOR_MANAGER));
			xx.setSelectedCDAPOCDMT000040ClinicalDocument(cda);
		} catch (Exception e) {
			log.error("Problem yo parse CDA document.", e);
			CDAPOCDMT000040ClinicalDocumentEditorLocal xx = (CDAPOCDMT000040ClinicalDocumentEditorLocal) (Component
					.getInstance(C_DAPOCDMT000040_CLINICAL_DOCUMENT_EDITOR_MANAGER));
			xx.setSelectedCDAPOCDMT000040ClinicalDocument(CDAInitiator.initPOCDMT000040ClinicalDocument());
			FacesMessages.instance().add(
					"The tool is not able to understand the content of the Uploaded file. Please reupload an other well file.");
		}
		return "/editor/cda/POCDMT000040ClinicalDocument.xhtml";
	}

	public static POCDMT000040ClinicalDocument load(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.cda");
		Unmarshaller u = jc.createUnmarshaller();
		return (POCDMT000040ClinicalDocument) u.unmarshal(is);
	}

	public void downloadFile(String string) {
		if (string == null) {
			FacesMessages.instance().add(
					"You want to download an empty file ! Thank you to inform the administrator about this problem.");
			return;
		}
		this.showFile(string.getBytes(), "xml", "generated.xml", true);
	}

	public void validate(String comment) {
		// First send the file to EVSClient
		HttpClient client = new HttpClient();
		String linkEVS = ApplicationConfiguration.getValueOfVariable("evs_url");
		PostMethod filePost = new PostMethod(linkEVS + "/upload");

		// use the type as file name
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String messageType = "comment" + sdf.format(new Date());

		PartSource partSource = new ByteArrayPartSource(messageType, comment.getBytes());
		Part[] parts = { new FilePart("message", partSource) };
		filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
		int status = -1;
		try {
			status = client.executeMethod(filePost);
		} catch (Exception e) {
			status = -1;
			log.error("Problem with validation.", e);
		}
		// redirect the user to the good page
		if (status == HttpStatus.SC_OK) {
			try {
				String key = filePost.getResponseBodyAsString();
				String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.name());
				ExternalContext extCtx = FacesContext.getCurrentInstance().getExternalContext();
				HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
				String url = linkEVS + "/validate.seam?key=" + encodedKey;
				response.sendRedirect(url);
			} catch (IOException e) {
				log.error("Problem with validation.", e);
			}
		}
	}

	private void saveDocument(String ss, String path) {
		if ((ss != null) && (!ss.equals(""))) {

			CDADoc xx = new CDADoc();
			xx.setAuthor(Identity.instance().getCredentials().getUsername());
			xx.setCreationDate(new Date());
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			xx = em.merge(xx);
			xx.setPath(path);
			em.flush();
			if (path == null || path.equals("")) {
				xx.setPath(xx.getId() + ".xml");
				xx = em.merge(xx);
				em.flush();
			}
			saveFileOnTheDisc(xx, ss);
		}
	}

	private void saveFileOnTheDisc(CDADoc xx, String content) {
		if ((xx != null) && (xx.getId() != null)) {
			String repo = ApplicationConfiguration.getValueOfVariable("doc_path");
			String fileName = xx.getId() + "_" + xx.getPath();
			File path = new File(repo);
			path.mkdirs();
			File ff = new File(repo + File.separator + fileName);
			try (FileWriter fw = new FileWriter(ff);
					BufferedWriter bw = new BufferedWriter(fw);
			){
				bw.write(content);

			} catch (IOException e) {
				log.info("Problem to write in document", e);
			}
		}
	}

	private void showFile(byte[] data, String extension, String fileName, boolean download) {
		try (InputStream inputStream = new ByteArrayInputStream(data);) {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

			int length = inputStream.available();
			byte[] bytes = new byte[length];

			defineContentType(extension, response);
			
			if (fileName != null) {
				response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
			}
			if (download) {
				response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
			}

			response.setContentLength(length);

			ServletOutputStream servletOutputStream;

			servletOutputStream = response.getOutputStream();

			while (inputStream.read(bytes) > 0) {
				servletOutputStream.write(bytes);
			}

			servletOutputStream.flush();
			servletOutputStream.close();

			context.responseComplete();

		} catch (Exception e) {
			log.info("error to treat the file {}", fileName, e);
			FacesMessages.instance().add("Impossible to display file : " + e.getMessage());
		}
	}

	private void defineContentType(String extension, HttpServletResponse response) {
		if (extension != null) {
			switch (extension) {
			case "jpeg":
			case "jpg":
				response.setContentType("image/jpeg");
				break;
			case "gif":
				response.setContentType("image/gif");
				break;
			case "png":
				response.setContentType("image/png");
				break;
			case "tiff":
				response.setContentType("image/tiff");
				break;
			case "txt":
			case "text":
			case "log":
				response.setContentType("text/plain");
				break;
			case "zip":
				response.setContentType("application/zip");
				break;
			case "tar":
				response.setContentType("application/x-tar");
				break;
			case "pdf":
				response.setContentType("application/pdf");
				break;
			case "xml":
			case "xsd":
			case "dtd":
				response.setContentType("text/xml");
				break;
			default:
				response.setContentType("application/octet-stream");
				break;
			}
		}
		else {
			response.setContentType("application/octet-stream");
		}
	}

	@Destroy
	@Remove
	public void destroy() {
		log.info("destroy generatorManager..");
	}

}
