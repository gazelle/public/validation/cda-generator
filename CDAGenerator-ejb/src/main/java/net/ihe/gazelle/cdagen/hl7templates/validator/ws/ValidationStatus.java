package net.ihe.gazelle.cdagen.hl7templates.validator.ws;

public enum ValidationStatus {
	
	UNKNOWN, IN_PROGRESS, COMPLETED, ABORTED

}
