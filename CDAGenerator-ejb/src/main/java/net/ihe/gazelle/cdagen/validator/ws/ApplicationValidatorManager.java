package net.ihe.gazelle.cdagen.validator.ws;

import java.io.Serializable;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;

/**
 * 
 * @author abderrazek boufahja
 */
@Name("applicationValidatorManager")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationValidatorManagerLocal")
public class ApplicationValidatorManager implements Serializable, ApplicationValidatorManagerLocal {

	/** Serial ID version of this object */
	private static final long serialVersionUID = -450910163101283760L;

	private static Logger log = LoggerFactory.getLogger(ApplicationValidatorManager.class);

	public void reloadSVSEndpoint(){
		log.info("Initiating the CDA validator of the application");
		SVSConsumer.initializeListConceptCache(); 
		CommonOperations.setValueSetProvider(new SVSConsumer(){
			@Override
			protected String getSVSRepositoryUrl() {
				return  ApplicationConfiguration.getValueOfVariable("svs_endpoint");
			}
		});
		FacesMessages.instance().add("SVS Endpoint reloaded");
	}

}
