package net.ihe.gazelle.cdagen.edispensation.action;

import java.io.Serializable;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import net.ihe.gazelle.cdagen.datamodel.PatientDataModel;
import net.ihe.gazelle.cdagen.model.Patient;

@Stateful
@Name("authorManager")
@Scope(ScopeType.SESSION)
public class AuthorManager implements Serializable, AuthorManagerLocal {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Logger
    private static Log log;
    
    private PatientDataModel authorsFounds;
    
    private boolean searchForIdentity = false;
    
    private Patient selectedAuthor;
    
    public Patient getSelectedAuthor() {
        return selectedAuthor;
    }

    public void setSelectedAuthor(Patient selectedAuthor) {
        this.selectedAuthor = selectedAuthor;
    }

    public PatientDataModel getAuthorsFounds() {
        if (authorsFounds == null){
            authorsFounds = new PatientDataModel(null);
        }
        return authorsFounds;
    }

    public void setAuthorsFounds(PatientDataModel authorsFounds) {
        this.authorsFounds = authorsFounds;
    }

    public boolean isSearchForIdentity() {
        return searchForIdentity;
    }

    public void setSearchForIdentity(boolean searchForIdentity) {
        this.searchForIdentity = searchForIdentity;
        this.getAuthorsFounds().setCreator(Identity.instance().getCredentials().getUsername());
        this.getAuthorsFounds().resetCache();
    }
    
    public void displayAuthor(Patient inPatient) {
        selectedAuthor = inPatient;
    }

    @Destroy
    @Remove
    public void destroy()
    {
        log.info("destroy");
    }


}
