package net.ihe.gazelle.cdagen.tools;

import java.io.ByteArrayInputStream;

import javax.persistence.EntityManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jboss.seam.util.Base64;
import org.jdom.Document;
import org.jdom.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.simulator.common.utils.XMLValidation;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObject;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectE;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectResponseE;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.xmltools.xsl.XSLTransformer;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class CDAValidator {
	
    private static final String PROBLEM_WHEN_VALIDATING_CDA_DOCUMENT = "problem when validating CDA document";

	private CDAValidator(){}
    
    
	private static Logger log = LoggerFactory.getLogger(CDAValidator.class);
    
    public static boolean validateCDAByMetaData(String requestV3, String metaData, EntityManager em){
        boolean isValid = false;
        if (requestV3 == null){
            return false;
        }
        try{
            String targetEndpoint = ApplicationPreferenceManager.getStringValue("schematron_validator");
            String xmlMetaData = metaData;
            String xmlRefStandard = "CDA";
            String detailedResult = getValidationResult(targetEndpoint, xmlMetaData, xmlRefStandard, requestV3);
            if (detailedResult.contains("<ValidationTestResult>PASSED</ValidationTestResult>")){
                isValid = true;
            }
            else{
                isValid = false;
            }
        }
        catch(Exception e){
            log.info(PROBLEM_WHEN_VALIDATING_CDA_DOCUMENT, e);
            isValid = false;
        }
        return isValid;
    }
    
    public static String getValidationResultV3(String xmlMetaData,String message, EntityManager em){
        String result = "";
        String targetEndpoint = ApplicationPreferenceManager.getStringValue("schematron_validator");
        String xmlRefStandard = "CDA";
        try {
            GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(targetEndpoint);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            ValidateObject params = new ValidateObject();
            params.setXmlMetadata(xmlMetaData);
            params.setXmlReferencedStandard(xmlRefStandard);
            String msgToSend = Base64.encodeBytes(message.getBytes("UTF-8"));
            params.setBase64ObjectToValidate(msgToSend);
            ValidateObjectE paramsE = new ValidateObjectE();
            paramsE.setValidateObject(params);
            
            ValidateObjectResponseE responseE = stub.validateObject(paramsE);
            result = responseE.getValidateObjectResponse().getValidationResult();
        } catch (Exception e) {
        	log.info(PROBLEM_WHEN_VALIDATING_CDA_DOCUMENT, e);
        }
        return result;
        
    }
    
    private static String getValidationResult(String targetEndpoint, String xmlMetaData, String xmlRefStandard, String message){
        String result ="";
        try {
            GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(targetEndpoint);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            ValidateObject params = new ValidateObject();
            params.setXmlMetadata(xmlMetaData);
            params.setXmlReferencedStandard(xmlRefStandard);
            String msgToSend = Base64.encodeBytes(message.getBytes("UTF-8"));
            params.setBase64ObjectToValidate(msgToSend);
            
            ValidateObjectE paramsE = new ValidateObjectE();
            paramsE.setValidateObject(params);
            ValidateObjectResponseE responseE = stub.validateObject(paramsE);
            result = responseE.getValidateObjectResponse().getValidationResult();
        } catch (Exception e) {
        	log.info(PROBLEM_WHEN_VALIDATING_CDA_DOCUMENT, e);
        }
        return result;
        
    }
    
    public static String transformXMLToHTML(String inFilePath, EntityManager em)
    {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(inFilePath.getBytes("UTF-8"));
            String pathrr = ApplicationPreferenceManager.getStringValue("cda_result_detail");
            return XSLTransformer.resultTransformation(bais, pathrr, null);
        } catch (Exception e) {
            e.printStackTrace( );
            return null;
        }
    }
    
    public static boolean assertStringIsXML(String doc){
        DocumentBuilder builder = null;
        try{
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            builder.parse(new ByteArrayInputStream(doc.getBytes()));
        }
        catch(Exception e){
            return false;
        }
        return true;
    }
    
    public static boolean assertStringIsCDAEXtended(String doc, EntityManager em){
        DocumentValidXSD res =XMLValidation.isXSDValidEpsos(doc);
        if ((res != null) && res.getResult() != null && (res.getResult().equals("PASSED"))){
        	return true;
        }
        return false;
    }
    
    public static boolean assertStringIsCDAEPrescription(String doc,EntityManager em){
        if (!assertStringIsCDAEXtended(doc, em)) {
        	return false;
        }
        Document fileD = null;
        try {
            fileD = JdomHelper.buildDocument(new ByteArrayInputStream(doc.getBytes()));
            org.jdom.Element e = fileD.getRootElement();
            org.jdom.Element code = e.getChild("code", Namespace.getNamespace("urn:hl7-org:v3"));
            String val = code.getAttributeValue("code");
            if (val.equals("57833-6")) {
            	return true;
            }
        } catch (Exception e) {
        	log.info(PROBLEM_WHEN_VALIDATING_CDA_DOCUMENT, e);
            return false;
        }
        return false;
    }
    

}
