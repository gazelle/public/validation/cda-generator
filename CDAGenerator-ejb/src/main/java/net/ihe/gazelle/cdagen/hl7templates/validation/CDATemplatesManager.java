package net.ihe.gazelle.cdagen.hl7templates.validation;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.cdagen.datamodel.HL7TemplValidationDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

@Stateful(name="cdaTemplatesManager")
@Name(value="cdaTemplatesManager")
@GenerateInterface(value="CDATemplatesManagerLocal")
public class CDATemplatesManager implements CDATemplatesManagerLocal{
	
	HL7TemplValidationDataModel temps;
	
	public HL7TemplValidationDataModel getTemps() {
		if (temps == null) {
			temps = new HL7TemplValidationDataModel();
		}
		return temps;
	}

	public void setTemps(HL7TemplValidationDataModel temps) {
		this.temps = temps;
	}

	public HL7TemplValidationDataModel getListValidatedTemplates() {
		return this.getTemps();
	}
	
	@Remove
	public void delete() {
		// default delete bean method
	}
	

}
