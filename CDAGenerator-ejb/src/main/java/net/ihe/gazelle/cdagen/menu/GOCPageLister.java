package net.ihe.gazelle.cdagen.menu;

import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@MetaInfServices(PageLister.class)
public class GOCPageLister implements PageLister {

    @Override
    public Collection<Page> getPages() {
        Collection<Page> pages = new ArrayList<>();
        pages.addAll(Arrays.asList(Pages.values()));
        return pages;
    }
}
